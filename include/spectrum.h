#ifndef __SPECTRUM_H__
#define __SPECTRUM_H__


#include "TH1D.h"
#include "TMath.h"
#include "TRandom3.h"


namespace fast {

/** Type of a fourier spectrum
 */
enum class SpectrumType {
  PS,   ///< Power spectrum
  PSD,  ///< Power spectral density
  LS,   ///< Linear spectrum
  LSD   ///< Linear spectral density
};

/** Window functions to be used with DFTs.
 */
class WindowFct {
 public:
  /** Type of the window function
   */
  enum Type {
    RECT,     ///< Rectangle
    HANNING,  ///< Hanning
    HFT116D,  ///< HFT116D
    FTNI,     ///< National Instruments flat-top
    BLAHAR,   ///< Blackman-Harris
  };

  /** Constructor.
   *
   *  \param N size of the window
   *  \param type window function type
   */
  WindowFct(unsigned int N, const WindowFct::Type& type=RECT);

  /** Get the type of this window function
   *
   *  \return window functino type
   */
  WindowFct::Type getType() const;

  /** Get the size of this window function
   *
   *  \return size of the window function
   */
  std::size_t size() const;

  /** Get the vector of weights
   *
   *  \return vector of weights
   */
  std::vector<double> getWeights() const;

  /** Get the sum of weights
   *
   *  \return sum of weights
   */
  double getSumW() const;

  /** Get the sum of squared weights
   *
   *  \return sum of squared weights
   */
  double getSumW2() const;

  /** Apply this window function to a time series
   *
   *  \param hist time series
   */
  void applyOn(TH1* hist);

 private:
  const WindowFct::Type kType;    ///< current type of window function
  std::vector<double> weights_;   ///< vector of weights
  double sumw_;                   ///< sum of weights
  double sumw2_;                  ///< sum of squared weights

  /** Fill the weights vector according to the current window functino type
   */
  void setWeights();

  /** Implementation of a rectangular window function
   *
   *  \param N window size
   *  \param i index of the weight to calculate
   *  \return weight of index i
   *
   *  \f[
   *    w_i = 1
   *  \f]
   */
  static double weight_rect   (unsigned int N, unsigned int i);

  /** Implementation of a Hanning window function
   *
   *  \param N window size
   *  \param i index of the weight to calculate
   *  \return weight of index i
   *
   *  \f[
   *    w_i = \frac{1-\cos z}{2}
   *    \qquad \text{with} \qquad
   *    z = \frac{2 \pi \cdot i}{N}
   *  \f]
   */
  static double weight_hanning(unsigned int N, unsigned int i);

  /** Implementation of a HFT116D window function
   *
   *  \param N window size
   *  \param i index of the weight to calculate
   *  \return weight of index i
   *
   *  \f{align*}{
   *    w_i = 1 &− 1.9575375 \cos(z)  \\
   *            &+ 1.4780705 \cos(2z) \\
   *            &− 0.6367431 \cos(3z) \\
   *            &+ 0.1228389 \cos(4z) \\
   *            &− 0.0066288 \cos(5z) \\
   *            &\text{with} \quad z = \frac{2 \pi \cdot i}{N}
   *  \f}
   */
  static double weight_hft116d(unsigned int N, unsigned int i);

  /** Implementation of a FTNI window function
   *
   *  \param N window size
   *  \param i index of the weight to calculate
   *  \return weight of index i
   *
   *  \f{align*}{
   *    w_i =    0.2810639
   *          &− 0.5208972 \cos(z)  \\
   *          &+ 0.1980399 \cos(2z)  \\
   *          &\text{with} \quad z = \frac{2 \pi \cdot i}{N}
   *  \f}
   */
  static double weight_ftni(unsigned int N, unsigned int i);

  /** Implementation of a Blackman-Harris window function
   *
   *  \param N window size
   *  \param i index of the weight to calculate
   *  \return weight of index i
   *
   *  \f{align*}{
   *    w_i =    0.35875
   *          &− 0.48829 \cos(z)  \\
   *          &+ 0.14128 \cos(2z)  \\
   *          &- 0.01168 \cos(3z)  \\
   *          &\text{with} \quad z = \frac{2 \pi \cdot i}{N}
   *  \f}
   */
  static double weight_blahar(unsigned int N, unsigned int i);

};

/** Discrete Fourier Transfromation.
 *  Uses the ROOT capabilities for Fast Fourier Transforms based on FFTW.
 */
class DFT {
 public:
  /** Constructor
   *
   *  \param sample timeseries
   *  \param type window function type to use
   */
  DFT(const TH1* sample, const WindowFct::Type type=WindowFct::HANNING);

  /** Constructor
   *
   *  \param sample timeseries
   *  \param type window function type to use
   */
  DFT(const TH1 &sample, const WindowFct::Type type=WindowFct::HANNING);

  /** Constructor
   *
   *  \param volts time domain data points
   *  \param fs sampling frequency in Hz
   *  \param type window function type to use
   */
  DFT(const std::vector<double>& volts, double fs,
      const WindowFct::Type type=WindowFct::HANNING);

  /** Destructor
   */
  virtual ~DFT() = default;

  /** Get the sampling frequency
   *
   *  \return sampling frequency in Hz
   */
  double getSamplingFreq() const;

  /** Get sample size
   *
   *  \return size of the (sub-)sample
   *
   *  If a averaging is on, e.g. by choosing a different frequency resolution,
   *  this returns the size of each subsample.
   */
  std::size_t getN() const;

  /** Get sample length
   *
   *  \return length of the (sub-)sample in seconds
   *
   *  If a averaging is on, e.g. by choosing a different frequency resolution,
   *  this returns the length of each subsample.
   */
  double getTimeSpan() const;

  /** Get the effective frequency resolution
   *
   *  \return frequency resolution in Hz
   */
  double getFreqResolution() const;

  /** Set the frequency resolution
   *
   *  \param target desired frequency resolution in Hz
   *
   *  The resulting effective frequency resolution depends on the calculated
   *  splitting optimization. In order to increase the performance of the FFT
   *  the sample size is choosen according to `calcSplitting()` and thus might
   *  result in a slightly different effective resolution.
   */
  void setFreqResolution(const double target);

  /** Check if DC offset removal is on.
   *
   *  \return true if DC offset removal is on
   */
  bool getDCremoval() const;

  /** Set DC offset removal flag.
   *
   *  \param removeDC set true to activate DC offset removal
   */
  void setDCremoval(const bool removeDC);

  /** Get the used window function
   *
   *  \return window function in use
   */
  const std::shared_ptr<WindowFct> getWindowFct() const;

  /** Choose a different window function
   *
   *  \param type window function type to use
   */
  void setWindowFctType(const WindowFct::Type type);

  /** Calculate the spectrum of the specified type
   *
   *  \param type spectrum type to calculate
   *  \param name ROOT internal name of the returned spectrum
   *  \param title title string of the returned spectrum
   *  \return (averaged) spectrum of the input timeseries
   *
   *  If a different frequency resolution has been choosen, this return the
   *  averaged spectrum.
   */
  TH1D spectrum(const SpectrumType type, const std::string& name="",
                const std::string& title="") const;

  /** Get the effective sample length for efficient FFT
   *
   *  \param fs actual sampling frequency in Hz
   *  \param fres target frequency resolution in Hz
   *  \param feff resulting effective frequency resolution in Hz
   *  \return optimized sample length
   *
   *  Calculate the optimized sample length and effective frequency resolution
   *  for high efficient FFTs.
   *
   *  See also `calcEffFFTWsize()`
   */
  static std::size_t calcSplitting(double fs, double fres, double& feff);

  /** Omptimze sample length for FFTW performance
   *
   *  \param N original sample length
   *  \return optimized sample length
   *
   *  FFTW perfroms fourier transformations highly efficient for samples of
   *  length
   *
   *  \f[
   *    N = 2^a 3^b 5^c 7^d 11^e 13^f
   *    \qquad \text{with} \qquad
   *    a,b,c,d,e,f \in \mathcal{N} 
   *    \qquad \text{and} \qquad e+f \in \{0,1\}
   *  \f]
   *
   *  \todo Recheck that this is implemented correctly
   */
  static std::size_t calcEffFFTWsize(std::size_t N);

 private:
  std::unique_ptr<TH1> sample_          = nullptr;  ///< original sample
  std::shared_ptr<WindowFct> windowFct_ = nullptr;  ///< window function in use
  std::size_t splitLength_;                         ///< eff. subsample length
  bool removeDC_ = true;                            ///< DC offset removal flag

  /** Get the number of non redundant frequency bins
   *
   *  \return number of bins
   *
   *  Since real valued input data is considered, the upper half of the fourier
   *  coefficients is redundant.
   */
  std::size_t getNumNonRedundantBins() const;

  /** Calculate squared magnitudes histogram
   *
   *  \return (averaged) histogram of squared fourier magnitudes
   *
   *  The main transformation work is done here comprising the following steps:
   *  1. Split the sample into subsamples for the desired frequency resolution
   *  2. Remove the DC offset if activated
   *  3. Apply the choosen window function
   *  4. Calculate the non redundnat magnitudes fourier tranform coefficients
   *     and square them
   *  5. Average over all subsamples
   */
  TH1D compSquareMagnitudes() const;

  /** Cacluclate the square root of a histogram
   *
   * \param hist histogram to take the square root per bin
   */
  void histSqrt(TH1D &hist) const;

  /** Remove the DC offset
   *
   *  \param hist histogram to remove the DC offset
   *
   *  This is done by shifting the histogram such that the integral is 0
   */
  void removeDCcomponent(TH1D &hist) const;
};

/** Inverse Discrete Fourier Transformation.
 *  Uses the ROOT capabilities for Fast Fourier Transforms based on FFTW.
 */
class IDFT {
 public:
  /** Constructor
   *
   *  \param spectrum to calculate the inverse fourier transfrom from
   *  \param specType type of the given spectrum, e.g. LSD, PS,...
   */
  IDFT(const TH1* spectrum, const SpectrumType specType);

  /** Constructor
   *
   *  \param spectrum to calculate the inverse fourier transfrom from
   *  \param specType type of the given spectrum, e.g. LSD, PS,...
   */
  IDFT(const TH1 &spectrum, const SpectrumType specType);

  /** Get the Nyquist frequency
   *
   *  \return Nyquist frequency in Hz
   */
  double getNyquistFreq() const;

  /** Get the sampling frequency
   *
   *  \return sampling frequency in Hz
   */
  double getSamplingFreq() const;

  /** Get the frequency resolution
   *
   *  \return bin width of the given frequency spectrum in Hz
   */
  double getFreqResolution() const;

  /** Get the size of the resulting timeseries
   *
   *  \return size of the resulting timeseries
   */
  std::size_t getSampleSize() const;

  /** Draw a randowm sample following the given spectrum
   *
   *  \return random timeseries sample
   *
   *  Draws random numbers \f$ \phi_i \in \{0, 2\pi \} \f$ for the fourier
   *  phase in each frequency bin of the given spectrum and perform an
   *  inverse fourier transformation.
   */
  TH1D drawSample() const;

  /** Draw a random phase and decompose real and imaginary part
   *
   *  \param mag target magnitude of the complex number
   *  \param re resulting real part of the complex number
   *  \param im resulting imaginary part of the complex number
   *
   *  Draws random phase \f$ \phi_i \in \{0, 2\pi \} \f$ and decomposes the
   *  complex number of given magntiude and this random phase into real and
   *  imaginary part.
   */
  void drawRndPhase(double mag, double &re, double &im) const;

  /** Set the internal random generators seed.
   *
   *  \param seed to set on the generator
   */
  void setRandomSeed(const unsigned int seed);

 private:
  const SpectrumType specType_;               ///< input spectrum type
  std::unique_ptr<TH1> spectrum_ = nullptr;   ///< input spectrum
  std::unique_ptr<TRandom3> rnd_ = nullptr;   ///< random number generator

  /** Perform the actual inverse fourier transformation
   *
   *  \param N  resulting timeseries length
   *  \param re real part of the fourier coefficients
   *  \param im imaginary part of the fourier coefficients
   *  \return timeseries histogram
   */
  std::unique_ptr<TH1> backTransform(int N, const std::vector<double>& re,
                                     const std::vector<double>& im) const;

  /** Calculate the fourier coefficient magnitude from a spectrum bin
   *
   *  \param specBin spectrum bin content
   *  \return complex magnitude
   */
  double spec2mag(const double specBin) const;
};

} /* fast */


#endif /* __SPECTRUM_H__ */
