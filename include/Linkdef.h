#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ namespace fast;

/**
 *  dataio.h
 */
#pragma link C++ class fast::AlazarRecord;
#pragma link C++ class fast::TreeReader<fast::AlazarRecord>;
#pragma link C++ class fast::UnifiedVector<double>;
#pragma link C++ class fast::UnifiedVector<float>;

/**
 *  defs.h
 */
#pragma link C++ enum fast::Channel;


/**
 *  generator.h
 */
#pragma link C++ class fast::Generator;

/**
 *  generatorrecord.h
 */
#pragma link C++ class fast::GeneratorRecord;

/**
 *  processor.h
 */
#pragma link C++ class fast::ProcessingUnit;
#pragma link C++ class fast::ToTProcessor;
#pragma link C++ class fast::PulseFitProcessor;
#pragma link C++ class fast::CloneProcessor;
#pragma link C++ class fast::PHPIProcessor;

/**
 *  pulsefct.h
 */
#pragma link C++ namespace fast::pulsefct;
#pragma link C++ function fast::pulsefct::photon;

/**
 *  pulsefitter.h
 */
#pragma link C++ class fast::PulseFitter;

/**
 *  spectrum.h
 */
#pragma link C++ enum fast::SpectrumType;
#pragma link C++ enum fast::WindowFct::Type;
#pragma link C++ class fast::WindowFct;
#pragma link C++ class fast::DFT;
#pragma link C++ class fast::IDFT;

/**
 *  timeline.h
 */
#pragma link C++ class fast::TimelineTrigger;
#pragma link C++ enum fast::TimelineTrigger::Slope;

/**
 *  transformations.h
 */
#pragma link C++ class fast::ParameterType;
#pragma link C++ class fast::FixedValue;
#pragma link C++ class fast::GausValue;
#pragma link C++ class fast::FlatValue;
#pragma link C++ class fast::HistValue;
#pragma link C++ class fast::Parameter;

#pragma link C++ class fast::Transformation;
#pragma link C++ class fast::AddScalar;
#pragma link C++ class fast::Multiply;
#pragma link C++ class fast::TimeShift;
#pragma link C++ class fast::AddWhiteNoise;
#pragma link C++ class fast::AddSpectralNoise;
#pragma link C++ class fast::AddPhotonPulse;

/**
 *  utils.h
 */
#pragma link C++ function fast::utils::addChannelSuffix;
#pragma link C++ class fast::utils::ConfigParser;

#endif
