#ifndef __PROCESSOR_H__
#define __PROCESSOR_H__

#include <memory>

#include "TTree.h"

#include "dataio.h"
#include "defs.h"
#include "pulsefitter.h"
#include "routines.h"


namespace fast {


/** Base type for for AlazarRecord processor classes.
 *  ProcessingUnits take an AlazarRecord entry and perform a particular
 *  processing step on this data, e.g. PulseFitting, PeakIntegral estimation,
 *  etc. and return their result as TreeRecord inherited struct, hence all
 *  results can be easily written to a TTree. Therefor all Processing Units
 *  implements the `TreeLoggable` interface.
 *
 *  ProcessingUnits work on a single channel basis, which is why all
 *  constructor take a `Channel` selection argument. If you like to process
 *  both channel you need to intantiate a second ProcessingUnit for that
 *  purpose.
 */
class ProcessingUnit : public TreeLoggable {
 public:
  /** Constructor.
   *
   *  \param name name of this processing unit
   *  \param selection selected channel to work on.
   */
  ProcessingUnit(const std::string& name, const Channel& selection);

  /** Destructor.
   */
  virtual ~ProcessingUnit() = default;

  /** Main processing operator to call with an `AlazarRecord`.
   *
   *  \param record data record to process by this unit
   *  \return TreeRecord reference containing the result
   *
   *  All the processing functionality has to be implemented here and makes
   *  this ProcessingUnit object callable with a AlazarRecord instance to
   *  process.
   */
  virtual const TreeRecord& operator() (const AlazarRecord& record) = 0;

  /** Get the selected `Channel`
   *  
   *  \return the selected `Channel`
   */
  Channel getChannel() const;

  /** Return the name of this ProcessingUnit
   */
  std::string getName() const;

 protected:
  /** Get the data pointer of the selected channel.
   *
   *  \param record AlazarRecord to process
   *  \return pointer to the data vector of the selected channel
   *
   *  Convenience method to retrieve the data vector according to the selected
   *  channel.
   */
  std::vector<double>* getSelectedData(const AlazarRecord& record) const;

  /** Create a branchname with prefix and suffix.
   *
   *  \param original name of the branch
   */
  std::string getBranchName(const std::string& original) const;

 private:
  const Channel ch_;    ///< selected Channel to work on
  std::string name_;    ///< name of this processing unit
};


/** Base type for all ProcessingUnits that are able to find peaks.
 */
class PeakFindingUnit : public ProcessingUnit {
 public:
  /** Method ID used for peakfinding (L2 Trigger)
   */
  enum MethodID : unsigned int {
    DERIV = 1,   ///< Derivative
    XCORR = 2,   ///< Cross-Correlation
  };

  /** Constructor.
   *
   *  \param name name of this processing unit.
   *  \param selection selected channel to work on.
   *  \param methodID Identifier of this units peakfinding method.
   */
  PeakFindingUnit(const std::string& name, const Channel& selection,
                  const MethodID methodID);

  /** Return a vector of identified peak positions
   */
  virtual std::vector<double> getPeakPos(const AlazarRecord& record) = 0;

  MethodID getMethodID() const;

 private:
  const MethodID kMethodID;
};


/** ProcessingUnit to estimate the TimeOverThreshold
 */
class ToTProcessor : public ProcessingUnit {
 public:
  /** Direction of the comparison
   */
  enum Sign {
    ABOVE = +1,  ///< count data if above threshold
    BELOW = -1,  ///< count data if below threshold
  };

  /** Constructor.
   *
   *  \param name name of this processing unit
   *  \param selection selected channel to work on.
   *  \param sign direction of the comparison.
   *  \param threshold estimate time the pulse is exceeding this value.
   */
  ToTProcessor(const std::string& name, const Channel selection,
               const Sign sign, const double threshold);

  /** Result record
   */
  struct Record : TreeRecord {
    double tot;   ///< time over threshold in seconds

    BranchMap branchmap = {
      {"tot", &tot}
    };
  };

  const Record& operator() (const AlazarRecord& record) override;

  void setTree(const std::shared_ptr<TTree>& tree) override;

 private:
  const Sign sign_;         ///< sign of the comparison
  const double threshold_;  ///< threshold in volts
  Record result_;           ///< record member storing the results

  inline int sgn(const double val) const;
};


/** ProcessingUnit performing pulse fitting.
 *  Uses an internal instance of `PulseFitter` to perform fits of the
 *  pulseshape.
 */
class PulseFitProcessor : public ProcessingUnit {
 public:
  /** Constructor.
   *
   *  \param name name of this processing unit
   *  \param selection selected channel to work on.
   *  \param uncert Fixed uncertenty per data point.
   *  \param pf PeakFindingUnit to use for multple photons.
   *
   *  If no PeakFindingUnit is given only a single peak is assumed.
   */
  PulseFitProcessor(const std::string& name, const Channel& selection,
                    const double uncert,
                    const std::shared_ptr<PeakFindingUnit>& pf=nullptr);

  /** Result record
   */
  struct Record : TreeRecord {
    std::vector<double> amplitude;   ///< pulse amplitude
    std::vector<double> tRise;       ///< rise time
    std::vector<double> tFall;       ///< fall time
    std::vector<double> tOffset;     ///< pulse time offset
    double DCoffset;                 ///< constant DC offset
    double chi2;                     ///< chi2 of the fit
    unsigned int ndf;                ///< number of degrees of freedom

    std::shared_ptr<TGraphErrors> graph;  ///< plot of the data w/ fit-overlay
    TFitResultPtr fitResult;              ///< Pointer to the full fit result

    BranchMap branchmap {
      {"amplitude", &amplitude},
      {"tRise",     &tRise},
      {"tFall",     &tFall},
      {"tOffset",   &tOffset},
      {"DCoffset",  &DCoffset},
      {"chi2",      &chi2},
      {"ndf",       &ndf}
    };
  };

  const Record& operator() (const AlazarRecord& record) override;

  void setTree(const std::shared_ptr<TTree>& tree) override;

  const std::unique_ptr<PulseFitter>& pulseFitter() const;

  /** Return a pointer to the PeakFindingUnit.
   */
  const std::shared_ptr<PeakFindingUnit> getPeakFinder() const;

 private:
  const double uncert_;   ///< fixed uncertainty per data point

  std::unique_ptr<PulseFitter> fitter_; ///< instance of PulseFitter
  Record result_;                       ///< record member storing the results
  std::shared_ptr<PeakFindingUnit> pf_; ///< peak finder for multiple photons
};


/** ProcessingUnit to clone original data branches.
 *  Convenience processor to have the original input data available in the
 *  analysis results tree.
 */
class CloneProcessor : public ProcessingUnit {
 public:
  /** Constructor.
   *
   *  \param name name of this processing unit
   *  \param selection selected channel to work on.
   */
  CloneProcessor(const std::string& name, const Channel& selection);

  /** Result record
   */
  struct Record : TreeRecord {
    double               samplingFrequency;   ///< sampling freqeuncy branch
    bool                 trigger;             ///< trigger branch
    std::vector<double>* data;                ///< data branch

    BranchMap branchmap {
      {"samplingFrequency", &samplingFrequency},
      {"trigger",           &trigger},
      {"data",              &data},
    };
  };

  const Record& operator() (const AlazarRecord& record) override;

  void setTree(const std::shared_ptr<TTree>& tree) override;

 private:
  Record result_;   ///< record member storing the results
};


/** ProcessingUnit to estimate the peak height and integral numerically.
 *  For comparison reasons this processor implements estimation of peak height
 *  and integral the old fashioned way.
 */
class PHPIProcessor : public ProcessingUnit {
 public:
  /** Constructor.
   *
   *  \param name name of this processing unit
   *  \param selection selected channel to work on.
   *  \param fullLength instead of integrating the pulse only, integrate over
   *  the full length of the data sample
   */
  PHPIProcessor(const std::string& name, const Channel& selection,
                bool fullLength=false);

  /** Result record
   */
  struct Record : TreeRecord {
    double ph;
    double pi;
    unsigned int ph_pos_idx;
    unsigned int pi_beg_idx;
    unsigned int pi_end_idx;

    BranchMap branchmap {
      {"ph",         &ph},
      {"pi",         &pi},
      {"ph_pos_idx", &ph_pos_idx},
      {"pi_beg_idx", &pi_beg_idx},
      {"pi_end_idx", &pi_end_idx},
    };
  };

  const Record& operator() (const AlazarRecord& record) override;

  void setTree(const std::shared_ptr<TTree>& tree) override;

 private:
  Record result_;      ///< record member storing the results
  bool   fullLength_;  ///< integrate over full length of data
};


/** ProcessingUnit calculating the offset_corrected cross-correlation with the
 * analytic pulse.  The cross-correlation maximises if the analytic photon
 * pulse shape matches the data after a specific shift in time, which is also
 * estimated by this processor.
 */
class XCorrProcessor : public PeakFindingUnit {
 public:
  /** Constructor.
   *
   *  \param name name of this processing unit
   *  \param selection selected channel to work on.
   *  \param shape PulseShape definition of analytic pulse used in correlation.
   *  \param tMinFactor Analytic pulse length in multiples of tMin_analytic.
   *  \param threshold find only pulses exceeding this threshold. 0=deactivate
   */
  XCorrProcessor(const std::string& name, const Channel& selection,
                 const PulseShape& shape, const double tMinFactor,
                 const double threshold=0);

  /** Result record
   */
  struct Record : TreeRecord {
    std::vector<double> time;   ///< corrected times of maxima
    std::vector<double> value;  ///< height of maxima
    std::vector<double> data;   ///< xcorrelation function
    unsigned int methodID;      ///< peakfinding method ID
    std::vector<double> params; ///< parameters of this peakfinding unit
    double threshold;           ///< threshold used to filter results

    BranchMap branchmap {
      {"time",      &time},
      {"value",     &value},
      {"data",      &data},
      {"methodID",  &methodID},
      {"params",    &params},
      {"threshold", &threshold},
    };
  };

  const Record& operator() (const AlazarRecord& record) override;

  void setTree(const std::shared_ptr<TTree>& tree) override;

  std::vector<double> getPeakPos(const AlazarRecord& record) override;

  double getThreshold() const { return threshold_; };
  void setThreshold(const double t) { threshold_ = t; };

 private:
  const PulseShape kShape;
  const double kTminFactor;
  const double kTmin;
  double threshold_ = 0;

  Record result_;
  std::unique_ptr<FilterKernel> kernel_;

  /** Helper function for lazy initialization of the FilterKernel object.
   *
   *  \param fs sampling frequency
   */
  void initFilterKernel(const double fs);
};


/** Processor calculating the first derivative of a data record.
 *  The derivative is calculated via second order finite differences (see
 *  `derivative()` in `routines.h` for details). When specifying a nonzero
 *  cutOff frequency the data is passed through a DSP LowPass filter before
 *  calculating the derivative. In order to compensate for the time delay such
 *  an IIR filter would introduce the Zero-Phase filtering technique is used.
 */
class DerivProcessor : public PeakFindingUnit {
 public:
  /** Constructor
   *
   *  \param name name of this processing unit
   *  \param selection selected channel to work on.
   *  \param cutOff If nonzero a LowPass filter is used with this cutoff freq.
   *  \param threshold find only pulses exceeding this threshold. 0=deactivate
   */
  DerivProcessor(const std::string& name, const Channel& selection,
                 const double cutOff, const double threshold=0);

  /** Result Record
   */
  struct Record : TreeRecord {
    std::vector<double> time;
    std::vector<double> value;
    std::vector<double> data;
    unsigned int methodID;
    std::vector<double> params;
    double threshold;

    BranchMap branchmap {
      {"data",      &data},
      {"time",      &time},
      {"value",     &value},
      {"methodID",  &methodID},
      {"params",    &params},
      {"threshold", &threshold},
    };
  };

  const Record& operator() (const AlazarRecord& record) override;

  void setTree(const std::shared_ptr<TTree>& tree) override;

  std::vector<double> getPeakPos(const AlazarRecord& record) override;

  double getThreshold() const { return threshold_; };
  void setThreshold(const double t) { threshold_ = t; };

 private:
  const double kCutOff;
  double threshold_ = 0;

  Record result_;
  std::unique_ptr<DspFilter<Dsp::Butterworth::LowPass<5>>> filter_;

  /** Initialize the DSP LowPass filter
   *
   *  \param fs sampling frequency
   */
  void initDSPFilter(const double fs);
};

} /* namespace fast */


#endif /* __PROCESSOR_H__ */
