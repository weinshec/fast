#include "generatorrecord.h"
#include "utils.h"


using namespace fast;


GeneratorRecord::GeneratorRecord(const std::size_t size, const double samplingFreq)
    : fs_(samplingFreq), data_(size, 0.0) { };

std::size_t GeneratorRecord::size() const {
  return data_.size();
}

std::vector<double> GeneratorRecord::getData() const {
  return data_;
}

double GeneratorRecord::getSamplingFreq() const {
  return fs_;
}

std::map<std::string, double> GeneratorRecord::getTruthMap() const {
  return truthMap_;
}

void GeneratorRecord::addTruth(const std::string &name, const double value) {
  if (truthMap_.find(name) != truthMap_.end())
    utils::LogWarning("GeneratorRecord") << "Duplicate truth value name" + name;
  else
    truthMap_[name] = value;
}

void GeneratorRecord::append(const GeneratorRecord &sample) {
  append(sample.getData());
}

void GeneratorRecord::append(const std::vector<double> &data) {
  data_.insert(std::end(data_), std::begin(data), std::end(data));
}

GeneratorRecord& GeneratorRecord::operator+= (const double rhs) {
  for (auto &value : data_)
    value += rhs;
  return *this;
}

GeneratorRecord& GeneratorRecord::operator+= (const std::vector<double>& rhs) {
  if (data_.size() != rhs.size()) {
    utils::LogWarning("GeneratorRecord")
        << "Cannot add vector of different size";
    return *this;
  }

  for (std::size_t i = 0; i < data_.size(); i++)
    data_.at(i) += rhs.at(i);
  return *this;
}

GeneratorRecord& GeneratorRecord::operator*= (const double rhs) {
  for (auto &value : data_)
    value *= rhs;
  return *this;
}

GeneratorRecord& GeneratorRecord::operator<< (const std::size_t rhs) {
  auto oldSize = size();
  data_.erase( data_.begin(), data_.begin() + rhs );
  data_.resize( oldSize, 0.0 );
  return *this;
}

GeneratorRecord& GeneratorRecord::operator>> (const std::size_t rhs) {
  auto oldSize = size();
  data_.insert( data_.begin(), rhs, 0.0 );
  data_.resize(oldSize);
  return *this;
}

TH1D GeneratorRecord::makeHistogram() const {
  TH1D hist("hRaw", "hRaw", size(), 0.0, size()/fs_);
  for (std::size_t i = 0; i < size(); i++)
    hist.SetBinContent(i+1, data_.at(i));
  return hist;
}
