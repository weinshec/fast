#ifndef __FAST_ROUTINES_H__
#define __FAST_ROUTINES_H__

#include <algorithm>
#include <array>
#include <memory>
#include <numeric>
#include <vector>

#include "DspFilters/Dsp.h"


namespace fast {

/** Calculate the derivative based on finite differences.
 *
 *  \param data vector of data values
 *  \param dx data point spacing
 *  \return derivative of data
 *
 *  This method uses 2nd order accurate central finite differences on the
 *  interior of the data vector and 1st order forward/backward finite
 *  differences on the border.
 */
std::vector<double> derivative(const std::vector<double>& data,
                               const double dx);

/** Calculate the derivative based on finite differences.
 *
 *  \param data vector of data values
 *  \param dx data point spacing
 *  \return derivative of data
 *
 *  This method uses 2nd order accurate central finite differences on the
 *  interior of the data vector and 1st order forward/backward finite
 *  differences on the border.
 */
std::vector<double> derivative(const std::vector<double>* data,
                               const double dx);

/** Find local minima in a vector.
 *
 *  \param data vector of datapoints
 *  \returns vector with indices of local minima
 */
template<typename T>
std::vector<size_t> local_minima(const std::vector<T>& data) {
  std::vector<size_t> minima;
  for (size_t i = 1; i < data.size()-1; ++i)
    if (data.at(i-1) > data.at(i) && data.at(i) < data.at(i+1))
      minima.push_back(i);
  return minima;
}

/** Find local minima in a vector below a certain threshold.
 *
 *  \param data vector of datapoints
 *  \param threshold threshold below which minima are taken into account
 *  \returns vector with indices of local minima
 */
template<typename T>
std::vector<size_t> local_minima(const std::vector<T>& data,
                                 const T threshold) {
  auto minima = local_minima(data);
  minima.erase( std::remove_if(minima.begin(), minima.end(),
                    [&data, threshold](size_t minIdx){
                    return data.at(minIdx) > threshold; }),
                minima.end());
  return minima;
}

/** Find local maxima in a vector.
 *
 *  \param data vector of datapoints
 *  \returns vector with indices of local maxima
 */
template<typename T>
std::vector<size_t> local_maxima(const std::vector<T>& data) {
  std::vector<size_t> maxima;
  for (size_t i = 1; i < data.size()-1; ++i)
    if (data.at(i-1) < data.at(i) && data.at(i) > data.at(i+1))
      maxima.push_back(i);
  return maxima;
}

/** Find local maxima in a vector above a certain threshold.
 *
 *  \param data vector of datapoints
 *  \param threshold threshold above which maxima are taken into account
 *  \returns vector with indices of local maxima
 */
template<typename T>
std::vector<size_t> local_maxima(const std::vector<T>& data,
                                 const T threshold) {
  auto maxima = local_maxima(data);
  maxima.erase( std::remove_if(maxima.begin(), maxima.end(),
                    [&data, threshold](size_t maxIdx){
                    return data.at(maxIdx) < threshold; }),
                maxima.end());
  return maxima;
}

/** Create a sorting permuation of a vector for a given comparator.
 *
 *  \param v vector to get the sorting permuation from
 *  \param c comparator representing the sorting condition
 *  \return permutation vector representing the sorted indices of `v`
 *
 *  Example usage:
 *  ~~~.cpp
 *    auto p = sort_permuation(randomInts,
 *        [](const int& a, const int& b){ return a > b; });
 *    apply_permutation(randomInts, p);
 *  ~~~
 */
template<typename T, typename Compare>
std::vector<size_t> sort_permuation(const std::vector<T>& v, Compare c) {
  std::vector<size_t> sortIdx(v.size());
  std::iota( sortIdx.begin(), sortIdx.end(), 0);
  std::sort( sortIdx.begin(), sortIdx.end(),
      [&](std::size_t i, std::size_t j){ return c(v[i], v[j]); } );
  return sortIdx;
}

/** Apply a permuation on a vector vector.
 *
 *  \param v vector to apply the permutation
 *  \param perm permutation indices
 *
 *  Example usage:
 *  ~~~.cpp
 *    auto p = sort_permuation(randomInts,
 *        [](const int& a, const int& b){ return a > b; });
 *    apply_permutation(randomInts, p);
 *  ~~~
 */
template<typename T>
void apply_permutation(std::vector<T>& v, std::vector<size_t>& perm) {
  std::transform( perm.begin(), perm.end(), v.begin(),
    [=](std::size_t i){ return v[i]; });
}

/** Create a (sorted) superset of the two vectors.
 *
 *  \param vecA first vector
 *  \param vecB second vector
 *  \return sorted union of both input vectors
 */
template<typename T>
std::vector<T> superset(const std::vector<T>& vecA,
                        const std::vector<T>& vecB) {
  std::vector<T> result(vecA);
  std::copy(std::begin(vecB), std::end(vecB), std::back_inserter(result));
  std::sort(std::begin(result), std::end(result));
  return result;
}

/** Merge close-by elements in a (sorted) vector
 *
 *  \param vec input vector
 *  \param threshold distance between elements that are considered equal
 *  \returns original vector where elements being closer than `threshold` have
 *  been merged.
 *
 *  \note Elements to be merged are replaced by the first one of the subset,
 *  e.g. { 6.9, 7.0, 7.1 } -> {6.9} for threshold > 0.2
 */
template<typename T>
std::vector<T> merge_elements(const std::vector<T>& vec, const T threshold) {
  std::vector<T> result(vec);
  if (!std::is_sorted(std::begin(result), std::end(result)))
    std::sort(std::begin(result), std::end(result));

  auto comparator = [threshold](const T& a, const T&b) {
      return std::abs(a - b) <= threshold;
  };
  auto last = std::unique(std::begin(result), std::end(result), comparator);
  result.erase(last, result.end());

  return result;
}


/** Wrapper class for Digital Signal filtering.
 *  This wrapper is intended to simplify the interface to Dsp::Filter classes,
 *  especially the input data format is much more convenient in this wrapper.
 */
template<typename T>
class DspFilter {
 public:
  /** Constructor
   */
  DspFilter() : filter_(new Dsp::SimpleFilter<T,1>) {};

  /** Destructor
   */
  virtual ~DspFilter() = default;

  /** Configure the DSP filter.
   *
   *  \param args packed arguments passed to the actual Dsp::SimpleFilter class
   *
   *  Example usage for `Dsp::Butterworth::LowPass<4>` instance of this class;
   *  ~~~{.cpp}
   *    filter.setup(4, samplingFrequency, cornerFrequency);
   *  ~~~
   */
  template<typename... Args>
  void setup(Args... args) {
    if (!filter_) return;
    filter_->reset();
    filter_->setup(args...);
  }

  /** Filter the input data in place, i.e. modifying the data.
   *
   *  \param data vector of data points
   */
  void processInPlace(std::vector<double>& data) {
    if (!filter_) return;
    std::array<double*, 1> wrapped_data {{ data.data() }};
    filter_->process(data.size(), wrapped_data.data());
  }

  /** Filter the (copyied) input data.
   *
   *  \param data vector of data points
   *
   *  This creates a copy of the input data and returns the filtered copy.
   */
  std::vector<double> process(const std::vector<double>& data) {
    if (!filter_) return std::vector<double>(0);
    std::vector<double> new_data(data);
    processInPlace(new_data);
    return new_data;
  }

  /** Access the underlying `Dsp::SimpleFilter` object.
   */
  const std::unique_ptr<Dsp::SimpleFilter<T,1>>& filter() {
    return filter_;
  }

 private:
  std::unique_ptr<Dsp::SimpleFilter<T,1>> filter_; ///< SimpleFilter object
};


/** Discrete 1D filter kernel allowing convolution and correlation.
 *  Similar to image processing this class represents to 1D filter kernel
 *  featuring method to apply them in a vector of data calculating the
 *  convolution or cross-correlation.
 */
class FilterKernel {
 public:
  /** Bounding policies.
   */
  enum Padding {
    ZERO,     ///< Extend borders using 0.
    REPEAT,   ///< Extend borders repeating the last value.
  };

  /** Constructor.
   *
   *  \param kernel Numeric representation of a filter kernel
   *  \param padding specify padding policy at the borders
   *  \throws std::runtime_error in case the filter kernel is of even size
   *
   *  \note The size of the filter kernel needs to be odd by construction. An
   *  exception will be raised otherwise.
   */
  FilterKernel(const std::vector<double>& kernel,
               const Padding padding=Padding::ZERO);

  /** Create a Sobel filter.
   *  Instantiate a FilterKernel using the Sobel operator, i.e. `{2, 0, -2}`
   */
  static FilterKernel Sobel() { return FilterKernel({2, 0, -2}); }

  /** Get a vector representing the filter kernel.
   *
   *  \return vector of kernel elements.
   */
  std::vector<double> getKernel() const;

  /** Return the total size of the kernel.
   *  \return size of the filter kernel
   */
  size_t getKernelSize() const;

  /** Return the half size of the kernel.
   */
  size_t getKernelHalfSize() const;

  /** Return the current padding policy
   */
  Padding getPadding() const;

  /** Set the padding policy
   */
  void setPadding(const Padding padding);

  /** Calculate the convolution of a data vector with the filter kernel.
   *
   *  \param img original data vector
   *  \return convoluted data vector.
   *
   *  This calculates the convolution as defined here:
   *  \f[
   *    (f \star g)(m) = \sum_{i=-r}^r f(i) \cdot g(m-i)
   *  \f]
   *  with \f$ f \f$ being the filter kernel of size \f$ 2r+1 \f$.
   */
  std::vector<double> convolute(const std::vector<double>& img) const;

  /** Calculate the cross-correlation of a data vector with the filter kernel.
   *
   *  \param img original data vector
   *  \param norm calculate the normalized cross-correlation
   *  \return cross-correlation data vector.
   *
   *  This calculates the correlation as defined here:
   *  \f[
   *    (f \circ g)(m) = \sum_{i=-r}^r f(i) \cdot g(m+i)
   *  \f]
   *  with \f$ f \f$ being the filter kernel of size \f$ 2r+1 \f$.
   */
  std::vector<double> correlate(const std::vector<double>& img, 
                                const bool norm=false) const;

  /** Calculate the cross-correlation of a data vector with the filter kernel
   *  using mean offset correction.
   *
   *  \param img original data vector
   *  \return cross-correlation data vector.
   *
   *  This calculates the correlation as defined here:
   *  \f[
   *    (f \circ g)(m) = \sum_{i=-r}^r (f(i) - \bar f)
   *      \cdot (g(m+i) - \frac{1}{2r+1} \sum_{j=-r}^r g(m+j))
   *  \f]
   *  with \f$ f \f$ being the filter kernel of size \f$ 2r+1 \f$ and
   *  \f$ \bar f \f$ its mean value accross all elements.
   */
  std::vector<double> correlate_offsetCorrected(
      const std::vector<double>& img) const;

  /** Apply an offset correction such that mean(vector) = 0
   *
   *  \param first start iterator
   *  \param last end iterator
   */
  static void meanCorrect(const std::vector<double>::iterator& first,
                          const std::vector<double>::iterator& last);

  /** Normalize a vector in place
   *
   *  \param first start iterator
   *  \param last end iterator
   *
   *  Substracts the mean of all value in range and divides by the standard
   *  deviation.
   *  \f[
   *    v(m)' = \frac{ v(m) - \bar v }{ \sigma_v }
   *  \f]
   */
  static void normalize(const std::vector<double>::iterator& first,
                        const std::vector<double>::iterator& last);

  /** Return a subset of a vector symmetric around an iterator.
   *
   *  \param img original vector
   *  \param it element around which the subset window is centered
   *  \param halfSize number of elements to the left and right around the center
   *  \param padding the padding policy to use
   *  \return subset of `img` around `it`
   */
  static std::vector<double> getSubVec(const std::vector<double>& img,
      const std::vector<double>::const_iterator& it,
      const size_t halfSize, const Padding padding);


 private:
  const std::vector<double> kernel_;  ///< the kernel itself
  Padding padding_;
};


} /* namespace fast */


#endif /* __FAST_ROUTINES_H__ */
