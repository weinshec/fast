#include "gmock/gmock.h"
#include "test_common.h"

namespace testing {

std::vector<double> getHistContent(const TH1* hist) {
  std::vector<double> v;
  for (int i = 0; i < hist->GetNbinsX(); i++)
    v.push_back(hist->GetBinContent(i+1));
  return v;
}

std::vector<double> getHistContent(const TH1 &hist) {
  return getHistContent(&hist);
}

std::vector<double> getHistBinning(const TH1* hist, bool upperBound) {
  std::vector<double> v;
  auto N = hist->GetNbinsX();

  for (int i = 0; i < N; i++)
    v.push_back(hist->GetBinLowEdge(i+1));
  if (upperBound)
    v.push_back(hist->GetBinLowEdge(N+1));

  return v;
}

std::vector<double> getHistBinning(const TH1 &hist, bool upperBound) {
  return getHistBinning(&hist, upperBound);
}

} /* namespace testing */
