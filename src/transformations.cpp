#include "transformations.h"
#include "pulsefct.h"

#include "TFile.h"


using namespace fast;


const std::unique_ptr<TRandom3> ParameterType::rnd_(new TRandom3(0));


FixedValue::FixedValue(double value)
    : value_(value) { }

double FixedValue::value() const {
  return value_;
}

std::string FixedValue::to_string() const {
  std::stringstream ss;
  ss << "fixed:" << value_;
  return ss.str();
}


GausValue::GausValue(double mean, double stddev)
    : mean_(mean), stddev_(stddev) { }

double GausValue::value() const {
  return rnd_->Gaus(mean_, stddev_);
}

std::string GausValue::to_string() const {
  std::stringstream ss;
  ss << "gaus:" << mean_ << ":" << stddev_;
  return ss.str();
}


FlatValue::FlatValue(double low, double high)
    : low_(low), high_(high) { }

double FlatValue::value() const {
  return rnd_->Uniform(low_, high_);
}

std::string FlatValue::to_string() const {
  std::stringstream ss;
  ss << "flat:" << low_ << ":" << high_;
  return ss.str();
}


HistValue::HistValue(const TH1D& hist)
    : hist_( (TH1D*) hist.Clone() ) { }

double HistValue::value() const {
  if (!hist_) return 0;
  return hist_->GetRandom();
}

std::string HistValue::to_string() const {
  return "hist:" + std::string(hist_->GetName());
}


Parameter Parameter::fromJSON(const std::string& name,
                              const utils::ConfigParser::JsonConfig& json) {
  try {
    std::string type = json["type"];
    if (type == "fixed")
      return Parameter::type<FixedValue>(name,
                                         json["value"].get<double>());
    else if (type == "gaus")
      return Parameter::type<GausValue>(name,
                                        json["mean"].get<double>(),
                                        json["stddev"].get<double>());
    else if (type == "flat")
      return Parameter::type<FlatValue>(name,
                                        json["low"].get<double>(),
                                        json["high"].get<double>());
    else
      throw std::runtime_error("Unkown parameter type");
  } catch(...) {
    utils::LogError("Parameter") << "Cannot parse parameter from JSON config"
                                 << json;
    throw std::runtime_error("Error while parsing JSON config");
  }
}

double Parameter::value() {
  currentValue_ = strategy_ ? strategy_->value() : 0;
  return currentValue_;
}

double Parameter::value(GeneratorRecord &record) {
  auto val = value();
  record.addTruth(getName(), val);
  return val;
}

std::string Parameter::getName() const {
  return name_;
}

std::string Parameter::to_string() const {
  return getName() + "(" + strategy_->to_string() + ")";
}

Parameter::Parameter(const std::string& name) : name_(name) { }

void Parameter::setTree(const std::shared_ptr<TTree>& tree) {
  tree->Branch(getName().c_str(), &currentValue_);
}


void Transformation::setTree(const std::shared_ptr<TTree>& tree) {
  for (auto& entry : pmap_) entry.second.setTree(tree);
}


AddScalar::AddScalar(const Parameter& addend) {
  pmap_.emplace(std::make_pair("addend",addend));
}

AddScalar& AddScalar::operator() (GeneratorRecord& record) {
  record += pmap_.at("addend").value(record);
  return *this;
}

std::string AddScalar::to_string() const {
  return "<AddScalar: " + pmap_.at("addend").to_string() + ">";
}


Multiply::Multiply(const Parameter &factor) {
  pmap_.emplace(std::make_pair("factor",factor));
}

Multiply& Multiply::operator() (GeneratorRecord& record) {
  record *= pmap_.at("factor").value(record);
  return *this;
}

std::string Multiply::to_string() const {
  return "<Multiply: " + pmap_.at("factor").to_string() + ">";
}


TimeShift::TimeShift(const Parameter &offset) {
  pmap_.emplace(std::make_pair("offset",offset));
}

TimeShift& TimeShift::operator() (GeneratorRecord& record) {
  int offset = time2Offset(record);
  if (offset > 0)
    record >> offset;
  else
    record << std::abs(offset);
  return *this;
}

std::string TimeShift::to_string() const {
  return "<TimeShift: " + pmap_.at("offset").to_string() + ">";
}

int TimeShift::time2Offset(GeneratorRecord& record) {
  double offset = pmap_.at("offset").value(record);
  return std::trunc( offset * record.getSamplingFreq());
}


AddWhiteNoise::AddWhiteNoise(const Parameter &stddev, const unsigned int seed)
    : rnd_(new TRandom3(seed)) {
  pmap_.emplace(std::make_pair("stddev",stddev));
}

AddWhiteNoise& AddWhiteNoise::operator() (GeneratorRecord& record) {
  auto stddev = pmap_.at("stddev").value(record);

  std::vector<double> noise(record.size());
  for (auto& v : noise)
    v = rnd_->Gaus(0, stddev);
  record += noise;

  return *this;
}

std::string AddWhiteNoise::to_string() const {
  return "<AddWhiteNoise: " + pmap_.at("stddev").to_string() + ">";
}

unsigned int AddWhiteNoise::getSeed() const {
  return rnd_->GetSeed();
}

void AddWhiteNoise::setSeed(const unsigned int seed) {
  rnd_->SetSeed(seed);
}


AddSpectralNoise::AddSpectralNoise(const TH1D& spectrum,
                                   const SpectrumType type)
    : idft_(new IDFT(spectrum, type)) {}

AddSpectralNoise::AddSpectralNoise(const TH1D* spectrum,
                                   const SpectrumType type)
    : idft_(new IDFT(spectrum, type)) {}

AddSpectralNoise::AddSpectralNoise(const std::string& filename,
                                   const std::string& histName,
                                   const SpectrumType type)
{
  TFile file(filename.c_str(), "READ");
  if (file.IsZombie())
    utils::LogError("AddSpectralNoise") << "Cannot open file " + filename;

  auto hist = (TH1*) file.Get(histName.c_str());
  if (!hist)
    utils::LogError("AddSpectralNoise") << "Cannot read spectrum " + histName;

  idft_.reset(new IDFT(hist, type));
  file.Close();
}

AddSpectralNoise& AddSpectralNoise::operator() (GeneratorRecord& record) {
  if (!requirementsMatch(record)) return *this;

  auto rndHist = idft_->drawSample();

  std::vector<double> noise(record.size());
  for (std::size_t i = 0; i < record.size(); ++i)
    noise.at(i) = rndHist.GetBinContent(i+1);
  record += noise;

  return *this;
}

std::string AddSpectralNoise::to_string() const {
  std::stringstream ss;
  ss << "<AddSpectralNoise: (IDFT:" << idft_->getSamplingFreq() << "@"
                                    << idft_->getFreqResolution() << ")>";
  return ss.str();
}

bool AddSpectralNoise::requirementsMatch(const GeneratorRecord& record) const {
  if (!idft_) {
    utils::LogWarning("AddSpectralNoise") << "No valid spectrum loaded!";
    return false;
  }
  if (record.getSamplingFreq() != idft_->getSamplingFreq()) {
    utils::LogWarning("AddSpectralNoise")
        << "Sampling frequencies do not match!";
    return false;
  }
  if (record.size() > idft_->getSampleSize()) {
    utils::LogWarning("AddSpectralNoise")
        << "Unsufficient frequency resolution!";
    return false;
  }
  return true;
}


AddPhotonPulse::AddPhotonPulse(const Parameter &amplitude,
                               const Parameter &tRise,
                               const Parameter &tFall,
                               const Parameter &tOffset) {
  pmap_.emplace(std::make_pair("amplitude",amplitude));
  pmap_.emplace(std::make_pair("tRise",tRise));
  pmap_.emplace(std::make_pair("tFall",tFall));
  pmap_.emplace(std::make_pair("tOffset",tOffset));

  sp_fPhoton_ = std::make_shared<TF1>(
      "fPhoton", fast::pulsefct::photon, 0., 1., 5);
  sp_fPhoton_->SetParameters( pmap_.at("amplitude").value(),
                              pmap_.at("tRise").value(),
                              pmap_.at("tFall").value(),
                              pmap_.at("tOffset").value(),
                              0);
  sp_fPhoton_->SetParNames(amplitude.getName().c_str(),
                           tRise.getName().c_str(),
                           tFall.getName().c_str(),
                           tOffset.getName().c_str(),
                           "DCoffset");
}

AddPhotonPulse& AddPhotonPulse::operator() (GeneratorRecord& record) {
  auto samplingFreq = record.getSamplingFreq();
  auto photonY = std::vector<double>(record.size());

  sp_fPhoton_->SetParameters( pmap_.at("amplitude").value(record),
                              pmap_.at("tRise").value(record),
                              pmap_.at("tFall").value(record),
                              pmap_.at("tOffset").value(record),
                              0);

  for (std::size_t i = 0; i < photonY.size(); i++)
    photonY.at(i) = sp_fPhoton_->Eval((double)i/samplingFreq);

  record += photonY;
  return *this;
}

std::string AddPhotonPulse::to_string() const {
  std::string s{"<AddPhotonPulse:"};
  for (const auto& p : pmap_)
    s += " " + p.second.to_string();
  return s + ">";
}

double AddPhotonPulse::getPeakPosition() const {
  auto tp = sp_fPhoton_->GetParameter(1);
  auto tm = sp_fPhoton_->GetParameter(2);
  auto dt = sp_fPhoton_->GetParameter(3);
  return pulsefct::tmin_analytic(tp, tm) + dt;
}

std::shared_ptr<TF1> AddPhotonPulse::getFunction() const {
  return sp_fPhoton_;
}
