#ifndef __FAST_DEFS_H__
#define __FAST_DEFS_H__


namespace fast {


/** Binary channel definition. ChA or ChB.
 *  Used to select data from either readout channel.
 */
enum class Channel {
  ChA,    ///< Readout channel A
  ChB,    ///< Readout channel B
};


/** Set of parameters describing the pulse shape.
 */
struct PulseShape {
  double amplitude;   ///< amplitude given in mV
  double tRise;       ///< pulse rise time (falling flank) in seconds
  double tFall;       ///< pulse fall time (relaxing flank) in seconds

  /** Standard definition of -70 mV signal photon.
   *
   *  | parameter | value   |
   *  | --------- | ------- |
   *  | amplitude | -70 mV  |
   *  | tRise     | 70 ns   |
   *  | tFall     | 1.53 us |
   */
  static constexpr PulseShape PHOTON_70mV() {
    return PulseShape{-0.07, 70e-9, 1.53e-6};
  }

  /** Standard definition of -55 mV signal photon.
   *
   *  | parameter | value   |
   *  | --------- | ------- |
   *  | amplitude | -55 mV  |
   *  | tRise     | 70 ns   |
   *  | tFall     | 1.53 us |
   */
  static constexpr PulseShape PHOTON_55mV() {
    return PulseShape{-0.055, 70e-9, 1.53e-6};
  }

  /** Standard definition of -35 mV signal photon.
   *
   *  | parameter | value   |
   *  | --------- | ------- |
   *  | amplitude | -35 mV  |
   *  | tRise     | 70 ns   |
   *  | tFall     | 1.53 us |
   */
  static constexpr PulseShape PHOTON_35mV() {
    return PulseShape{-0.035, 70e-9, 1.53e-6};
  }
};


} /* namespace fast */



#endif /* __FAST_DEFS_H__ */
