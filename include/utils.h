#ifndef __FAST_UTILS_H__
#define __FAST_UTILS_H__

#include <string>

#include "TGraph.h"

#include "defs.h"
#include "json/json.hpp"


namespace fast {
namespace utils {


/** Print library version containing git commit information
 */
void printLibVersion();


/** Convert a non-string type value to string
 *
 *  \param t value to convert
 *
 *  This uses std::to_string() if possible.
 */
template<typename T>
std::string to_string(const T t) {
  return std::to_string(t);
}

/** Specialization of to_string for Channel type values.
 */
template<>std::string to_string<Channel>(const Channel ch);

/** Specialization of to_string for std::strings.
 */
template<>std::string to_string<std::string>(const std::string s);


template<typename T>
std::string add_suffix(const std::string& s, const T suffix) {
  auto str_suffix = to_string(suffix);
  if (str_suffix != "")
    return s + "_" + str_suffix;
  else
    return s;
}

template<typename T>
std::string add_prefix(const T prefix, const std::string& s) {
  auto str_prefix = to_string(prefix);
  if (str_prefix != "")
    return to_string(prefix) + "_" + s;
  else
    return s;
}

template<typename T, typename U>
std::string add_prefix_suffix(const T pre, const std::string& s, const U suf) {
  return add_suffix<U>(add_prefix<T>(pre, s), suf);
}


/** Function object modifying a given string.
 */
using StringMorph = std::function<std::string(const std::string&)>;

/** Function object appending a Channel suffix
 */
StringMorph channel_suffix(const Channel& ch);

/** Function object appending a number suffix
 */
StringMorph number_suffix(const int n);

/** Function object prepending a name prefix and appending a channel suffix.
 */
StringMorph puPrefix_chSuffix(const std::string& pre, const Channel suf);


/** Retrieve a system environment variable
 *
 *  \param variable environment variable
 *  \return empty string if environmental variable not found
 *
 *  \note Just us the bare name, i.e. no leading "$" or surrounding brackets
 */
std::string getEnv(const std::string& variable);

/** Expand all environment variables in a strings
 *
 *  \param s string containing environmental variables like "${VAR}"
 *
 *  This uses a regular expression match the "$" sign followed by any sequence
 *  of characters enclosed in brackets, e.g. "${HOME}"
 */
void autoExpandEnv(std::string& s);

/** Expand all environment variables and return altered copy
 *
 *  \param s string containing environmental variables like "${VAR}"
 *  \return copy with expanded env variables
 *
 *  This uses a regular expression match the "$" sign followed by any sequence
 *  of characters enclosed in brackets, e.g. "${HOME}"
 */
std::string expandEnv(const std::string& s);


/** Create a vector of consecutive points in time.
 *
 *  \param N length of the vector
 *  \param fs sampling frequency, i.e. inverse spacing between points
 *  \return vector of time points
 */
template<typename T>
std::vector<T> sampleTime(const size_t N, const double fs) {
  std::vector<T> time_(N);
  for (size_t i = 0; i < N; ++i)
    time_.at(i) = static_cast<T>(i / fs);
  return time_;
}

/** Create a TGraph from a data vector.
 *
 *  \param data data points to draw
 *  \param fs sampling frequency
 *  \param name name and title of the newly created graph
 *  \return TGraph showing the data
 */
template<typename T>
TGraph createGraph(const std::vector<T>& data, const double fs,
                   const std::string& name="graph") {
  std::vector<T> time_ = sampleTime<T>(data.size(), fs);
  TGraph g(data.size(), time_.data(), data.data());
  g.SetNameTitle(name.c_str(), name.c_str());
  return g;
}

/** Create a TGraph from a data vector.
 *
 *  \param data data points to draw
 *  \param fs sampling frequency
 *  \param name name and title of the newly created graph
 *  \return TGraph showing the data
 */
template<typename T>
TGraph createGraph(std::vector<T>* data, const double fs,
                   const std::string& name="graph") {
  if (data) return createGraph(*data, fs, name);
  else return TGraph();
}


/** Parse text based configuration
 *  Helper class providing static methods to parse configuration settings from
 *  text based file formats, e.g. JSON.
 */
class ConfigParser {
 public:
  using JsonConfig = nlohmann::json;  ///< alias for external package JSON obj.

  /** Parse configuration from a JSON file.
   *
   *  \param file path to the JSON file.
   *  \return deserialized Json object
   *
   *  \note The return Json object will be empty in case the input file could
   *  not be opened.
   */
  static JsonConfig parseJson(const std::string& file);

  /** Update a JsonConfig object with another one.
   *
   *  \param config Json object to update
   *  \param other Json object containing the new values
   *
   *  This overwrites exisitng values in `config` with the values from `other`.
   *  If `other` contains values not existent in `config`, these will be added.
   */
  static void updateJson(JsonConfig& config, const JsonConfig& other);
};


/** Log levels available to set for Logger
 */
enum class LogLevel {
  QUIET   = 0,    ///< no output
  ERROR   = 1,    ///< critical error message only
  WARNING = 2,    ///< non-critical warning messages
  INFO    = 3,    ///< more verbose info messages
  DEBUG   = 4,    ///< full debug messages
};

/** Log message dispatcher singelton class.
 *  There is only one (automatically) created instance of this class availbale.
 *  You can access is calling the static `instance()` method.
 */
class Logger {
 public:
  /** Deleted constructor.
   */
  Logger(const Logger&) = delete;

  /** Deleted constructor.
   */
  Logger(Logger&&) = delete;

  /** Destructor.
   */
  virtual ~Logger() {};

  /** Get the singelton instance of the Logger class.
   *
   *  \return pointer to the singleton instance.
   */
  static const std::unique_ptr<Logger>& instance();

  /** Set the log message level inclusively.
   *
   *  \param loglevel The inclusive level of log message to output.
   */
  void setLevel(const LogLevel loglevel);

  /** Method to report a log message including a level and a context.
   *
   *  \param level Log level of the message to report
   *  \param context Context aka. the sender of this message
   *  \param t the actual message to report
   *
   *  This method is templated in order to support all types of streams
   *  processible by `std::ostream`, i.e. t gets stream to an `std::ostream`
   *  member via the `<<` operator.
   *
   *  \note For consistent usage, do not use the method directly, rather use
   *  one of the instances of `LogSender`.
   */
  template<typename T>
  void report(const LogLevel level, const std::string& context,
              const T& t) {
    if (level > level_) return;

    switch(level) {
      case LogLevel::QUIET:
        return;
      case LogLevel::ERROR:
        os_ << "[ERROR]"; break;
      case LogLevel::WARNING:
        os_ << "[WARNING]"; break;
      case LogLevel::INFO:
        os_ << "[INFO]"; break;
      case LogLevel::DEBUG:
        os_ << "[DEBUG]"; break;
    }
    os_ << ":<" << context << "> " << t << std::endl;
  };

 private:
  LogLevel level_;    ///< log message level to report
  std::ostream os_;   ///< stream to pass the messages to

  /** Constructor.
   *
   *  \param level of messages to report
   */
  Logger(const LogLevel level=LogLevel::INFO);
  static std::unique_ptr<Logger> instance_;   ///< pointer to the singelton
};

/** Convenience class calling the report method of `Logger`.
 */
template<LogLevel L>
class LogSender {
 public:
  /** Constructor.
   *
   *  \param context The emitting context of this logmessage
   */
  LogSender(const std::string& context) : context_(context) {};

  /** Stream operator for easy reporting a message or stream.
   *
   *  \param t the message or stream to pass.
   *  \return reference to this instance for chaining the operator.
   */
  template<typename T>
  LogSender& operator<< (const T& t) {
    Logger::instance()->report(level_, context_, t);
    return *this;
  };

 private:
  const LogLevel level_ = L;    ///< log level of this message sender
  const std::string context_;   ///< emitting context of this message sender
};

using LogError   = LogSender<LogLevel::ERROR>;    ///< Error message
using LogWarning = LogSender<LogLevel::WARNING>;  ///< Warning message
using LogInfo    = LogSender<LogLevel::INFO>;     ///< Info message
using LogDebug   = LogSender<LogLevel::DEBUG>;    ///< Debug message


} /* namespace utils */
} /* namespace fast */



#endif /* __FAST_UTILS_H__ */
