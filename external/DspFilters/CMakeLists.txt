#______________________________________________________________________________
#                                                                         FILES
set(headers Bessel.h
            Biquad.h
            Butterworth.h
            Cascade.h
            ChebyshevI.h
            ChebyshevII.h
            Common.h
            Custom.h
            Design.h
            Dsp.h
            Elliptic.h
            Filter.h
            Layout.h
            Legendre.h
            MathSupplement.h
            Params.h
            PoleFilter.h
            RBJ.h
            RootFinder.h
            SmoothedFilter.h
            State.h
            Types.h
            Utilities.h)
set(sources Bessel.cpp
            Biquad.cpp
            Butterworth.cpp
            Cascade.cpp
            ChebyshevI.cpp
            ChebyshevII.cpp
            Custom.cpp
            Design.cpp
            Elliptic.cpp
            Filter.cpp
            Legendre.cpp
            Param.cpp
            PoleFilter.cpp
            RBJ.cpp
            RootFinder.cpp
            State.cpp)


#______________________________________________________________________________
#                                                                       LIBRARY
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
add_library(dspfilters SHARED ${sources})


#______________________________________________________________________________
#                                                                       INSTALL
install(TARGETS dspfilters
        EXPORT ${PROJECT_NAME}LibraryDepends
        LIBRARY DESTINATION lib COMPONENT libs
        RUNTIME DESTINATION bin COMPONENT libs)
install(FILES ${headers}
        DESTINATION include/DspFilters COMPONENT dev)
