#ifndef __GENERATORRECORD_H__
#define __GENERATORRECORD_H__

#include <map>
#include <memory>
#include <stdexcept>
#include <vector>

#include "TH1D.h"


namespace fast {

/** Basic dataset for MonteCarlo simulation.
 *  Represents a set of data points generated during simulation.
 *  GeneratorRecords get instantiated with a flat sample of datapoints and
 *  might be modified by Transformations.
 */
class GeneratorRecord {

 friend class Generator;

 public:
  /** Constructor.
   *
   *  \param size of the dataset
   *  \param samplingFreq sampling frequency in Hz
   *
   *  Creates a flat data samples.
   */
  GeneratorRecord(const std::size_t size, const double samplingFreq);

  /** Get the size of the data sample
   *
   *  \return size of the data sample
   */
  std::size_t size() const;

  /** Get the raw data sample
   *
   *  \return vector of raw data points
   */
  std::vector<double> getData() const;

  /** Get the sampling frequency
   *
   *  \return sampling frequency in Hz
   */
  double getSamplingFreq() const;

  /** Get the truth map of applied transformation parameters.
   *
   *  \return thruth map of transformation parameters
   *
   *  Each Transformation applied to this record saves its parameters in this
   *  map with the parameter names as keys.
   */
  std::map<std::string,double> getTruthMap() const;

  /** Save a parameter in the truth map.
   *
   *  \param name of the parameter
   *  \param value of the parameter
   */
  void addTruth(const std::string &name, const double value);

  /** Append another record to this one.
   *
   *  \param sample record to append
   */
  void append(const GeneratorRecord &sample);

  /** Append a vector of data points.
   *
   *  \param data points to append
   */
  void append(const std::vector<double> &data);

  /** Add a scalar value to all data points
   */
  GeneratorRecord& operator+= (const double rhs);

  /** Add a vector pointwise to the data points
   */
  GeneratorRecord& operator+= (const std::vector<double>& rhs);

  /** Multiply all data points by a scalar value
   */
  GeneratorRecord& operator*= (const double rhs);

  /** Shift the record to the left. Pads with zeros.
   */
  GeneratorRecord& operator<< (const std::size_t rhs);

  /** Shift the record to the right. Pads with zeros.
   */
  GeneratorRecord& operator>> (const std::size_t rhs);

  /** Create a histogram of the current dataset
   *
   *  return histgram of the data set
   */
  TH1D makeHistogram() const;

 private:
  double              fs_;                  ///< sampling frequency in Hz
  std::vector<double> data_;                ///< data points
  std::map<std::string, double> truthMap_;  ///< truth map for parameter values
};

} /* namepspace fast */

#endif /* __GENERATORRECORD_H__ */
