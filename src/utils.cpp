#include "utils.h"
#include "version.h"

#include <cstdlib>
#include <fstream>
#include <regex>


namespace fast {
namespace utils {


void printLibVersion() {
  std::cout << "--------------------------" << std::endl
            << "FAST - version information" << std::endl
            << "--------------------------" << std::endl
            << "build  : " << GIT_VERSION   << std::endl
            << "commit : " << GIT_HASH      << std::endl
            << std::endl;
}


template<>std::string to_string<Channel>(const Channel ch) {
  switch (ch) {
    case Channel::ChA:
      return "ChA";
    case Channel::ChB:
      return "ChB";
    default:
      return "";
  }
}

template<>std::string to_string<std::string>(const std::string s) {
  return s;
}


StringMorph channel_suffix(const Channel ch) {
  return [ch](const std::string& s){ return add_suffix<Channel>(s, ch); };
}

StringMorph number_suffix(const int n) {
  return [n](const std::string& s){ return add_suffix<const int>(s, n); };
}

StringMorph puPrefix_chSuffix(const std::string& pre, const Channel suf) {
  return [pre, suf](const std::string& s){
    return add_prefix_suffix<std::string,Channel>(pre, s, suf);
  };
}


std::string getEnv(const std::string& variable) {
  char* val = std::getenv( variable.c_str() );
  return val == nullptr ? std::string("") : std::string(val);
}

void autoExpandEnv(std::string& s) {
  std::regex env_regex( "\\$\\{([^}]+)\\}" );
  std::smatch match;
  while( std::regex_search(s, match, env_regex) )
    s.replace( match.position(0), match.length(0), getEnv(match.str(1)) );
}

std::string expandEnv(const std::string& s) {
  std::string expanded(s);
  autoExpandEnv(expanded);
  return expanded;
}


using JsonConfig = ConfigParser::JsonConfig;

JsonConfig ConfigParser::parseJson(const std::string& file) {
  JsonConfig config;
  std::ifstream inputFile(file);
  if (!inputFile.good()) { 
    std::cout << "Cannot open config file" << std::endl;
  } else {
    config << inputFile;
  }
  return config;
}

void ConfigParser::updateJson(JsonConfig& config, const JsonConfig& other) {
  for (auto it = other.begin(); it != other.end(); ++it) {
    if (it.value().is_object()) updateJson(config[it.key()], it.value());
    else config[it.key()] = it.value();
  }
}


std::unique_ptr<Logger> Logger::instance_ = nullptr;

const std::unique_ptr<Logger>& Logger::instance() {
  if (!instance_) instance_ = std::unique_ptr<Logger>(new Logger);
  return instance_;
}

void Logger::setLevel(const LogLevel level) {
  level_ = level;
}

Logger::Logger(const LogLevel level) : level_(level), os_(std::cout.rdbuf()) {}


} /* namespace utils */
} /* namespace fast */
