#include "spectrum.h"
#include <stdexcept>
#include "TMath.h"
#include "TVirtualFFT.h"


using namespace fast;


WindowFct::WindowFct(unsigned int N, const WindowFct::Type& type)
    : kType(type), weights_(N, 1.0), sumw_(0.0), sumw2_(0.0) {
  setWeights();
}

WindowFct::Type WindowFct::getType() const {
  return kType;
}

std::size_t WindowFct::size() const {
  return weights_.size();
}

std::vector<double> WindowFct::getWeights() const {
  return weights_;
}

double WindowFct::getSumW() const {
  return sumw_;
}

double WindowFct::getSumW2() const {
  return sumw2_;
}

void WindowFct::applyOn(TH1* hist) {
  if (hist->GetNbinsX() != (int) size())
    throw std::runtime_error(
        "Cannot apply windows due to incompatible sizes");

  for (std::size_t i = 0; i < size(); i++)
    hist->SetBinContent(i+1, hist->GetBinContent(i+1) * weights_.at(i));
}

void WindowFct::setWeights() {
  double (*weight_fct)(unsigned int, unsigned int) = nullptr;

  switch (kType) {
    case RECT:
      weight_fct = weight_rect;
      break;
    case HANNING:
      weight_fct = weight_hanning;
      break;
    case HFT116D:
      weight_fct = weight_hft116d;
      break;
    case FTNI:
      weight_fct = weight_ftni;
      break;
    case BLAHAR:
      weight_fct = weight_blahar;
      break;
  }

  double w;
  for (std::size_t i = 0; i < size(); i++) {
    w = weight_fct(size(), i);
    weights_.at(i) = w;
    sumw_       += w;
    sumw2_      += w*w;
  }
}

double WindowFct::weight_rect(unsigned int N, unsigned int i) {
  return 1.0 + 0.0*(N+i);
}

double WindowFct::weight_hanning(unsigned int N, unsigned int i) {
  return 0.5 * (1.0 - TMath::Cos(2.*TMath::Pi() * i / N));
}

double WindowFct::weight_hft116d(unsigned int N, unsigned int i) {
  auto z = 2. * TMath::Pi() * i / N;
  return 1. - 1.9575375*TMath::Cos(1.*z)
            + 1.4780705*TMath::Cos(2.*z)
            - 0.6367431*TMath::Cos(3.*z)
            + 0.1228389*TMath::Cos(4.*z)
            - 0.0066288*TMath::Cos(5.*z);
}

double WindowFct::weight_ftni(unsigned int N, unsigned int i) {
  auto z = 2. * TMath::Pi() * i / N;
  return   0.2810639
         - 0.5208972*TMath::Cos(1.*z)
         + 0.1980399*TMath::Cos(2.*z);
}

double WindowFct::weight_blahar(unsigned int N, unsigned int i) {
  auto z = 2. * TMath::Pi() * i / N;
  return   0.35875
         - 0.48829*TMath::Cos(1.*z)
         + 0.14128*TMath::Cos(2.*z)
         - 0.01168*TMath::Cos(3.*z);
}



DFT::DFT(const TH1* sample, const WindowFct::Type type) {
  sample_      = std::unique_ptr<TH1>( (TH1*)sample->Clone("dftSample") );
  splitLength_ = sample_->GetNbinsX();
  windowFct_   = std::make_shared<WindowFct>(getN(), type);
  sample_->SetDirectory(0);
}

DFT:: DFT(const TH1 &sample, const WindowFct::Type type) : DFT(&sample, type){}

DFT::DFT(const std::vector<double>& volts, double fs,
         const WindowFct::Type type) {
  auto N  = volts.size();
  auto dt = 1. / fs;
  sample_ = std::unique_ptr<TH1>(
      new TH1D("dftSample", "dftSample", N, 0, N*dt));
  sample_->SetDirectory(0);

  for (std::size_t i = 0; i < N; i++)
    sample_->SetBinContent(i+1, volts.at(i));

  splitLength_ = sample_->GetNbinsX();
  windowFct_   = std::make_shared<WindowFct>(getN(), type);
}

std::size_t DFT::getN() const {
  return splitLength_;
}

double DFT::getSamplingFreq() const {
  return 1. / sample_->GetBinWidth(1);
}

double DFT::getTimeSpan() const {
  return sample_->GetBinLowEdge( getN() + 1 );
}

double DFT::getFreqResolution() const {
  return getSamplingFreq() / getN();
}

const std::shared_ptr<WindowFct> DFT::getWindowFct() const {
  return windowFct_;
}

void DFT::setWindowFctType(const WindowFct::Type type) {
  windowFct_ = std::make_shared<WindowFct>(getN(), type);
}

void DFT::setFreqResolution(const double target) {
  double fres_eff;
  splitLength_ = calcSplitting(getSamplingFreq(), target, fres_eff);

  if ((int)splitLength_ > sample_->GetNbinsX())
    throw std::runtime_error("Frequency Resolution not achievable");

  windowFct_ = std::make_shared<WindowFct>(getN(), windowFct_->getType());
}

bool DFT::getDCremoval() const {
  return removeDC_;
}

void DFT::setDCremoval(const bool removeDC) {
  removeDC_ = removeDC;
}

TH1D DFT::spectrum(const SpectrumType type, const std::string& name,
                   const std::string& title) const {
  auto spectrum = compSquareMagnitudes();
  auto S1 = windowFct_->getSumW();
  auto S2 = windowFct_->getSumW2();
  auto fs = getSamplingFreq();

  switch (type) {
    case SpectrumType::PS:
      spectrum.Scale(2./(S1*S1));
      spectrum.SetNameTitle("hPS", "hPS");
      break;
    case SpectrumType::PSD:
      spectrum.Scale(2./(fs*S2));
      spectrum.SetNameTitle("hPSD", "hPSD");
      break;
    case SpectrumType::LS:
      spectrum.Scale(2./(S1*S1));
      histSqrt(spectrum);
      spectrum.SetNameTitle("hLS", "hLS");
      break;
    case SpectrumType::LSD:
      spectrum.Scale(2./(fs*S2));
      histSqrt(spectrum);
      spectrum.SetNameTitle("hLSD", "hLSD");
      break;
    default:
      throw std::runtime_error("Unkown spectrum type");
  }

  if (name != "" )  spectrum.SetName(name.c_str());
  if (title != "" ) spectrum.SetTitle(title.c_str());

  spectrum.SetDirectory(0);
  return spectrum;
}

std::size_t DFT::calcSplitting(double fs, double fres, double& feff) {
  auto N_effective = calcEffFFTWsize( fs / fres );
  feff = fs / N_effective;

  return N_effective;
}

std::size_t DFT::calcEffFFTWsize(std::size_t N) {
  std::size_t trial = N;
  while (true) {
    for (unsigned int i = 2; i <= 13; i++) {
      while ( trial % i == 0)
        trial /= i;
    }
    if (trial != 1) trial = --N;
    else return N;
  }
}

std::size_t DFT::getNumNonRedundantBins() const {
  return std::floor(getN() / 2. + 1.);
}

TH1D DFT::compSquareMagnitudes() const {
  TH1D sqMag("hSqMag", "hSqMag", getNumNonRedundantBins(),
             0, getSamplingFreq()/2. + getFreqResolution());

  auto nSubSamples = (unsigned int) std::floor(sample_->GetNbinsX()/getN());

  TH1D subHist("subHist", "subHist", getN(), 0, 1);
  for (std::size_t n = 0; n < nSubSamples; n++) {
    for (std::size_t i = 0; i < getN(); i++)
      subHist.SetBinContent(i+1, sample_->GetBinContent(i+1+n*getN()));

    if (removeDC_) removeDCcomponent(subHist);
    windowFct_->applyOn(&subHist);
    auto rawMag = subHist.FFT(nullptr,"MAG R2C");
    for (std::size_t i = 0; i < getNumNonRedundantBins(); i++) {
      auto mag = rawMag->GetBinContent(i+1);
      sqMag.AddBinContent(i+1, mag*mag/nSubSamples);
    }
    delete rawMag;
  }

  return sqMag;
}

void DFT::histSqrt(TH1D &hist) const {
  for (int i = 0; i < hist.GetNbinsX(); i++)
    hist.SetBinContent( i+1, TMath::Sqrt(hist.GetBinContent(i+1)) );
}

void DFT::removeDCcomponent(TH1D &hist) const {
  double DC = hist.Integral() / hist.GetNbinsX();
  for (auto i = 0; i < hist.GetNbinsX(); ++i)
    hist.AddBinContent(i+1, -DC);
}



IDFT::IDFT(const TH1* spectrum, const SpectrumType specType)
    : specType_(specType),
      spectrum_((TH1*)spectrum->Clone("IDFT_PS")),
      rnd_(new TRandom3) {
  spectrum_->SetDirectory(0);
  rnd_->SetSeed(0);
}

IDFT::IDFT(const TH1 &spectrum, const SpectrumType specType)
   : IDFT(&spectrum, specType) {}

double IDFT::getNyquistFreq() const {
  return spectrum_->GetBinLowEdge( spectrum_->GetNbinsX() );
}

double IDFT::getSamplingFreq() const {
  return 2.*getNyquistFreq();
}

std::size_t IDFT::getSampleSize() const {
  return 2. * getNyquistFreq() / getFreqResolution();
}

double IDFT::getFreqResolution() const {
  return spectrum_->GetBinWidth(1);
}

TH1D IDFT::drawSample() const {
  auto sampleSize = getSampleSize();
  std::vector<double> re(sampleSize), im(sampleSize);

  // Not necessary to store the upper redundant half, FFTW discards it anyway
  for (int i = 1; i < spectrum_->GetNbinsX(); i++)
    drawRndPhase( spec2mag(spectrum_->GetBinContent(i+1)), re[i], im[i] );

  // Treat DC and Nyq component special
  re.front() = im.front() = 0;
  re.at(spectrum_->GetNbinsX()-1) = im.at(spectrum_->GetNbinsX()-1) = 0;

  auto rawSample = backTransform(sampleSize, re, im);
  TH1D sample("hIDFTsample", "hIDFTsample",
              sampleSize, 0, sampleSize / getSamplingFreq());
  for (std::size_t i = 0; i < sampleSize; i++ )
    sample.SetBinContent(i+1, rawSample->GetBinContent(i+1));

  sample.SetDirectory(0);
  return sample;
}

void IDFT::drawRndPhase(double mag, double &re, double &im) const {
  auto phase = rnd_->Uniform(-TMath::Pi(), TMath::Pi());
  re = mag * TMath::Cos(phase);
  im = mag * TMath::Sin(phase);
}

void IDFT::setRandomSeed(const unsigned int seed) {
  rnd_->SetSeed(seed);
};

std::unique_ptr<TH1> IDFT::backTransform(int N, const std::vector<double>& re,
                                         const std::vector<double>& im) const {
  TVirtualFFT* fft_back = TVirtualFFT::FFT(1, &N, "C2R M K");
  fft_back->SetPointsComplex(&re[0], &im[0]);
  fft_back->Transform();

  TH1* backTransform = nullptr;
  backTransform = TH1::TransformHisto(fft_back, backTransform, "RE");
  backTransform->Scale(1./backTransform->GetNbinsX());

  delete fft_back;
  return std::unique_ptr<TH1> (backTransform);
}

double IDFT::spec2mag(const double specBin) const {
  /* Don't care about the window function used during FFT since the spectrum
   * is already correctely scaled and its type is known. Use S1 and S2 as in a
   * RECT window (i.e. S1 = S2 = N) just for easier compatibility with the
   * formulas used to scale the FFT.
   */
  auto S1 = getSampleSize(); // = N
  auto S2 = getSampleSize(); // = N

  switch (specType_) {
    case SpectrumType::PS:
      return TMath::Sqrt(specBin * S1 * S1 / 2.);
    case SpectrumType::PSD:
      return TMath::Sqrt(specBin * S2 * getSamplingFreq() / 2.);
    case SpectrumType::LS:
      return specBin * S1 / TMath::Sqrt(2.);
    case SpectrumType::LSD:
      return specBin * TMath::Sqrt( S2 * getSamplingFreq() / 2.);
    default:
      return 0;
  }
}

