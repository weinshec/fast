#ifndef __FAST_VERSION__
#define __FAST_VERSION__


namespace fast {

extern const char GIT_VERSION[];
extern const char GIT_HASH[];

} /* namespace fast */


#endif /* __FAST_VERSION__ */
