#!/usr/bin/env python
# -*- coding: utf-8 -*-


import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
ROOT.gSystem.Load("libfast.so")


if __name__ == "__main__":

    # Get the full timeseries from heinzel.root
    tfile         = ROOT.TFile("heinzel.root")
    heinzelSample = tfile.Get("hHeinzel")

    # Create a DFT object, set the window function and calculate the LSD
    dft = ROOT.fast.DFT(heinzelSample)
    dft.setWindowFctType(ROOT.fast.WindowFct.HFT116D)

    # Target frequency resolution
    fres_target = 3.0
    dft.setFreqResolution(fres_target)
    print("Target freq res. = %.4f" % fres_target)
    print("Actual freq res. = %.4f" % dft.getFreqResolution())

    # Calculate spectrum
    spectrum = dft.spectrum(ROOT.fast.LSD)

    # Draw the result and save it as .pdf
    c1 = ROOT.TCanvas("c1", "c1", 800, 800)
    c1.SetLogy()
    spectrum.Draw()
    c1.SaveAs("heinzel.pdf")

    del dft

    # Cleanup
    tfile.Close()
