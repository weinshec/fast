#ifndef __TEST_COMMON_H__
#define __TEST_COMMON_H__

#include "gmock/gmock.h"
#include "gmock/gmock-actions.h"
#include "gmock/internal/gmock-port.h"
#include <vector>
#include "TH1.h"


using ::std::tr1::get;


namespace testing {

MATCHER(ArrDoubleEq, "") {
  Matcher<double> m1 = DoubleEq(get<0>(arg));
  return m1.Matches(get<1>(arg));
}

MATCHER_P(ArrDoubleNear, epsilon, "") {
  Matcher<double> m1 = DoubleNear(get<0>(arg), epsilon);
  return m1.Matches(get<1>(arg));
}

std::vector<double> getHistContent(const TH1* hist);
std::vector<double> getHistContent(const TH1 &hist);
std::vector<double> getHistBinning(const TH1* hist, bool upperBound=false);
std::vector<double> getHistBinning(const TH1 &hist, bool upperBound=false);

} /* namespace testing */


#endif /* ifndef __TEST_COMMON_H__ */

