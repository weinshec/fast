#include "gmock/gmock.h"
#include "transformations.h"
#include "generatorrecord.h"
#include "utils.h"


using namespace testing;
using namespace fast;


class AParameter : public Test {
 public:
  void SetUp() {
   fast::utils::Logger::instance()->setLevel(fast::utils::LogLevel::QUIET);
  }
};

TEST_F(AParameter, FixedParameterAlwaysYieldsSameValue) {
  auto p = Parameter::type<FixedValue>("answer", 42);
  ASSERT_THAT( p.value(), Eq(p.value()) );
}

TEST_F(AParameter, UncertainParameterScatters) {
  auto p = Parameter::type<GausValue>("answer", 42, 1.0);
  ASSERT_THAT( p.value(), Ne(p.value()));
}

TEST_F(AParameter, SavesParameterValueToRecordTruthMap) {
  GeneratorRecord flatSample(0, 0.0);

  auto p = Parameter::type<FixedValue>("answer", 42);
  p.value(flatSample);
  ASSERT_THAT(flatSample.getTruthMap()["answer"], Eq(42));
}

TEST_F(AParameter, SamplingFromHistogramWorksAsExpected) {
  TH1D hist("dist", "dist", 5, 0, 1);
  for (int i = 0; i < hist.GetNbinsX(); i++)
    hist.SetBinContent(i+1, 1);

  auto p = Parameter::type<HistValue>("histParameter", hist);
  EXPECT_THAT( p.value(), Ge(0));
  EXPECT_THAT( p.value(), Le(1));
}

TEST_F(AParameter, AllowsResettingOfType) {
  auto p = Parameter::type<FixedValue>("answer", 42);
  p.resetType<FixedValue>(21);
  ASSERT_THAT( p.value(), Eq(21) );
}

TEST_F(AParameter, CopyCTorWorksAsExpected) {
  auto p = Parameter::type<FixedValue>("answer", 42);
  Parameter duplicate(p);
  ASSERT_THAT( p.value(), Eq(duplicate.value()) );
}

TEST_F(AParameter, ParsingFromJSONworksForFixedType) {
  utils::ConfigParser::JsonConfig json = {
    {"type", "fixed"},
    {"value", 3.14}
  };
  auto p = Parameter::fromJSON("someName", json);
  ASSERT_THAT( p.value(), Eq(3.14) );
}

TEST_F(AParameter, ParsingFromJSONThrowsExceptionIfCannotParse) {
  utils::ConfigParser::JsonConfig json = { {"type", "foo"} };
  ASSERT_THROW( Parameter::fromJSON("someName", json), std::runtime_error);
}


class Transformations: public Test {
 public:
  std::size_t size = 5;
  double fs        = 1.0;
  GeneratorRecord flatSample{size,fs};
};

TEST_F(Transformations, AddScalarActsCorrectly) {
  AddScalar trans(Parameter::type<FixedValue>("addend", 42));

  trans(flatSample);

  ASSERT_THAT(flatSample.getData(), Each(42));
}

TEST_F(Transformations, MultiplyActsCorrectly) {
  flatSample += 1;
  auto factor = Parameter::type<FixedValue>("factor", 42);
  Multiply trans(factor);

  trans(flatSample);

  ASSERT_THAT(flatSample.getData(), Each(factor.value()));
}

TEST_F(Transformations, TimeShiftCreatesDelayedSample) {
  flatSample += std::vector<double>{0, 1, 2, 3, 4};

  TimeShift trans(Parameter::type<FixedValue>("shift", 2.0));

  trans(flatSample);

  ASSERT_THAT(flatSample.getData(), ElementsAre(0, 0, 0, 1, 2));
}

TEST_F(Transformations, TimeShiftCreatesEarlySample) {
  flatSample += std::vector<double>{0, 1, 2, 3, 4};

  TimeShift trans(Parameter::type<FixedValue>("shift", -2.0));

  trans(flatSample);

  ASSERT_THAT(flatSample.getData(), ElementsAre(2, 3, 4, 0, 0));
}

TEST_F(Transformations, AddWhiteNoiseActsCorrectly) {
  AddWhiteNoise trans(Parameter::type<FixedValue>("stddev", 1.0));

  trans(flatSample);

  ASSERT_THAT(flatSample.getData(), Each(Ne(0.0)));
}

TEST_F(Transformations, AddPhotonPulseCalculatesPeakPositionCorrect) {
  auto ampl  = Parameter::type<FixedValue>("ampl",    -0.07);
  auto tau_p = Parameter::type<FixedValue>("tau_p",   70e-9);
  auto tau_m = Parameter::type<FixedValue>("tau_m",   1.53e-6);
  auto dt    = Parameter::type<FixedValue>("tOffset", 0.1);
  AddPhotonPulse trans(ampl, tau_p, tau_m, dt);

  auto t_min = 2.26269e-07 + dt.value(); // Analytically calculated

  ASSERT_THAT( trans.getPeakPosition(), DoubleNear(t_min, 1e-12) );
}

TEST_F(Transformations, AddPhotonPulseFunctionScalesAsExpected) {
  auto ampl  = Parameter::type<FixedValue>("ampl",  -0.07);
  auto tau_p = Parameter::type<FixedValue>("tau_p", 70e-9);
  auto tau_m = Parameter::type<FixedValue>("tau_m", 1.53e-6);
  auto dt    = Parameter::type<FixedValue>("tOffset", 0);
  AddPhotonPulse trans(ampl, tau_p, tau_m, dt);

  ASSERT_THAT( trans.getFunction()->Eval( trans.getPeakPosition() ),
      DoubleNear(ampl.value(), 0.000007) ); // 0.01 % accuracy
}

