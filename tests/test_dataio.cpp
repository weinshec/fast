#include "gmock/gmock.h"
#include "test_common.h"
#include "dataio.h"

#include <algorithm>
#include <numeric>


using namespace testing;
using namespace fast;


class AUnifiedVector: public Test {
 public:
   std::shared_ptr<TTree> tree = nullptr;

  void SetUp() {
    std::vector<double> v;
    tree = std::make_shared<TTree>("tree", "tree");
    tree->SetDirectory(0);
    tree->Branch("data", &v);

    v.push_back(1);
    tree->Fill();
    v.push_back(2);
    tree->Fill();
    v.push_back(3);
    tree->Fill();

    // RESULT: {1, 1, 2, 1, 2, 3};
  }
};

TEST_F(AUnifiedVector, YieldsCorrectSize) {
  UnifiedVector<double> uv(tree, "data");
  ASSERT_THAT(uv.size(), Eq(6));
}

TEST_F(AUnifiedVector, AtMethodReturnsCorrectValue) {
  UnifiedVector<double> uv(tree, "data");
  EXPECT_THAT( uv.at(0), DoubleEq(1) );
  EXPECT_THAT( uv.at(1), DoubleEq(1) );
  EXPECT_THAT( uv.at(2), DoubleEq(2) );
  EXPECT_THAT( uv.at(3), DoubleEq(1) );
  EXPECT_THAT( uv.at(4), DoubleEq(2) );
  EXPECT_THAT( uv.at(5), DoubleEq(3) );
}

TEST_F(AUnifiedVector, AccessOperatorReturnsCorrectValue) {
  UnifiedVector<double> uv(tree, "data");
  EXPECT_THAT( uv[0], DoubleEq(1) );
  EXPECT_THAT( uv[1], DoubleEq(1) );
  EXPECT_THAT( uv[2], DoubleEq(2) );
  EXPECT_THAT( uv[3], DoubleEq(1) );
  EXPECT_THAT( uv[4], DoubleEq(2) );
  EXPECT_THAT( uv[5], DoubleEq(3) );
}

TEST_F(AUnifiedVector, FrontAndBackMethodsReturnCorrectValue) {
  UnifiedVector<double> uv(tree, "data");
  EXPECT_THAT( uv.front(), DoubleEq(1) );
  EXPECT_THAT( uv.back(),  DoubleEq(3) );
}

TEST_F(AUnifiedVector, BeginIteratorWorksAsExpeced) {
  UnifiedVector<double> uv(tree, "data");
  auto it = uv.cbegin();
  ASSERT_THAT( *it, DoubleEq(1) );
}

TEST_F(AUnifiedVector, IncrementingIteratorWorks) {
  UnifiedVector<double> uv(tree, "data");
  auto it = uv.cbegin();

  it += 4;
  EXPECT_THAT( *it, DoubleEq(2) );

  it++;
  EXPECT_THAT( *it, DoubleEq(3) );
}

TEST_F(AUnifiedVector, DecrementingIteratorWorks) {
  UnifiedVector<double> uv(tree, "data");
  auto it = uv.cend();

  it--;
  EXPECT_THAT( *it, DoubleEq(3) );

  it -= 1;
  EXPECT_THAT( *it, DoubleEq(2) );
}

TEST_F(AUnifiedVector, IteratorComparisonWorksAsExpected) {
  UnifiedVector<double> uv1(tree, "data");
  UnifiedVector<double> uv2(uv1);

  EXPECT_TRUE( uv1.cbegin() == uv1.cbegin() );
  EXPECT_TRUE( uv1.cbegin() != uv1.cend() );
  EXPECT_FALSE( uv1.cbegin() == uv2.cbegin() );
  EXPECT_TRUE( uv1.cbegin() != uv2.cbegin() );
  EXPECT_TRUE( ++uv1.cbegin() != uv1.cbegin() );
}

TEST_F(AUnifiedVector, IteratorIsCompatibleWithSTL) {
  UnifiedVector<double> uv(tree, "data");
  auto sum = std::accumulate(uv.cbegin(), uv.cend(), 0.);
  ASSERT_THAT(sum, DoubleEq(10));
}

TEST_F(AUnifiedVector, DataMethodReturnsUnifiedVector) {
  UnifiedVector<double> uv(tree, "data");
  ASSERT_THAT( uv.data(), ElementsAre(1, 1, 2, 1, 2, 3) );
}

