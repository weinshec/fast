#!/usr/bin/env python
# -*- coding: utf-8 -*-


import argparse
import array
import logging
import sys
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
ROOT.gSystem.Load("libfast.so")


def fromTree(infile, treeName, branchName):
    """Create a DFT object from standard tree as defined in the TES Record
    struct. CAVE: Only the first tree entry is considered, any further entries
    are discarded.

    :infile:     path to root file containing the tree
    :treeName:   Name of the TTree object containing the Record information
    :branchName: Name of the data branch
    :returns:    DFT object of the data
    """

    tfile  = ROOT.TFile(infile, 'READ')
    logging.info("Processing %s" % args.inputFile)

    if not tfile.GetListOfKeys().Contains(treeName):
        logging.error('Tree %s not found in %s' % (treeName, tfile.GetName()))
        sys.exit(1)
    tree = tfile.Get(treeName)
    logging.info('Opened TTree %s' % treeName)

    if not tree.GetListOfBranches().Contains(branchName):
        logging.error('Branch %s not found in %s' % (branchName, treeName))
        sys.exit(1)
    logging.info('Reading Branch %s' % branchName)

    fs = array.array('d', [0])
    tree.SetBranchAddress('samplingFrequency', fs)
    tree.GetEntry(0)

    uv_data = ROOT.fast.UnifiedVector('double')(tree, branchName)
    data = uv_data.data()
    del uv_data

    logging.debug('Read %d values' % data.size())
    logging.debug('SamplingFreq = %s' % str(fs[0]))

    return ROOT.fast.DFT(data, fs[0])


def fromHist(infile, histName):
    """Create a DFT object from a histogram

    :infile:     path to root file containing the histogram
    :histName:   Name of the histogram containing the data
    :returns:    DFT object of the data
    """

    tfile  = ROOT.TFile(infile, 'READ')
    logging.info("Processing %s" % args.inputFile)

    if not tfile.GetListOfKeys().Contains(histName):
        logging.error('Histogram %s not found in %s'
            % (histName, tfile.GetName()))
        sys.exit(1)
    hist = tfile.Get(histName)
    logging.debug('Got histogram with %d bins' % hist.GetNbinsX())

    return ROOT.fast.DFT(hist)


def fromCSV(infile, fs):
    """Create a DFT object from a csv file with given sampling frequency

    :infile:  path to the input file
    :fs:      sampling frequency given in Hz
    :returns: DFT object of the data
    """

    data = ROOT.vector("double")()

    with open(infile, 'r') as csvFile:
        for i, line in enumerate(csvFile):
            data.push_back( float(line) )

    logging.debug('Found %d datapoints' % data.size())

    return ROOT.fast.DFT(data, fs)





if __name__ == "__main__":

    # Create command line argument parser
    parser = argparse.ArgumentParser(description='Perform a spectral analysis\
        using a dirscrete fourier transformation.')
    parser.add_argument('inputFile', metavar='<input>', type=str,
                        help='input root or csv file')
    parser.add_argument('outputFile', metavar='<output>', type=str,
                        help='output root file')
    specType = parser.add_mutually_exclusive_group(required=True)
    specType.add_argument('--PS', action='store_true',
                       help='create PowerSpectrum')
    specType.add_argument('--PSD', action='store_true',
                       help='create PowerSpectralDensity')
    specType.add_argument('--LS', action='store_true',
                       help='create LinearSpectrum')
    specType.add_argument('--LSD', action='store_true',
                       help='create LinearSpectralDensity')
    itype = parser.add_mutually_exclusive_group(required=True)
    itype.add_argument('--branch', metavar='TREE:BRANCH', type=str,
                       help='read data from BRANCH in TREE (root file)')
    itype.add_argument('--hist', metavar='NAME', type=str,
                       help='histogram name of data to read (root file)')
    itype.add_argument('--csv', metavar='FLOAT', type=float,
                       help='csv sampling frequency')
    parser.add_argument('--fres', metavar='FLOAT', type=float,
                        help='set frequency resolution')
    parser.add_argument('--debug', action='store_true',
                        help='enable debug output')
    args = parser.parse_args()

    # Set logging options
    logLevel = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(format='[%(levelname)s]: %(message)s', level=logLevel)

    # Open the input file and output file
    outFile = ROOT.TFile(args.outputFile, 'RECREATE')
    logging.info("Created %s" % args.outputFile)

    # Create DFT object
    if args.branch:
        tokens = args.branch.split(':')
        if len(tokens) != 2:
            logging.error('Invalid argument %s' % args.branch)
            logging.error('Use syntax    --branch=treename:branchname')
            sys.exit(1)
        dft = fromTree(args.inputFile, *tokens)
    elif args.hist:
        dft = fromHist(args.inputFile, args.hist)
    elif args.csv:
        dft = fromCSV(args.inputFile, args.csv)

    # Set default Window Function
    dft.setWindowFctType(ROOT.fast.WindowFct.HANNING)

    # Set the frequency resolution
    if args.fres:
        dft.setFreqResolution(args.fres)
    logging.info("Effective freq res. = %s Hz" % str(dft.getFreqResolution()))

    # Calculate spectrum
    spectrum = None
    if args.PS:
        spectrum = dft.spectrum(ROOT.fast.PS)
        logging.info("Created PowerSpectrum")
    elif args.PSD:
        spectrum = dft.spectrum(ROOT.fast.PSD)
        logging.info("Created PowerSpectralDensity")
    elif args.LS:
        spectrum = dft.spectrum(ROOT.fast.LS)
        logging.info("Created LinearSpectrum")
    elif args.LSD:
        spectrum = dft.spectrum(ROOT.fast.LSD)
        logging.info("Created LinearSpectralDensity")

    # Write output
    outFile.cd()
    spectrum.Write()
    logging.info('Written to %s' % args.outputFile)

    # Cleanup
    del dft
    outFile.Close()
