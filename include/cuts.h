#ifndef __CUTS_H__
#define __CUTS_H__

#include <functional>
#include <iomanip>
#include <iostream>


namespace fast {


/** Convenience class performing flexible event based cuts.
 *  Evaluates a boolean expression represented by a general function pointer or
 *  lambda expression for a given event. Further provides an internal counter
 *  on how many events have been rejected (i.e. the expression evaluted to
 *  true) or have been passed.
 */
template <typename... Args>
class EventCut {
 public:
  /** Constructor.
   *
   *  \param name representive name of this event cut
   *  \param func function pointer to the boolean expression to evaluate
   *
   *  Logic has to implemented such that the boolean expression evaluates to
   *  true for events to reject.
   *
   * Example usage: 
   * ~~~ {.cpp}
   *  EventCut<PHPIProcessor::Record> cutPHSaturation("saturation cut",
   *      [](const PHPIProcessor::Record& r){ return r.ph < -0.08; });
   * ~~~
   */
  EventCut(const std::string& name, const std::function<bool(Args...)>& func)
      : name_(name), func_(func) {}

  /** Boolean expression evaluator.
   *
   *  \param args Parameters passed to the function pointer of the boolean
   *  expression.
   *  \return true in case of event rejection
   */
  bool operator() (Args... args) {
    bool reject = func_(args...);
    if (reject) nRejected_++; else nPassed_++;
    return reject;
  }

  /** Get the number of rejected events.
   */
  unsigned int getRejected() const {
    return nRejected_;
  };

  /** Get the number of passed events.
   */
  unsigned int getPassed() const {
    return nPassed_;
  };

  /** Get the total number of events.
   */
  unsigned int getTotal() const {
    return getRejected() + getPassed();
  }

  /** Pretty print the number of rejected/passed events.
   *
   *  \param printHead prints the column description, useful on first call to
   *  `pprint()`
   */
  void pprint(bool printHead=false) const {
    if (printHead) {
      std::cout << std::right
                << std::setw(21) << "cut name "
                << std::setw(11) << "rejected "
                << std::setw(11) << "passed "
                << std::setw(11) << "total "
                << std::setw(0) << std::endl
                << std::string(20, '-') << " "
                << std::string(10, '-') << " "
                << std::string(10, '-') << " "
                << std::string(10, '-')
                << std::endl;
    }
    std::cout << std::right
              << std::setw(20) << name_
              << std::setw(11) << std::to_string(getRejected())
              << std::setw(11) << std::to_string(getPassed())
              << std::setw(11) << std::to_string(getTotal())
              << std::endl << std::left << std::setw(0);
  };

 private:
  const std::string name_;                  ///< name of the event cut
  const std::function<bool(Args...)> func_; ///< function ptr to bool expr.

  unsigned int nRejected_ = 0;  ///< counter for rejected events
  unsigned int nPassed_   = 0;  ///< counter for passed events
};


} /* namespace fast */


#endif /* __CUTS_H__ */
