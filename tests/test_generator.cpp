#include "gmock/gmock.h"
#include "test_common.h"
#include "generator.h"


using namespace testing;
using namespace fast;


class AGenerator: public Test {
 public:
   Generator gen{10, 2.0};
};

TEST_F(AGenerator, GeneratedRecordHasCorrectSize) {
  gen.addTransformation<AddScalar>(
      Channel::ChA, Parameter::type<FixedValue>("addend", 42));
  gen.setSize(10);
  gen.generate();
  ASSERT_THAT( gen.record().dataChA->size(), Eq(10) );
}

TEST_F(AGenerator, ChangingTheRecordSizeIntermediately) {
  gen.addTransformation<AddScalar>(
      Channel::ChA, Parameter::type<FixedValue>("addend", 42));
  gen.setSize(10); gen.generate();
  gen.setSize(2);  gen.generate();
  ASSERT_THAT( gen.record().dataChA->size(), Eq(2) );
}

TEST_F(AGenerator, GeneratedRecordHasCorrectSamplingRate) {
  gen.addTransformation<AddScalar>(
      Channel::ChA, Parameter::type<FixedValue>("addend", 42));
  gen.setSamplingFreq(42.0);
  gen.generate();
  ASSERT_THAT( gen.record().samplingFrequency, DoubleEq(42.0) );
}

TEST_F(AGenerator, ListOfTransformationsIsEmptyPerDefault) {
  auto transList = gen.getTransformations();
  ASSERT_THAT( transList, IsEmpty() );
}

TEST_F(AGenerator, AddingATransformationIncreasesListLength) {
  gen.addTransformation<AddScalar>(
      Channel::ChA, Parameter::type<FixedValue>("addend", 42));
  ASSERT_THAT( gen.getTransformations(), SizeIs(1) );
}

TEST_F(AGenerator, TransformationsGetAppliedToGeneratorRecord) {
  gen.addTransformation<AddScalar>(
      Channel::ChA, Parameter::type<FixedValue>("addend", 42));
  gen.generate();
  ASSERT_THAT( *gen.record().dataChA, Each(Eq(42)) );
}

TEST_F(AGenerator, TransformationsMayBeChannelSpecific) {
  gen.addTransformation<AddScalar>(
      Channel::ChB, Parameter::type<FixedValue>("addend", 42));
  gen.generate();
  ASSERT_THAT( *gen.record().dataChB, Each(Eq(42)) );
}

TEST_F(AGenerator, ChannelHavingNoTransformationIsEmpty) {
  gen.addTransformation<AddScalar>(
      Channel::ChB, Parameter::type<FixedValue>("addend", 42));
  gen.generate();
  ASSERT_THAT( gen.record().dataChA, Eq(nullptr) );
}
