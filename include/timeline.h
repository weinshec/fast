#ifndef __TIMELINE_H__
#define __TIMELINE_H__

#include <memory>
#include "TTree.h"

#include "defs.h"
#include "dataio.h"


namespace fast {

/** Offline trigger class of timeline data.
 *  Extracts records from timelines fulfilling offline trigger criteria
 *  on-the-fly e.g. without need to store intermediate files.
 */
class TimelineTrigger {
 public:
  /** Definition of the trigger slope.
   */
  enum Slope {
    POS,  ///< positive trigger slope.
    NEG,  ///< negative trigger slope.
  };

  /** Constructor.
   *
   *  \param tree TTree to read the timeline from
   *  \param branchname name of the std::vector branch containting the timeline
   */
  TimelineTrigger(const std::shared_ptr<TTree>& tree,
                  const std::string& branchname);

  /** Scan the full timeline for trigger events.
   *
   *  \param trigLvl threshold in Volts
   *  \param slope trigger slope
   *  \param deadtime number of data points considered as deadtime after a
   *         previously triggered events
   *  \return number of found trigger events
   */
  size_t scan(const double trigLvl, const Slope slope=NEG,
              const size_t deadtime=0);

  /** Get the found peak positions.
   *
   *  \return vector of peak positions
   *
   *  Use this method after `scan()` to get the global peak positions.
   */
  std::vector<size_t> getPeakPositions() const;

  /** Copy a sample around a trigger event position.
   *
   *  \param pos position of the trigger event
   *  \param preSamples number of pre-samples to take
   *  \param postSamples number of post-samples to take
   *  \return samples in the interval (pos-preSamples, pos+preSamples)
   *
   *  \note if `pos` is too close to the timelines begin or end the resulting
   *  subsample will be truncated.
   */
  std::vector<double> extract(const size_t pos, const size_t preSamples,
                              const size_t postSamples) const;

  /** Create an AlazarRecord from a trigger event position.
   *
   *  \param pos position of the trigger event
   *  \param preSamples number of pre-samples to take
   *  \param postSamples number of post-samples to take
   *  \return AlazarRecord of the trigger event around `pos`
   *
   *  You may call `setRecordChannel()` and `setRecordSamplingFreq()`
   *  beforehand in order to get the timeStamp, samplingFrequency and channel
   *  mapping right.
   *
   *  \note if `pos` is too close to the timelines begin or end the resulting
   *  record will be truncated.
   */
  AlazarRecord extractRecord(const size_t pos, const size_t preSamples,
                             const size_t postSamples) const;

  /** Set the channel mapping for new records.
   *
   *  \param ch channel to set when calling `extractRecord()`
   */
  void setRecordChannel(const Channel ch);

  /** Set the sampling frequency for new records.
   *
   *  \param fs sampling frequency in Hz to set when calling `extractRecord()`
   */
  void setRecordSamplingFreq(const double fs);

 private:
  UnifiedVector<double> uv_;    ///< UnifiedVector of the timeline
  std::vector<size_t> peakPos_; ///< peak positions found in `scan()`

  Channel ch_ = Channel::ChA;   ///< channel setting for records
  double fs_  = 0.0;            ///< sampling freq. settings for records

  /** Calculate the distance to the last found peak position.
   *
   *  \param it iterator to the current found peak
   *  \return distance to the last found peak position or 0 in case its the
   *          first peak, i.e. peakPos_ is empty
   */
  size_t distanceToLast(const UnifiedVector<double>::ConstIterator& it) const;
};

} /* namespace fast */


#endif /* __TIMELINE_H__ */
