#include "gmock/gmock.h"
#include "test_common.h"
#include "timeline.h"


using namespace testing;
using namespace fast;


class ATimelineTrigger: public Test {
 public:
   std::shared_ptr<TTree> tree = nullptr;

  void SetUp() {
    std::vector<double> v;
    tree = std::make_shared<TTree>("tree", "tree");
    tree->SetDirectory(0);
    tree->Branch("data", &v);

    v = {0, 1, -1, 0, -4, -5, -3, -2, -1, 0};
    tree->Fill();
    v = {0, -1, 3, 2, 0, -5, 0, -5, -3, 1};
    tree->Fill();
  }
};

TEST_F(ATimelineTrigger, FindsTheCorrectNumberOfPeaks) {
  TimelineTrigger tlt(tree, "data");
  ASSERT_THAT( tlt.scan(-2.5), Eq(3) );
}

TEST_F(ATimelineTrigger, FindsCorrectPositionOfPeaks) {
  TimelineTrigger tlt(tree, "data");
  tlt.scan(-2.5);
  ASSERT_THAT( tlt.getPeakPositions(), ElementsAre(4, 15, 17) );
}

TEST_F(ATimelineTrigger, AlsoFindsPositiveSlopedPeaks) {
  TimelineTrigger tlt(tree, "data");
  tlt.scan(2.5, TimelineTrigger::Slope::POS);
  ASSERT_THAT( tlt.getPeakPositions(), ElementsAre(12) );
}

TEST_F(ATimelineTrigger, ExtractYieldsCorrectRecordSize) {
  TimelineTrigger tlt(tree, "data");
  auto record = tlt.extractRecord(5, 3, 3);
  ASSERT_THAT( record.dataChA->size(), Eq(3+3) );
}

TEST_F(ATimelineTrigger, ExtractYieldsCorrectDataContent) {
  TimelineTrigger tlt(tree, "data");
  auto record = tlt.extractRecord(5, 3, 3);
  ASSERT_THAT( *record.dataChA, ElementsAre(-1, 0, -4, -5, -3, -2) );
}

TEST_F(ATimelineTrigger, ExtractLetsYouChooseWhichChannelToSaveIn) {
  TimelineTrigger tlt(tree, "data");
  tlt.setRecordChannel(Channel::ChB);
  auto record = tlt.extractRecord(5, 3, 3);
  ASSERT_THAT( *record.dataChB, ElementsAre(-1, 0, -4, -5, -3, -2) );
}

TEST_F(ATimelineTrigger, ExtractSetsTriggerFlagCorrectly) {
  TimelineTrigger tlt(tree, "data");
  tlt.setRecordChannel(Channel::ChB);
  auto record = tlt.extractRecord(5, 3, 3);
  EXPECT_THAT( record.triggerChA , Eq(false) );
  EXPECT_THAT( record.triggerChB , Eq(true)  );
}

TEST_F(ATimelineTrigger, ExtractSetsTimeStampCorrectly) {
  TimelineTrigger tlt(tree, "data");
  tlt.setRecordSamplingFreq(2.0);
  auto record = tlt.extractRecord(5, 3, 3);
  ASSERT_THAT( record.timeStamp, DoubleEq(2.5) );
}

TEST_F(ATimelineTrigger, ExtractSetsSamplingFrequencyRight) {
  TimelineTrigger tlt(tree, "data");
  tlt.setRecordSamplingFreq(2.0);
  auto record = tlt.extractRecord(5, 3, 3);
  ASSERT_THAT( record.samplingFrequency, DoubleEq(2.0) );
}

TEST_F(ATimelineTrigger, TooEarlySamplesAreShorter) {
  TimelineTrigger tlt(tree, "data");
  ASSERT_THAT( tlt.extract(1, 3, 3), ElementsAre(0, 1, -1, 0) );
}

TEST_F(ATimelineTrigger, TooLateSamplesAreShorter) {
  TimelineTrigger tlt(tree, "data");
  ASSERT_THAT( tlt.extract(18, 3, 3), ElementsAre(-5, 0, -5, -3, 1) );
}

TEST_F(ATimelineTrigger, ScanningAllowsDeadtimeSpecification) {
  TimelineTrigger tlt(tree, "data");
  tlt.scan(-2.5, TimelineTrigger::Slope::NEG, 3);
  ASSERT_THAT( tlt.getPeakPositions(), ElementsAre(4, 15) );
}
