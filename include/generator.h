#ifndef __GENERATOR_H__
#define __GENERATOR_H__

#include <ctime>
#include <memory>

#include "TDirectory.h"
#include "TTree.h"

#include "defs.h"
#include "dataio.h"
#include "generatorrecord.h"
#include "transformations.h"


namespace fast {

/** Channel specific transformation.
 *  Assigns a Channel specifier to a Transformation.
 */
using ChannelTransformation =
    std::pair<Channel,std::shared_ptr<Transformation>>;

/** Monte-Carlo Generator class producing simulated AlazarRecords.
 *  The general idea is for each to event to start off with a plain (flat)
 *  vector of zeros for both channels and sequentially apply a list of
 *  transformations on them, e.g. AddPhotonPulse, AddWhiteNoise,...
 */
class Generator : public TreeLoggable {
 public:
  /** Constructor.
   *  \param size size of each individual AlazarRecord.
   *  \param samplingFreq sampling frequency in Hz.
   */
  Generator(const size_t size, const double samplingFreq);

  /** Destructor.
   */
  virtual ~Generator();

  /** Change the sample size of AlazarRecords.
   */
  void setSize(const std::size_t size);

  /** Change the sampling frequency.
   */
  void setSamplingFreq(const double freq);

  /** Get the list of registered transformations.
   *  \return vector of channel-transformation pairs
   *
   *  Returns a list of registered transformations and their according channel
   *  to act on.
   */
  std::vector<ChannelTransformation> getTransformations() const;

  /** Register a new Transformation.
   *
   *  \param ch Channel the new Transformation has to be applied to
   *  \param args argument pack to be passed to the Transformations
   *    constructor. Usually instances of type Parameter.
   *
   *  The type `<T>` refers to type of the Transformation.
   *
   *  Example usage:
   *  ~~~{.cpp}
   *    generator.addTransformation<AddScalar>(
   *        Channel::ChA, Parameter::type<FixedValue>("addend", 42));
   *  ~~~
   */
  template <typename T, typename... Args>
  void addTransformation(const Channel ch, Args... args) {
    transformations_.push_back(
        std::make_pair( ch, std::make_shared<T>(args...) ));
    utils::LogInfo("Generator")
        << "Added " + transformations_.back().second->to_string();
    if (tree_) transformations_.back().second->setTree(tree_);
  };

  /** Generate a new event.
   *
   *  Simulates a new event and saves it in the output TTree if set by
   *  `setTree`. The filled AlazarRecord is also accessible via `record()`.
   */
  void generate();

  /** Access the recently filled AlazarRecord.
   *
   *  \return reference to the filled AlazarRecord.
   */
  const AlazarRecord& record() const;

  virtual void setTree(const std::shared_ptr<TTree>& tree) override;

 private:
  std::size_t size_;                ///< event size in number of samples
  double samplingFreq_;             ///< sampling frequency in Hz
  GeneratorRecord genRecord_ChA_;   ///< GeneratorRecord for Channel A
  GeneratorRecord genRecord_ChB_;   ///< GeneratorRecord for Channel B
  AlazarRecord record_;             ///< Internal instance of AlazarRecord
  std::vector<ChannelTransformation> transformations_; ///< transformations

  std::shared_ptr<TTree> tree_ = nullptr;   ///< output TTree pointer

  /** Check if a channel transformation for a specific channel has been added.
   *
   *  \param ch Channel to check transformation list for
   *  \return `true` if there is at least one transformation according to the
   *    given channel
   */
  bool hasTransForChannel(const Channel ch) const;
};

} /* namespace fast */

#endif /* __GENERATOR_H__ */
