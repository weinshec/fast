#ifndef __PULSEFITTER_H__
#define __PULSEFITTER_H__

#include <memory>

#include "TF1.h"
#include "TFitResult.h"
#include "TFitResultPtr.h"
#include "TGraphErrors.h"

#include "defs.h"
#include "pulsefct.h"


namespace fast {

/** Elaborate fitting methods for photon pulses.
 *  Uses the analytic pulse description defined in the `photon` pulsefunction
 *  to estimate the parameters of the pulse. This this funtion is
 *  non-differentiable at \f$ t = t_0 \f$ the fit relies on a good initial
 *  value of the `tOffset` parameter. Hence to make fitting more robust, an
 *  iterative approach is used shifting the initial value for `tOffset` further
 *  to the left (earlier in time) with each iteration.
 */
class PulseFitter {
 public:
  /** Constructor
   *
   *  \param initShape PulseShape definition for initial fit values
   */
  PulseFitter(const PulseShape& initShape=PulseShape::PHOTON_55mV());

  /** Perform a pulse fit.
   *
   *  \param fs sampling frequency in Hz
   *  \param data vector of data points (y-values)
   *  \param uncert fixed uncertainty for each data point
   *  \param peaks approximate position of peaks in case of multiple
   *  \return pointer to the fit results
   *
   *  If `peaks` is not given or empty only a single peak is assumed and its
   *  position is determined automatically.
   */
  TFitResultPtr fit(const double fs, const std::vector<double> &data,
                    const double uncert,
                    const std::vector<double>& peaks=std::vector<double>());

  /** Perform a pulse fit.
   *
   *  \param fs sampling frequency in Hz
   *  \param data vector of data points (y-values)
   *  \param uncert vector of uncertainties (same size as data)
   *  \param peaks approximate position of peaks in case of multiple
   *  \return pointer to the fit results
   *
   *  If `peaks` is not given or empty only a single peak is assumed and its
   *  position is determined automatically.
   */
  TFitResultPtr fit(const double fs, const std::vector<double> &data,
                    const std::vector<double> &uncert,
                    const std::vector<double>& peaks=std::vector<double>());

  /** Perform a pulse fit.
   *
   *  \param graph data graph to fit
   *  \param peaks approximate position of peaks in case of multiple
   *  \return pointer to the fit results
   *
   *  If `peaks` is not given or empty only a single peak is assumed and its
   *  position is determined automatically.
   */
  TFitResultPtr fit(const TGraphErrors &graph,
                    const std::vector<double>& peaks=std::vector<double>());

  /** Get a plot of the data overlayed with the fitted function
   *
   *  \return data plot with fit-overlay
   */
  std::shared_ptr<TGraphErrors> getGraph() const;

  /** Set a particular parameter to a fixed value.
   *
   *  \param idx index of the parameter (see `pulsefct::photon()`)
   *  \param value fix parameter to this value
   */
  void fixParameter(size_t idx, const double value);

  /** Release a formerly fixed parameter again.
   *
   *  \param idx index of the parameter (see `pulsefct::photon()`)
   */
  void resetParameter(size_t idx);

 private:
  const std::string kMinimizer = "Minuit";  ///< default minimizer
  const unsigned int kMaxIterations = 50;   ///< max allowed iterations
  const PulseShape kShape;                  ///< initial pulse shape values
  const double kInitDCoffset = 0.0;         ///< initial value for DC offset

  std::vector<bool> fixedParameter_;        ///< flags for fixed parameters

  std::unique_ptr<pulsefct::MultiPhoton> multiPhoton_; ///< analytic function
  std::shared_ptr<TGraphErrors> graph_;  ///< graph representation
  std::vector<double> peaks_;            ///< approx. peak positions

  /** Set values for `photonPulse_` according to best guess
   */
  void setInitFitValues();

  /** Shift `tOffset` earlier in time according to iteration
   *
   *  \param iteration number of next iteration
   */
  void lowerTOffsetForIteration(unsigned int iteration);

  /** Recursively calling fit function
   *
   *  \param iteration number of next iteration
   *  \return pointer to the fit results
   *
   *  Calls itselfs recursivley until either a good fit result has been reached
   *  (FitStatus = 0) or the `kMaxIterations` has been reached.
   */
  TFitResultPtr fitGraph(unsigned int iteration=0);

  /** Sample a vector of time points with given length and sampling frequency
   *
   *  \param N size of th vector
   *  \param fs sampling frequency in Hz
   *  \return vector time points
   */
  std::vector<double> sampleTime(const std::size_t N, const double fs) const;

  /** Check if a parameter has been fixed, otherwise set its value and bounds
   *
   *  \param idx index of the parameter (see `pulsefct::photon()`)
   *  \param value initial value for this parameter
   *  \param lowerBound lower bound used during fitting
   *  \param upperBound upper bound used during fitting
   */
  void setParameterIfNotFixed(size_t idx, const double value,
                              const double lowerBound, const double upperBound);

  /** Set the vector of approximate peak positions.
   *
   *  \param peaks approximate peak positions
   *
   *  Checks if the given vector of peak positions is empty. In that case
   *  assume only a single peak and estimate its position by the graphs minimum.
   *  Otherwise use the given positions.
   */
  void setApproxPeakPositions(const std::vector<double>& peaks);
};

} /* namespace fast */

#endif /* __PULSEFITTER_H__ */
