#ifndef __TRANSFORMATIONS_H__
#define __TRANSFORMATIONS_H__


#include "TF1.h"
#include "TH1D.h"
#include "TMath.h"
#include "TRandom3.h"

#include "dataio.h"
#include "generatorrecord.h"
#include "spectrum.h"


namespace fast {


/** Implement a particular type of Parameter behaviour.
 */
class ParameterType {
 public:
  /** Destructor.
   */
  virtual ~ParameterType() {};

  /** Get a value according to this type of parameter.
   *
   *  \return value
   */
  virtual double value() const = 0;

  /** Return a desciptive string about this ParameterType
   *
   *  \return description string
   */
  virtual std::string to_string() const = 0;

 protected:
  static const std::unique_ptr<TRandom3> rnd_;  ///< random number generator
};

/** Fixed immutable parameter.
 */
class FixedValue : public ParameterType {
 public:
  /** Constructor.
   *
   *  \param value fixed value
   */
  FixedValue(double value);
  double value() const override;
  std::string to_string() const override;

 private:
  double value_;  ///< fixed value
};

/** Value following a gaussian distribution.
 */
class GausValue : public ParameterType {
 public:
  /** Constructor.
   *
   *  \param mean mean of the gaussian distribution
   *  \param stddev standard deviation of the gaussian distribution
   */
  GausValue(double mean, double stddev);
  double value() const override;
  std::string to_string() const override;

 private:
  double mean_;   ///< mean of the gaussian distribution
  double stddev_; ///< stddev standard deviation of the gaussian distribution
};

/** Value following a flat distribution.
 */
class FlatValue : public ParameterType {
 public:
  /** Constructor.
   *
   *  \param low lower bound
   *  \param high upper bound
   */
  FlatValue(double low, double high);
  double value() const override;
  std::string to_string() const override;

 private:
  double low_;  ///< lower bound
  double high_; ///< upper bound
};

/** Value following a custom distribution.
 */
class HistValue : public ParameterType {
 public:
  /** Constructor.
   *
   *  \param hist distribution this value follows
   */
  HistValue(const TH1D& hist);
  double value() const override;
  std::string to_string() const override;

 private:
  std::unique_ptr<TH1> hist_ = nullptr;   ///< distribution of this value
};

/** General parameter of GeneratorRecord Transformations.
 *  Transformations in general rely on Parameters, which might be fixed, have
 *  an uncertainty or follow a specific distribution. The Parameter class
 *  realizes all types of Parameters given their particular ParameterType
 *  member.
 */
class Parameter : public TreeLoggable {
 public:
  /** Destructor.
   */
  virtual ~Parameter() = default;

  /** Factory method to create a Parameter with given type.
   *
   *  \param name unique name of the parameter
   *  \param args additional arguments passed to the ParameterType constructor
   *
   *  Example usage:
   *  ~~~ {.cpp}
   *    Parameter::type<FixedValue>("offset", 1.0);
   *  ~~~
   */
  template <typename T, typename... Args>
  static Parameter type(const std::string& name, Args... args) {
    Parameter p(name);
    p.strategy_.reset(new T(args...));
    return p;
  };

  /** Factory method to create a Parameter from JSON config
   *
   *  \param name unique name of the parameter
   *  \param json JsonConfig to parse the parameter information from
   *
   *  Expects a 'type' key and argument keys according to the ParameterType
   *
   *  Example:
   *  ~~~ {.json}
   *  {
   *    "type": "flat",
   *    "low:       -1,
   *    "high:      42
   *  }
   *  ~~~
   */
  static Parameter fromJSON(const std::string& name,
                            const utils::ConfigParser::JsonConfig& json);

  /** Change the type this Parameter.
   *
   *  \param args arguments passed to the ParameterType constructor
   */
  template <typename T, typename... Args>
  void resetType(Args... args) {
    strategy_.reset(new T(args...));
  };

  /** Get parameter value.
   *
   *  \return value of this parameter according to its ParameterType
   */
  double value();

  /** Get parameter value and store it in a GeneratorRecords truth map.
   *
   *  \param record to store the value in
   *  \return value of this parameter according to its ParameterType
   *
   *  Convenience method especially for non-fixed Parameters to store the drawn
   *  value in a record.
   */
  double value(GeneratorRecord &record);

  /** Get this parameters name.
   *
   *  \return name of this parameter
   */
  std::string getName() const;

  /** Return a desciptive string about this Parameter
   *
   *  \return description string
   */
  std::string to_string() const;

  virtual void setTree(const std::shared_ptr<TTree>& tree) override;

 protected:
  /** Constructor.
   *
   *  \param name unique name of this parameter
   *
   *  Do not use this ctor directle, rather invoke the Factory method
   *  `Parameter::type()`
   */
  Parameter(const std::string& name);

 private:
  double currentValue_ = 0;   ///< the current value
  std::string name_;          ///< name of this parameter
  std::shared_ptr<ParameterType> strategy_ = nullptr; ///< current ParameterType
};


/** Apply a transformation on a GeneratorRecord.
 *  Transformation are the building blocks of GeneratorRecord generation and
 *  represent a particular kind of modifiation, e.g. add-some-noise or
 *  shift-the-record-in-time.
 */
class Transformation : public TreeLoggable {
 public:
  /** Destructor.
   */
  virtual ~Transformation() = default;

  /** Apply this transformation on a GeneratorRecord
   */
  virtual Transformation& operator() (GeneratorRecord& record) = 0;

  /** Return a desciptive string about this transformation
   *
   *  \return description string
   */
  virtual std::string to_string() const = 0;

  virtual void setTree(const std::shared_ptr<TTree>& tree) override;

 protected:
  std::map<std::string,Parameter> pmap_;  ///< map of Parameters used
};

/** Shift the record vertically by a scalar value
 */
class AddScalar : public Transformation {
 public:
  /** Constructor.
   *
   *  \param addend scalar value to add
   */
  AddScalar(const Parameter& addend);

  AddScalar& operator() (GeneratorRecord& record) override;

  std::string to_string() const override;
};

/** Scale the record by a scalar value
 */
class Multiply : public Transformation {
 public:
  /** Constructor.
   *
   *  \param factor scaling factor
   */
  Multiply(const Parameter& factor);

  Multiply& operator() (GeneratorRecord& record) override;

  std::string to_string() const override;
};

/** Shift the record horizontally
 */
class TimeShift : public Transformation {
 public:
  /** Constructor.
   *
   *  \param offset time offset in seconds.
   *
   *  Positive offsets shift to later times while negative offsets to earlier
   *  times.
   */
  TimeShift(const Parameter &offset);

  TimeShift& operator() (GeneratorRecord& record) override;

  std::string to_string() const override;

 private:
  /** Convert a timespan to a number of datapoints
   *
   *  \param record GeneratorRecord to shift
   *  \return number of data points to shift
   */
  int time2Offset(GeneratorRecord& record);
};

/** Add gaussian distributed noise to a record
 */
class AddWhiteNoise : public Transformation {
 public:
  /** Constructor.
   *
   *  \param stddev standard deviation of the noise
   *  \param seed random seed for the internal random number generator
   */
  AddWhiteNoise(const Parameter &stddev, const unsigned int seed = 0);

  AddWhiteNoise& operator() (GeneratorRecord& record) override;

  std::string to_string() const override;

  /** Get the initial random seed.
   *
   *  \return inital seed
   */
  unsigned int getSeed() const;

  /** Set the initial random seed.
   *
   *  \param seed to set on the random generator
   */
  void setSeed(const unsigned int seed);

 private:
  std::unique_ptr<TRandom3> rnd_; ///< random number generator
};

/** Add noise following a given power spectrum
 */
class AddSpectralNoise : public Transformation {
 public:
  /** Constructor.
   *
   *  \param spectrum spectrum of the noise to add
   *  \param type specifies the type of the given spectrum
   */
  AddSpectralNoise(const TH1D& spectrum, const SpectrumType type);

  /** Constructor.
   *
   *  \param spectrum spectrum of the noise to add
   *  \param type specifies the type of the given spectrum
   */
  AddSpectralNoise(const TH1D* spectrum, const SpectrumType type);

  /** Constructor.
   *
   *  \param filename path to the root file containing the power spectrum
   *  \param histName of the power spectrum to load
   *  \param type specifies the type of the given spectrum
   */
  AddSpectralNoise(const std::string& filename, const std::string& histName,
                   const SpectrumType type);

  AddSpectralNoise& operator() (GeneratorRecord& record) override;

  std::string to_string() const override;

 private:
  std::unique_ptr<IDFT> idft_ = nullptr;  ///< Inverse DFT instance

  /** Check if the spectrum is suitable to generate noise for this record
   *
   *  \return true if requirements are fullfilled
   *
   *  Check the following conditions:
   *  1. Sampling frequencies match
   *  2. IDFT samplesize is greater than record size
   */
  bool requirementsMatch(const GeneratorRecord& record) const;
};

/** Add a pulse to a record following the small signal theory for photons
 */
class AddPhotonPulse : public Transformation {
 public:
  /** Constructor.
   *
   *  \param amplitude peak amplitude in Volt
   *  \param tRise pulse rise time in seconds
   *  \param tFall pulse fall time in seconds
   *  \param tOffset offset relativ to record begin
   */
  AddPhotonPulse(const Parameter &amplitude, const Parameter &tRise,
                 const Parameter &tFall, const Parameter &tOffset);

  AddPhotonPulse& operator() (GeneratorRecord& record) override;

  std::string to_string() const override;

  /** Calculate the pulse minimum time
   *
   *  return minimum time in seconds
   *
   *  Uses the anlytic solution and incorporates `tOffset`
   */
  double getPeakPosition() const;

  /** Get the underlying analytic pulse function
   *
   *  \return photon pulse function
   */
  std::shared_ptr<TF1> getFunction() const;

 private:
  std::shared_ptr<TF1> sp_fPhoton_ = nullptr; ///< photon pulse function
};


} /* namespace fast */

#endif /* __TRANSFORMATIONS_H__ */
