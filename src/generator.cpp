#include "generator.h"


using namespace fast;


Generator::Generator(const size_t size, const double samplingFreq)
    : size_(size), samplingFreq_(samplingFreq),
      genRecord_ChA_(size, samplingFreq), genRecord_ChB_(size, samplingFreq) {}

Generator::~Generator() {}

void Generator::setSize(const std::size_t size) {
  size_ = size;
}

void Generator::setSamplingFreq(const double freq) {
  samplingFreq_ = freq;
}

std::vector<ChannelTransformation>
Generator::getTransformations() const {
  return transformations_;
}

void Generator::generate() {
  genRecord_ChA_ = GeneratorRecord(size_, samplingFreq_);
  genRecord_ChB_ = GeneratorRecord(size_, samplingFreq_);
  record_.dataChA = nullptr;
  record_.dataChB = nullptr;

  for (const auto& transform : transformations_) {
    switch (transform.first) {
      case Channel::ChA:
        (*transform.second)(genRecord_ChA_); break;
      case Channel::ChB:
        (*transform.second)(genRecord_ChB_); break;
    }
  }

  if (hasTransForChannel(Channel::ChA))
    record_.dataChA = &genRecord_ChA_.data_;
  if (hasTransForChannel(Channel::ChB))
    record_.dataChB = &genRecord_ChB_.data_;

  record_.samplingFrequency = samplingFreq_;
}

const AlazarRecord& Generator::record() const {
  return record_;
}

void Generator::setTree(const std::shared_ptr<TTree>& tree) {
  tree_ = tree;

  tree->Branch("dataChA",           &record_.dataChA);
  tree->Branch("dataChB",           &record_.dataChB);
  tree->Branch("triggerChA",        &record_.triggerChA);
  tree->Branch("triggerChB",        &record_.triggerChB);
  tree->Branch("timeStamp",         &record_.timeStamp);
  tree->Branch("samplingFrequency", &record_.samplingFrequency);

  for (const auto &t : transformations_) t.second->setTree(tree);
}

bool Generator::hasTransForChannel(const Channel ch) const {
  return std::any_of(transformations_.cbegin(), transformations_.cend(),
      [ch](const ChannelTransformation& t){ return t.first==ch; });
}
