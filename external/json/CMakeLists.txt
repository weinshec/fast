#______________________________________________________________________________
#                                                                         FILES
set(headers json.hpp)

#______________________________________________________________________________
#                                                                       INSTALL
install(FILES ${headers}
        DESTINATION include/json COMPONENT dev)
