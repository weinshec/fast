#include "routines.h"

#include <stdexcept>

#include "utils.h"


namespace fast {


std::vector<double> derivative(const std::vector<double>& data,
                               const double dx) {
  auto N = data.size();
  std::vector<double> dy(N);

  // Lower Bound: Use 1st order accurate forward finite difference
  dy.at(0) = (data.at(1) - data.at(0)) / dx;

  // Interior: Use 2nd order accurate central finite differences
  for (std::size_t i = 1; i < N-1; i++)
    dy.at(i) = (data.at(i+1) - data.at(i-1)) / (2.*dx);

  // Upper Bound: Use 1st order accurate backward finite difference
  dy.at(N-1) = (data.at(N-1) - data.at(N-2)) / dx;

  return dy;
}

std::vector<double> derivative(const std::vector<double>* data,
                               const double dx) {
  if (data == nullptr) return std::vector<double>();
  else return derivative(*data, dx);
}


FilterKernel::FilterKernel(const std::vector<double>& kernel,
                           const Padding padding)
    : kernel_(kernel), padding_(padding) {
  if (kernel_.size() % 2 == 0) {
    utils::LogError("FilterKernel") << "Kernel must not be even sized";
    throw std::runtime_error("Error while initializing FilterKernel");
  }
}

std::vector<double> FilterKernel::getKernel() const {
  return kernel_;
}

size_t FilterKernel::getKernelSize() const {
  return kernel_.size();
}

size_t FilterKernel::getKernelHalfSize() const {
  return (getKernelSize() - 1) / 2;
};

FilterKernel::Padding FilterKernel::getPadding() const {
  return padding_;
}

void FilterKernel::setPadding(const Padding padding) {
  padding_ = padding;
}

std::vector<double> FilterKernel::convolute(
    const std::vector<double>& img) const {
  auto kern = getKernel();
  auto halfSize = getKernelHalfSize();
  std::reverse(kern.begin(), kern.end());

  std::vector<double> filtered;
  for (auto it = img.cbegin(); it != img.cend(); ++it) {
    auto subImg = getSubVec(img, it, halfSize, padding_);
    auto conv = std::inner_product(subImg.begin(), subImg.end(),
                                     kern.begin(), 0.0);
    filtered.push_back(conv);
  }
  return filtered;
}

std::vector<double> FilterKernel::correlate(const std::vector<double>& img,
                                            const bool norm) const {
  auto kern = getKernel();
  auto halfSize = getKernelHalfSize();
  if (norm) normalize(kern.begin(), kern.end());

  std::vector<double> filtered;
  for (auto it = img.cbegin(); it != img.cend(); ++it) {
    auto subImg = getSubVec(img, it, halfSize, padding_);
    if (norm) FilterKernel::normalize(subImg.begin(), subImg.end());
    auto corr = std::inner_product(subImg.begin(), subImg.end(),
                                   kern.begin(), 0.0);
    if (norm) corr /= getKernelSize();
    filtered.push_back(corr);
  }
  return filtered;
}

std::vector<double> FilterKernel::correlate_offsetCorrected(
    const std::vector<double>& img) const {
  auto kern = getKernel();
  auto halfSize = getKernelHalfSize();
  meanCorrect(kern.begin(), kern.end());

  std::vector<double> filtered;
  for (auto it = img.cbegin(); it != img.cend(); ++it) {
    auto subImg = getSubVec(img, it, halfSize, padding_);
    meanCorrect(subImg.begin(), subImg.end());
    auto corr = std::inner_product(subImg.begin(), subImg.end(),
                                   kern.begin(), 0.0);
    filtered.push_back(corr);
  }
  return filtered;
}

void FilterKernel::meanCorrect(const std::vector<double>::iterator& first,
                               const std::vector<double>::iterator& last) {
  double mean = std::accumulate(first, last, 0.0) / std::distance(first, last);
  std::transform(first, last, first,
                 [mean](const double x) { return (x-mean); });
}

void FilterKernel::normalize(const std::vector<double>::iterator& first,
                             const std::vector<double>::iterator& last) {
  meanCorrect(first, last);
  double sq_sum = std::inner_product(first, last, first, 0.0);
  double stddev = std::sqrt(sq_sum / std::distance(first,last));
  std::transform(first, last, first,
                 [stddev](const double x) { return x/stddev; });
}

std::vector<double> FilterKernel::getSubVec(
    const std::vector<double>& img,
    const std::vector<double>::const_iterator& it,
    const size_t halfSize, const Padding padding) {

  std::vector<double> subImg;
  auto winBegin = it - halfSize;    // window begin
  auto winEnd   = it + halfSize+1;  // window end

  // Left side padding (winBegin before img start)
  if (winBegin < img.cbegin()) {
    std::fill_n(std::back_inserter(subImg),
                std::distance(winBegin, img.cbegin()),
                padding == ZERO ? 0.0 : img.front());
    winBegin = img.cbegin();
  }

  // Right size padding (winEnd after img end)
  if (winEnd <= img.cend()) {
    std::copy(winBegin, winEnd, std::back_inserter(subImg));
  } else {
    std::copy(winBegin, img.cend(), std::back_inserter(subImg));
    std::fill_n(std::back_inserter(subImg),
                std::distance(img.cend(), winEnd),
                padding == ZERO ? 0.0 : img.back());
  }
  return subImg;
}


} /* namespace fast */
