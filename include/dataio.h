#ifndef __DATAIO_H__
#define __DATAIO_H__

#include "defs.h"
#include "utils.h"

#include <iterator>
#include <memory>

#include "TFile.h"
#include "TTree.h"

namespace fast {

/** Mapping branch names with data fields.
 */
using BranchMap = std::map<std::string,void*>;

/** Base class for entries read by the TreeReader.
 *  Entries read by TreeReader instances have to inherit from TreeRecord and
 *  initialize the branchmap member, which is empty by default.
 */
struct TreeRecord {
  BranchMap branchmap = {};
};

/** Definition of entries read from alazar acquired data.
 */
struct AlazarRecord : TreeRecord {
  double timeStamp             = 0;       ///< record time stamp in seconds
  double samplingFrequency     = 0.0;     ///< sampling frequency in Hz
  bool triggerChA              = false;   ///< trigger flag Channel A
  bool triggerChB              = false;   ///< trigger flag Channel B
  std::vector<double>* dataChA = nullptr; ///< recorded data Channel A
  std::vector<double>* dataChB = nullptr; ///< recorded data Channel B

  BranchMap branchmap {
    {"timeStamp",         &timeStamp},
    {"samplingFrequency", &samplingFrequency},
    {"triggerChA",        &triggerChA},
    {"triggerChB",        &triggerChB},
    {"dataChA",           &dataChA},
    {"dataChB",           &dataChB}
  };
};

/** Interface enabling objects to store (intermediate) results in a TTree.
 *  Unifies the way to save (intermediata) results while processing data in a
 *  loop, e.g. while generating MC records or while analyzing data. The
 *  implementing object must create TBranches on the given TTree pointing to
 *  internal members, e.g.
 *  ~~~ {.cpp}
 *    tree->Branch("name", &member_);
 *  ~~~
 *  During loop processing the members have to be filled. The actual call to
 *  `tree->Fill()` is left to client code, since more than one object may have
 *  branches in the same tree.
 */
class TreeLoggable {
 public:
  /** Create branches in TTree pointing to member variables.
   *
   *  \param tree TTree object to create the branches in.
   */
  virtual void setTree(const std::shared_ptr<TTree>& tree) = 0;
};

/** Convenience class for easy reading TTree entries of known structure.
 *  Simplifies the task to read entries from a TTree with known branch
 *  structure, e.g. Alazar acquired data. The class is templated taking the
 *  branch structure from a TreeRecord type containing a member field for every
 *  branch to read and the according BranchMap mapping branch names with
 *  fields.
 */
template<typename T>
class TreeReader {
 public:
  /** Constructor.
   *
   *  \param filename path to the input root file
   *  \param treename name of the TTree to read
   *  \param branchname_modifier changes the branchname according to the given
   *    function
   *
   *  If branchname_modifier is not `nullptr` the modifier function is applied
   *  to all branchnames in the `BranchMap` of this TreeReaders Record type.
   *  The following example demonstrates a common use case when reading
   *  branches with a "_ChA" prefix:
   *  ~~~ {.cpp}
   *    TreeReader<TruthRecord> truthReader(tree,
   *        [](const std::string& s){ return utils::addChannelSuffix(s,ChA);});
   *  ~~~
   */
  TreeReader(const std::string &filename, const std::string &treename,
             const utils::StringMorph& branchname_modifier = nullptr)
      : file_(new TFile(filename.c_str(), "READ")),
        tree_((TTree*) file_->Get(treename.c_str())) {

    if (file_->IsZombie())
      utils::LogError("TreeReader") << "Cannot open file " + filename;
    else
      utils::LogInfo("TreeReader") << "Opened file " + filename;

    setBranchAddresses(branchname_modifier);
  }

  /** Constructor.
   *
   *  \param tree smart pointer to TTree to read
   *  \param branchname_modifier changes the branchname according to the given
   *    function
   *
   *  If branchname_modifier is not `nullptr` the modifier function is applied
   *  to all branchnames in the `BranchMap` of this TreeReaders Record type.
   *  The following example demonstrates a common use case when reading
   *  branches with a "_ChA" prefix:
   *  ~~~ {.cpp}
   *    TreeReader<TruthRecord> truthReader(tree,
   *        [](const std::string& s){ return utils::addChannelSuffix(s,ChA);});
   *  ~~~
   */
  TreeReader(const std::shared_ptr<TTree> tree,
             const utils::StringMorph& branchname_modifier = nullptr)
      : file_(nullptr), tree_(tree) {
    setBranchAddresses(branchname_modifier);
  }

  /** Get number of entries.
   *
   *  \return number entries in tree
   */
  std::size_t size() const {
    return tree_->GetEntries();
  }

  /** Get an entry from the tree.
   *
   *  \param index number of the entry to retrieve
   *  \return entry read from tree
   */
  T getEntry(const std::size_t index) const {
    tree_->GetEntry(index);
    return T(entry_);
  }

 private:
  std::unique_ptr<TFile> file_ = nullptr;   ///< Pointer to containing TFile
  std::shared_ptr<TTree> tree_ = nullptr;   ///< TTree object to read from

  T entry_;   ///< TreeRecord type to set branch addresses to

  /** Load branches according to TreeRecords branchmap
   *
   *  \param modifier changes the branchname according to the given function
   */
  void setBranchAddresses(const utils::StringMorph& modifier) {
    checkBranchMap();
    for (const auto& branch : entry_.branchmap) {
      std::string branchName;
      if (modifier) branchName = modifier(branch.first);
      else          branchName = branch.first;
      utils::LogDebug("TreeReader") << "Set branch address for " + branchName;
      tree_->SetBranchAddress(branchName.c_str(), branch.second);
    }
  }

  /** Check if BranchMap is empty and display a warning.
   */
  void checkBranchMap() const {
    if (entry_.branchmap.empty())
      utils::LogWarning("TreeReader") << "Warning: BranchMap is empty!";
  }
};

/** Treat a branch of std::vector as unifed vector.
 *  A UnifiedVector treats the entries in a std::vector branch of TTree as
 *  single concatenated version. It is implemented in compliance with the STL
 *  std::vector so that standard algorithms and operations work as expected.
 *  Caching of tree entries makes it fast without copying the full tree into
 *  memory.
 */
template<typename T>
class UnifiedVector {
 public:

  /** Constant Iterator for UnifiedVectors
   */
  class ConstIterator {
   public:
    using value_type        = T;
    using size_type         = std::size_t;
    using difference_type   = std::ptrdiff_t;
    using reference         = T&;
    using const_reference   = const T&;
    using pointer           = T*;
    using const_pointer     = const T*;
    using iterator_category = std::bidirectional_iterator_tag;
    using self_type         = ConstIterator;

    ConstIterator(const UnifiedVector<T>* uv, std::size_t pos)
        : uv_(uv), pos_(pos) { }

    const_reference operator*() {
      return (*uv_)[pos_];
    }

    const_pointer operator->() {
      return &(operator*());
    }

    self_type& operator++() {
      ++pos_;
      return *this;
    }

    self_type operator++(int) {
      self_type tmp(*this);
      ++(*this);
      return tmp;
    }

    self_type& operator--() {
      --pos_;
      return *this;
    }

    self_type operator--(int) {
      self_type tmp(*this);
      --(*this);
      return tmp;
    }

    self_type operator+(difference_type n) {
      self_type tmp(*this);
      tmp.pos_ += n;
      return tmp;
    }

    self_type& operator+=(difference_type n) {
      pos_ += n;
      return *this;
    }

    self_type operator-(difference_type n) {
      self_type tmp(*this);
      tmp.pos_ -= n;
      return tmp;
    }

    self_type &operator-=(difference_type n) {
      pos_ -= n;
      return *this;
    }

    bool operator==(const self_type &other) const {
      return (other.uv_ == uv_) && (other.pos_ == pos_);
    }

    bool operator!=(const self_type &other) const {
      return (other.uv_ != uv_) || (other.pos_ != pos_);
    }

   private:
    const UnifiedVector<T>* uv_ = nullptr;
    std::size_t pos_;
  };

  using value_type             = T;
  using size_type              = std::size_t;
  using difference_type        = std::ptrdiff_t;
  using reference              = T&;
  using const_reference        = const T&;
  using pointer                = T*;
  using const_pointer          = const T*;
  using self_type              = UnifiedVector<T>;
  using const_iterator         = ConstIterator;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  /** Constructor.
   *
   *  \param tree TTree object containing the std::vector branch
   *  \param branchname name of the std::vector branch to read
   */
  UnifiedVector(const std::shared_ptr<TTree>& tree,
                const std::string& branchname)
      : buf_(new std::vector<T>), tree_(tree) {
    tree_->SetBranchAddress(branchname.c_str(), &buf_);

    size_type acc = 0;
    for (auto n = 0; n < tree_->GetEntries(); ++n) {
      tree->GetEntry(n);
      acc += buf_->size();
      accSize_.push_back(acc);
    }
  }

  /** Constructor.
   *
   *  \param tree TTree object containing the std::vector branch
   *  \param branchname name of the std::vector branch to read
   *
   */
  UnifiedVector(TTree* tree, const std::string& branchname)
      : UnifiedVector(std::shared_ptr<TTree>(tree), branchname) {}

  /** Get an (const) iterator pointing to the first element.
   */
  const_iterator begin() const {
    return cbegin();
  }

  /** Get an (const) iterator pointing after the last element.
   */
  const_iterator end() const {
    return cend();
  }

  /** Get an (const) iterator pointing to the first element.
   */
  const_iterator cbegin() const {
    return const_iterator(this, 0);
  }

  /** Get an (const) iterator pointing after the last element.
   */
  const_iterator cend() const {
    return const_iterator(this, size());
  }

  /** Get an (const) reverse iterator pointing before the first element.
   */
  const_reverse_iterator crbegin() const {
    return const_reverse_iterator(this, 0);
  }

  /** Get an (const) reverse iterator pointing to the last element.
   */
  const_reverse_iterator crend() const {
    return const_reverse_iterator(this, size());
  }

  /** Get the unified size, i.e. the accumulated size of all entries.
   */
  size_type size() const {
    return accSize_.back();
  }

  /** Check if all entries are empty.
   */
  bool empty() const {
    return (size() == 0);
  }

  /** Access element with global index.
   *
   *  \param pos global index of element
   *  \return reference to element at index
   */
  const_reference at(size_type pos) const {
    return buf_->at( getLocalIndex(pos) );
  }

  /** Access element with global index without range check.
   *
   *  \param pos global index of element
   *  \return reference to element at index
   */
  const_reference operator[](size_type pos) const {
    return buf_->operator[]( getLocalIndex(pos) );
  }

  /** Get reference to first element.
   */
  const_reference front() const {
    tree_->GetEntry(0);
    return buf_->front();
  }

  /** Get reference to last element.
   */
  const_reference back() const {
    tree_->GetEntry( tree_->GetEntriesFast() - 1 );
    return buf_->back();
  }

  /** Create a copy of the full tree.
   *
   *  \return copy of the full (concatenated) tree
   *
   *  \warning Depending on the tree size, this might take a lot of memory!
   */
  std::vector<T> data() const {
    std::vector<T> to_vector;
    std::copy(cbegin(), cend(), std::back_inserter(to_vector));
    return to_vector;
  }

 private:
  std::vector<T>* buf_;             ///< internal buffer used for caching.
  std::shared_ptr<TTree> tree_;     ///< tree holding the branch to read.
  std::vector<size_type> accSize_;  ///< accumulated sizes of the entries.

  /** Load the underlying entry according to its index if not already cached.
   *
   *  \param idx index of the entry to load
   */
  void loadEntry(size_type idx) const {
    size_type currentEntry = tree_->GetReadEntry();
    if (currentEntry != idx) tree_->GetEntry(idx);
  }

  /** Translate a a global index to a local (entry specific) index.
   *
   *  \param pos global index requested
   *  \return local index within the according entry
   *
   *  This also loads the according index using `loadEntry()`.
   */
  size_type getLocalIndex(size_type pos) const {
    auto it = std::upper_bound(accSize_.begin(), accSize_.end(), pos);
    loadEntry(it - accSize_.begin());
    return pos - *it + buf_->size();
  }
};

} /* namespace fast */


#endif /* __DATAIO_H__ */
