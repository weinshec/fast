#ifndef __PULSEFCT_H__
#define __PULSEFCT_H__

#include <memory>
#include "TF1.h"
#include "TMath.h"

#include "defs.h"


namespace fast {
namespace pulsefct {

/** Photon-like pulse shape function
 *  \param x time of evaluation
 *  \param par shape parameters of the pulse
 *
 *  Describes the TES signal for the solution of the small signal theory. The
 *  shape parameters are defined as follows:
 *
 *  Parameter Nr | Description
 *  ------------ | -----------
 *  0            | peak amplitude [V]
 *  1            | pulse rise time [s]
 *  2            | pulse relaxation time [s]
 *  3            | time offset [s]
 *  4            | DC voltage offset [V]
 *
 *  \note Although the first argument requires an array only the for first
 *  entry is considered. This is due to compatibility reasons for ROOT's TF1
 */
double photon(double *x, double *par);

/** Photon-like pulse shape function
 *  \param x time of evaluation
 *  \param par shape parameters of the pulse
 *
 *  Describes the TES signal for the solution of the small signal theory. The
 *  shape parameters are defined as follows:
 *
 *  Parameter Nr | Description
 *  ------------ | -----------
 *  0            | peak amplitude [V]
 *  1            | pulse rise time [s]
 *  2            | pulse relaxation time [s]
 *  3            | time offset [s]
 *
 *  \note Although the first argument requires an array only the for first
 *  entry is considered. This is due to compatibility reasons for ROOT's TF1
 */
double photon_noOffset(double *x, double* par);


/** Create a TF1 from pulsefct::photon pulse shape function.
 *
 *  \param shape Values to set for the shape parameters.
 *  \return TF1 object using the photon pulse function
 */
TF1 tf1Photon(const PulseShape& shape=PulseShape::PHOTON_55mV());


/** Class representing the analytic function for a MultiPhoton event.
 */
class MultiPhoton {
 public:
  /** Constructor.
   *
   *  \param n the number of photon peaks
   */
  MultiPhoton(const size_t n);

  /** Call operator for compatibility with ROOT's TF1 ctor.
   */
  double operator() (double *x, double *par);

  /** Return a pointer to a TF1 instance using this MultiPhoton object.
   *
   *  The parameters are named as follows
   *
   *  Parameter Nr | Photon Nr | Description
   *  ------------ | --------- | -----------
   *  0            | 1         | peak amplitude [V]
   *  1            | 1         | pulse rise time [s]
   *  2            | 1         | pulse relaxation time [s]
   *  3            | 1         | time offset [s]
   *  4            | 2         | peak amplitude [V]
   *  5            | 2         | pulse rise time [s]
   *  (...)        | (...)     | (...)
   *  4*n          |           | DC voltage offset [V]
   */
  const std::unique_ptr<TF1>& tf1() const;

  /** Return the number of photon peaks.
   */
  size_t getN() const;

 private:
  size_t nPhotons_;             ///< number of photon peaks
  std::unique_ptr<TF1> func_;   ///< TF1 object using this as function instance
};


/** Calculate the minimum time of a photon pulse analytically.
 *
 *  \param tRise rise time of the pulse in seconds
 *  \param tFall fall time of the pulse in seconds
 *  \return time of the pulse minimum
 */
double tmin_analytic(const double tRise, const double tFall);

/** Calculate the minimum time of a photon pulse analytically.
 *
 *  \param shape Pulse shape definition
 *  \return time of the pulse minimum
 */
double tmin_analytic(const PulseShape& shape);

double pulse_integral(const double amplitude, const double tRise,
                      const double tFall);

double pulse_integral(const PulseShape& shape);

} /* namespace pulsefct */
} /* namespace fast */



#endif /* __PULSEFCT_H__ */
