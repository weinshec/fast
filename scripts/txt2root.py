#!/usr/bin/env python
# encoding: utf-8


import argparse
import logging
import os
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
ROOT.gSystem.Load('libfast.so')


def extractMetaData(fileObj, record):
    """Extract meta data from the file header and save it to the record.

    :fileObj: opened txt file object
    :record:  record object to fill
    """

    head = fileObj.tell()
    for line in fileObj:
        if 'Sample Interval' in line:
            period = float(str.split(line)[2])
            record.samplingFrequency = 1./period
            logging.debug("samplingFrequency: %s"
                % str(record.samplingFrequency))
    fileObj.seek(head)


def extractData(fileObj, record):
    """Extract the voltage data from the file.

    :fileObj: opened txt file object
    :record:  record object to fill
    """

    head = fileObj.tell()
    record.dataChA = ROOT.vector('double')()
    for line in fileObj:
        record.dataChA.push_back( float( str.split(line)[-1]) )
    logging.debug("Found samples: %d" % (record.dataChA.size()))
    fileObj.seek(head)


def createTree(record, treeName='alazar_data'):
    """Write the record to a newly created tree.

    :record:  record object to write
    :treeName: name of the tree to create
    :returns:  TTree the newly created data tree
    """

    tree = ROOT.TTree(treeName, treeName)
    tree.SetAutoFlush(0)

    dataChA = ROOT.vector('double')(record.dataChA)
    dataChB = ROOT.vector('double')()

    tree.Branch('dataChA',           dataChA)
    tree.Branch('dataChB',           dataChB)
    tree.Branch('samplingFrequency',
        ROOT.AddressOf(record, 'samplingFrequency'), 'samplingFrequency/D')
    tree.Branch('timeStamp',
        ROOT.AddressOf(record, 'timeStamp'), 'timeStamp/D')
    tree.Branch('triggerChA',
        ROOT.AddressOf(record, 'triggerChA'), 'triggerChA/O')
    tree.Branch('triggerChB',
        ROOT.AddressOf(record, 'triggerChB'), 'triggerChB/O')

    tree.Fill()

    return tree


def assembleTFileName(origName, path=None):
    """Exchange the .txt with .root and prepend it with the given path.

    :origName: original filepath of the .txt file
    :path:     if not None replaces the original path
    :returns:  new file filepath of the .root file

    """
    basename = os.path.basename(origName)
    dirname  = os.path.dirname(origName)

    filename, extension = os.path.splitext(basename)

    tfile    = filename + '.root'
    fullpath = ''

    if not path:
        fullpath = os.path.join( dirname, tfile )
    else:
        fullpath = os.path.join( path, tfile )

    logging.info("Output: %s" % fullpath)

    return fullpath


def convertTxtFile(filepath, args):
    """Convert the given txt file to a root file

    :filepath: path to the txt file
    :args:     parsed commandline arguments
    :returns:  None
    """

    logging.info("Processing %s" % filepath)

    # Create output file and tree
    tfile = ROOT.TFile( assembleTFileName(filepath, args.dir_), 'RECREATE')

    with open(filepath, 'r') as txtFile:

        record = ROOT.fast.AlazarRecord()

        extractMetaData(txtFile, record)
        extractData(txtFile, record)

        record.triggerChA = args.trigger
        logging.debug("trigger: %s" % str(record.triggerChA))

        tree = createTree(record)
        tree.Write()

    # Close the output file
    tfile.Close()

    logging.info("Done processing %s\n" % filepath)




if __name__ == '__main__':

    # Create command line argument parser
    parser = argparse.ArgumentParser(description='Convert txt data like\
        timelines recorded by the Tektronix oscilliscope to a root TTree\
        including some selected metadata in a separate tree')
    parser.add_argument('files', metavar='txtfile', type=str, nargs='+',
                        help='tektronix txt saved data')
    parser.add_argument('-t', '--trigger', action='store_true',
                        help='this record triggered the event')
    parser.add_argument('-d', '--dir', metavar='PATH', type=str, dest='dir_',
                        nargs='?', help='root file(s) output directory')
    parser.add_argument('--debug', action='store_true',
                        help='enable debug output')
    args = parser.parse_args()

    # Set logging options
    logLevel = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(format='[%(levelname)s]: %(message)s', level=logLevel)

    # Process txt files
    for txtFile in args.files:
        convertTxtFile( txtFile, args )
