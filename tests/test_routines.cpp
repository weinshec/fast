#include "gmock/gmock.h"
#include "test_common.h"

#include <cmath>
#include "routines.h"
#include "utils.h"


using namespace testing;


class AFilterKernel: public Test {
 public:
  std::vector<double> kernel{1, 2, 3};
  std::vector<double> img{ 1, 0, 3, 4 };

  void SetUp() {
   fast::utils::Logger::instance()->setLevel(fast::utils::LogLevel::QUIET);
  }
};


TEST_F(AFilterKernel, HasCorrectEntriesWhenInitializedWithVector) {
  fast::FilterKernel k(kernel);
  ASSERT_THAT( k.getKernel(), ElementsAre(1, 2, 3));
}

TEST_F(AFilterKernel, ThrowsExceptionIfFilterKernelIsEvenSize) {
  std::vector<double> kernel{1, 1, 1, 1};
  ASSERT_THROW( fast::FilterKernel{kernel}, std::runtime_error );
}

TEST_F(AFilterKernel, MeanCorrectTransformsVectorInPlace) {
  std::vector<double> v {1, 2, 3};
  fast::FilterKernel::meanCorrect(v.begin(), v.end());
  ASSERT_THAT(v, ElementsAre(-1, 0, 1));
}

TEST_F(AFilterKernel, NormalizeTransformsVectorInPlace) {
  std::vector<double> v {1, 2, 3};
  fast::FilterKernel::normalize(v.begin(), v.end());
  ASSERT_THAT(v, ElementsAre(-std::sqrt(3./2.), 0, std::sqrt(3./2.)));
}

TEST_F(AFilterKernel, CanGetSubImageByIterator) {
  std::vector<double> v {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  const size_t halfSize = 2;
  const auto padding = fast::FilterKernel::Padding::ZERO;

  auto it = v.cbegin(); // pointing to begin of v (0)
  EXPECT_THAT(fast::FilterKernel::getSubVec(v, it, halfSize, padding),
              ElementsAre(0, 0, 0, 1, 2));

  std::advance(it, 4); // pointing to interior of v (4)
  EXPECT_THAT(fast::FilterKernel::getSubVec(v, it, halfSize, padding),
              ElementsAre(2, 3, 4, 5, 6));

  it = v.cend(); // pointing behind v
  EXPECT_THAT(fast::FilterKernel::getSubVec(v, it, halfSize, padding),
              ElementsAre(8, 9, 0, 0, 0));
}

TEST_F(AFilterKernel, ConvolutionWorksAsExpected) {
  fast::FilterKernel k(kernel);
  ASSERT_THAT( k.convolute(img), ElementsAre( 2, 6, 10, 17 ));
}

TEST_F(AFilterKernel, ConvolutionsWorksForRepeatedPadding) {
  fast::FilterKernel k(kernel, fast::FilterKernel::Padding::REPEAT);
  ASSERT_THAT( k.convolute(img), ElementsAre( 5, 6, 10, 21 ) );
}

TEST_F(AFilterKernel, CrossCorrelationWorksAsExpected) {
  fast::FilterKernel k(kernel);
  ASSERT_THAT( k.correlate(img), ElementsAre( 2, 10, 18, 11));
}

TEST_F(AFilterKernel, NormalizedCrossCorrelationIsInCorrectRange) {
  fast::FilterKernel k(kernel);
  EXPECT_THAT( k.correlate(img, true), Each(Ge(-1)));
  EXPECT_THAT( k.correlate(img, true), Each(Le(+1)));
}


class ALocalExtremaFinder: public Test {
 public:
  std::vector<double> data{0, -5, -3, 2, 1, 6, 2, 5, -1};
};

TEST_F(ALocalExtremaFinder, FindsCorrectPositionsOfMinima) {
  ASSERT_THAT( fast::local_minima(data), ElementsAre(1, 4, 6) );
}

TEST_F(ALocalExtremaFinder, IgnoresMinimumAboveThreshold) {
  ASSERT_THAT( fast::local_minima(data, 0.0), ElementsAre(1) );
}

TEST_F(ALocalExtremaFinder, FindsCorrectPositionsOfMaxmima) {
  ASSERT_THAT( fast::local_maxima(data), ElementsAre(3, 5, 7) );
}

TEST_F(ALocalExtremaFinder, IgnoresMaximumBelowThreshold) {
  ASSERT_THAT( fast::local_maxima(data, 3.0), ElementsAre(5, 7) );
}


class ASortingVectorFunction: public Test {
 public:
  std::vector<int>  numbers { 5 ,  1 ,  3 ,  2 ,  4 };
  std::vector<char> letters {'e', 'a', 'c', 'b', 'd'};
};

TEST_F(ASortingVectorFunction, SortingPermutationVectorIsCorrect) {
  auto p = fast::sort_permuation(numbers,
      [](const int& a, const int& b){ return a < b; });
  ASSERT_THAT( p, ElementsAre(1, 3, 2, 4, 0) );
}

TEST_F(ASortingVectorFunction, ApplyingAPermutationWorksAsExpected) {
  auto p = fast::sort_permuation(numbers,
      [](const int& a, const int& b){ return a > b; });
  fast::apply_permutation(letters, p);
  ASSERT_THAT( letters, ElementsAre('e', 'd', 'c', 'b', 'a') );
}


class AMergingVectorFunction: public Test {
 public:
  std::vector<double> vecA { 5e-6, 10e-6, 6.96e-6 };
  std::vector<double> vecB { 7e-6, 10.1e-6 };
};

TEST_F(AMergingVectorFunction, SupersetHoldsAllEntriesAndIsSorted) {
  auto superset = fast::superset(vecA, vecB);
  ASSERT_THAT(superset, ElementsAre(5e-6, 6.96e-6, 7e-6, 10e-6, 10.1e-6));
}

TEST_F(AMergingVectorFunction, MergingCloseByElementsUsingThreshold) {
  double merge_threshold = 0.05e-6;
  auto superset = fast::superset(vecA, vecB);
  auto merged = fast::merge_elements(superset, merge_threshold);
  ASSERT_THAT(merged, ElementsAre(5e-6, 6.96e-6, 10e-6, 10.1e-6));
}

TEST_F(AMergingVectorFunction, MergingCloseByElementsWorksForMoreThan2Values) {
  double merge_threshold = 2.5e-6;
  auto superset = fast::superset(vecA, vecB);
  auto merged = fast::merge_elements(superset, merge_threshold);
  ASSERT_THAT(merged, ElementsAre(5e-6, 10e-6));
}

TEST_F(AMergingVectorFunction, MergingWorksAsExpectedForManyValues) {
  double merge_threshold = 1.5e-6;
  std::vector<double> probe { 5e-6, 6e-6, 7e-6, 10e-6};
  auto merged = fast::merge_elements(probe, merge_threshold);
  ASSERT_THAT(merged, ElementsAre(5e-6, 7e-6, 10e-6));
}
