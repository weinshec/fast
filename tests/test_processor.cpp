#include "gmock/gmock.h"
#include "test_common.h"
#include "processor.h"


using namespace testing;
using namespace fast;


class AToTProcessor: public Test {
 public:
  AlazarRecord record;

  void SetUp() {
    record.samplingFrequency = 1.0;
  }
};

TEST_F(AToTProcessor, CalculatesCorrectTimeOverThreshold) {
  record.dataChA = new std::vector<double>({0, 0, 1, 2, 1, 0});
  ToTProcessor p("tot", Channel::ChA, ToTProcessor::ABOVE, 0.5);
  ASSERT_THAT( p(record).tot, DoubleEq(3.0) );
}

TEST_F(AToTProcessor, YieldsZeroForEmptyDataset) {
  record.dataChA = new std::vector<double>({0, 0, 0, 0, 0, 0});
  ToTProcessor p("tot", Channel::ChA, ToTProcessor::ABOVE, 0.5);
  ASSERT_THAT( p(record).tot, DoubleEq(0.0) );
}

TEST_F(AToTProcessor, WorksAlsoForNegtiveSignedData) {
  record.dataChA = new std::vector<double>({0, -1, -2, -1, 0});
  ToTProcessor p("tot", Channel::ChA, ToTProcessor::BELOW, -0.5);
  ASSERT_THAT( p(record).tot, DoubleEq(3.0) );
}


class APHPIProcessor: public Test {
 public:
  AlazarRecord record;

  void SetUp() {
    record.samplingFrequency = 1.0;
  }
};

TEST_F(APHPIProcessor, YieldsCorrectPeakHeight) {
  record.dataChA = new std::vector<double>({0, 0, -1, -2, -1, 0});
  PHPIProcessor p("phpi", Channel::ChA);
  ASSERT_THAT( p(record).ph, DoubleEq(-2) );
}

TEST_F(APHPIProcessor, FindsPulseStartAndEndIndicesCorrectely) {
  record.dataChA = new std::vector<double>(
      {-0.1, 0.2, 0.1, 0, -1.5, -2, -1, 0.5, -0.2});
  PHPIProcessor p("phpi", Channel::ChA);
  auto res = p(record);

  EXPECT_THAT( res.pi_beg_idx, Eq(4) );
  EXPECT_THAT( res.pi_end_idx, Eq(6) );
}

TEST_F(APHPIProcessor, FindsPulseStartAndEndIndicesForTooLondPulses) {
  record.dataChA = new std::vector<double>(
      {-1.5, -2, -1, -0.2});
  PHPIProcessor p("phpi", Channel::ChA);
  auto res = p(record);

  EXPECT_THAT( res.pi_beg_idx, Eq(0) );
  EXPECT_THAT( res.pi_end_idx, Eq(3) );
}

TEST_F(APHPIProcessor, YieldsCorrectPeakIntegral) {
  record.dataChA = new std::vector<double>(
      {-0.1, 0.2, 0.1, 0, -1.5, -2, -1, 0.5, -0.2});
  PHPIProcessor p("phpi", Channel::ChA);
  ASSERT_THAT( p(record).pi, DoubleEq(-4.5) );
}

TEST_F(APHPIProcessor, YieldsCorrectPeakIntegralForTooLongPulses) {
  record.dataChA = new std::vector<double>(
      {-0.1, 0.2, 0.1, 0, -1.5, -2, -1});
  PHPIProcessor p("phpi", Channel::ChA);
  ASSERT_THAT( p(record).pi, DoubleEq(-4.5) );
}

TEST_F(APHPIProcessor, YieldsCorrectPeakIntegralForTooEarlyPulses) {
  record.dataChA = new std::vector<double>(
      {-1.5, -2, -1, 0.5, -0.2});
  PHPIProcessor p("phpi", Channel::ChA);
  ASSERT_THAT( p(record).pi, DoubleEq(-4.5) );
}

TEST_F(APHPIProcessor, SumsOverFullLengthWhenSpecified) {
  record.dataChA = new std::vector<double>(
      {-0.1, 0.2, 0.1, 0, -1.5, -2, -1, 0.5, -0.2});
  PHPIProcessor p("phpi", Channel::ChA, true);
  EXPECT_THAT( p(record).pi, DoubleEq(-4) );
  EXPECT_THAT( p(record).pi_beg_idx, Eq(0) );
  EXPECT_THAT( p(record).pi_end_idx, Eq(8) );
}
