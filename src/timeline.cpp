#include "timeline.h"

using namespace fast;

TimelineTrigger::TimelineTrigger(const std::shared_ptr<TTree>& tree,
                                 const std::string& branchname)
    : uv_(tree, branchname) { }

size_t TimelineTrigger::scan(const double trigLvl, const Slope slope,
                             const size_t deadtime) {
  double lastValue = 0;

  for (auto it = uv_.cbegin(); it != uv_.cend(); ++it) {
    if ( ((*it < trigLvl && lastValue > trigLvl && slope == Slope::NEG) ||
          (*it > trigLvl && lastValue < trigLvl && slope == Slope::POS))
         && (distanceToLast(it) > deadtime || distanceToLast(it) == 0) ) {
      peakPos_.push_back(std::distance(uv_.cbegin(), it));
    }
    lastValue = *it;
  }

  return peakPos_.size();
}

std::vector<size_t> TimelineTrigger::getPeakPositions() const {
  return peakPos_;
}

std::vector<double> TimelineTrigger::extract(const size_t pos,
                                             const size_t preSamples,
                                             const size_t postSamples) const {
  auto begin = uv_.cbegin() + pos - preSamples;
  if (preSamples > pos) begin = uv_.begin();

  auto end   = uv_.cbegin() + pos + postSamples;
  if (postSamples + pos > uv_.size()) end = uv_.cend();

  return std::vector<double>(begin, end);
}

AlazarRecord TimelineTrigger::extractRecord(const size_t pos,
                                            const size_t preSamples,
                                            const size_t postSamples) const {
  auto data = new std::vector<double>(extract(pos, preSamples, postSamples));

  AlazarRecord record;
  switch (ch_) {
    case Channel::ChA:
      record.dataChA = data;
      record.triggerChA = true;
      break;
    case Channel::ChB:
      record.dataChB = data;
      record.triggerChB = true;
      break;
  }

  if (fs_) {
    record.samplingFrequency = fs_;
    record.timeStamp = (double) pos / fs_;
  }

  return record;
}

void TimelineTrigger::setRecordChannel(const Channel ch) {
  ch_ = ch;
}

void TimelineTrigger::setRecordSamplingFreq(const double fs) {
  fs_ = fs;
}

size_t TimelineTrigger::distanceToLast(
    const UnifiedVector<double>::ConstIterator& it) const {
  if (peakPos_.empty()) return 0;
  else return std::distance(uv_.cbegin(), it) - peakPos_.back();
}
