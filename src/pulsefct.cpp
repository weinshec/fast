#include "pulsefct.h"
#include "TMath.h"

double fast::pulsefct::photon(double *x, double *par) {
  return photon_noOffset(x, par) + par[4];
}

double fast::pulsefct::photon_noOffset(double *x, double* par) {
  double xx = x[0] - par[3];
  auto norm = TMath::Power(par[2]/par[1], par[2]/(par[1]-par[2])) -
              TMath::Power(par[2]/par[1], par[1]/(par[1]-par[2]));
  if (xx < 0) return 0.0;
  return par[0] / norm
         * (TMath::Exp(-xx/par[1]) - TMath::Exp(-xx/par[2]));
}


TF1 fast::pulsefct::tf1Photon(const PulseShape& shape) {
  TF1 f1("tf1Photon", fast::pulsefct::photon, 0, 1, 5);
  f1.SetParNames("amplitude", "tRise", "tFall", "tOffset", "DCoffset");
  f1.SetParameters(shape.amplitude, shape.tRise, shape.tFall, 0.0, 0.0);
  f1.SetNpx(1000);
  return f1;
}


fast::pulsefct::MultiPhoton::MultiPhoton(const size_t n) : nPhotons_(n) {
  func_.reset(new TF1("tf1MultiPhoton", this, 0, 1, 4*n+1));
  for (size_t n = 0; n < nPhotons_; ++n) {
    func_->SetParName(4*n+0,
        (std::string("amplitude_")+std::to_string(n)).c_str());
    func_->SetParName(4*n+1,
        (std::string("tRise_")+std::to_string(n)).c_str());
    func_->SetParName(4*n+2,
        (std::string("tFall_")+std::to_string(n)).c_str());
    func_->SetParName(4*n+3,
        (std::string("tOffset_")+std::to_string(n)).c_str());
  }
  func_->SetParName(4*n, "DCoffset");
  func_->SetNpx(1000);
}

double fast::pulsefct::MultiPhoton::operator() (double *x, double *par) {
  double global_function = 0.0;
  for (size_t n = 0; n < nPhotons_; ++n)
    global_function += photon_noOffset(x, &par[4*n]);
  global_function += par[4*nPhotons_];
  return global_function;
}

const std::unique_ptr<TF1>& fast::pulsefct::MultiPhoton::tf1() const {
  return func_;
}

size_t fast::pulsefct::MultiPhoton::getN() const {
  return nPhotons_;
}


double fast::pulsefct::tmin_analytic(const double tRise, const double tFall) {
  return -TMath::Log( TMath::Power(tFall/tRise, (tRise*tFall)/(tRise-tFall)) );
}

double fast::pulsefct::tmin_analytic(const PulseShape& shape) {
  return tmin_analytic(shape.tRise, shape.tFall);
}

double fast::pulsefct::pulse_integral(const double amplitude,
                                      const double tRise,
                                      const double tFall) {
  auto xi = TMath::Power(tFall/tRise, tFall/(tRise-tFall)) -
            TMath::Power(tFall/tRise, tRise/(tRise-tFall));
  return amplitude / xi * (tRise - tFall);
}

double fast::pulsefct::pulse_integral(const PulseShape& shape) {
  return pulse_integral(shape.amplitude, shape.tRise, shape.tFall);
}
