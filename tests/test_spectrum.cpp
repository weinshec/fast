#include "gmock/gmock.h"
#include "test_common.h"
#include "spectrum.h"

#include "TF1.h"
#include "TH1D.h"


using namespace testing;
using namespace fast;


class aWindowFct: public Test {
 public:
  unsigned int N = 4;
  TH1D* testSample = new TH1D("testSample", "testSample", N, 0, 1.0);
  std::vector<double> testValues{0.0, 1.0, 2.0, 3.0};

  void SetUp() {
    for (int i = 0; i < testSample->GetNbinsX(); i++)
      testSample->SetBinContent(i+1, testValues.at(i));
  }

  void TearDown() {
    delete testSample;
  }
};

TEST_F(aWindowFct, ReturnsWindowOfCorrectSize) {
  WindowFct w(N);
  ASSERT_THAT( w.size(), Eq(N) );
}

TEST_F(aWindowFct, RectWindowIsFilledWithOnes) {
  WindowFct window(N, WindowFct::Type::RECT);
  auto weights = window.getWeights();

  ASSERT_THAT( weights, Each(1.0) );
}

TEST_F(aWindowFct, HanningWindowHasCorrectWeights) {
  WindowFct window(N, WindowFct::Type::HANNING);
  auto weights = window.getWeights();

  ASSERT_THAT(weights,
    ElementsAre(DoubleEq(0.0), DoubleEq(0.5), DoubleEq(1.0), DoubleEq(0.5)));
}

TEST_F(aWindowFct, ApplyingTheWindowYieldsCorrectValues) {
  WindowFct window(N, WindowFct::Type::HANNING);
  window.applyOn(testSample);

  ASSERT_THAT( getHistContent(testSample),
      ElementsAre(DoubleEq(0.0),     // 0.0 * 0.0
                  DoubleEq(0.5),     // 1.0 * 0.5
                  DoubleEq(2.0),     // 2.0 * 1.0
                  DoubleEq(1.5)) );  // 3.0 * 0.5
}

TEST_F(aWindowFct, ApplyOnRaisesExceptionIfIncompatibleSizes) {
  auto wrongSize = N+5;
  WindowFct window(wrongSize);

  ASSERT_THROW( window.applyOn(testSample), std::runtime_error );
}

TEST_F(aWindowFct, CalculatesNormalizationsCorrect) {
  WindowFct window(N, WindowFct::Type::HANNING);

  EXPECT_THAT( window.getSumW(),  Eq(2.0));
  EXPECT_THAT( window.getSumW2(), Eq(1.5));
}



class aDFT: public Test {
 public:
  // Create sine with f = 20Hz of 3s sampled at 100Hz
  double fs      = 100.0;
  double T       = 3.0;
  unsigned int N = fs*T;

  TH1D* hSample = nullptr;

  void SetUp() {
    TF1* fsin = new TF1("fsin", "sin(2*TMath::Pi()*20*x)", 0, T);
    hSample   = new TH1D("hsample", "hsample", N, 0, T);

    double t;
    for (unsigned int i = 0; i < N ; i++) {
      t = hSample->GetBinLowEdge(i);
      hSample->SetBinContent(i+1, fsin->Eval(t));
    }
  }

  void TearDown() {
    delete hSample;
  }
};

TEST_F(aDFT, HistConstructorGetsSamplingFrequencyRight) {
  DFT dft(hSample);
  ASSERT_THAT(dft.getSamplingFreq(), Eq(fs));
}

TEST_F(aDFT, ArrayConstructorGetsTimeSpanRight) {
  std::vector<double> volts{1, 2, 3, 4, 5, 6};
  double fs = 10.0;

  DFT dft(volts, fs);
  ASSERT_THAT(dft.getTimeSpan(), DoubleEq(0.6));
}

TEST_F(aDFT, CalculatesFrequencyResolutionRight) {
  DFT dft(hSample);
  ASSERT_THAT(dft.getFreqResolution(), DoubleEq(fs/N));
}

TEST_F(aDFT, AllowsSettingWindowFunction) {
  DFT dft(hSample);
  dft.setWindowFctType(WindowFct::Type::BLAHAR);
  EXPECT_THAT(dft.getWindowFct()->getType(), Eq(WindowFct::Type::BLAHAR));
}

TEST_F(aDFT, PowerSpectrumHasCorrectXscale) {
  DFT dft(hSample);
  auto PS = dft.spectrum(SpectrumType::PS);

  auto binning = getHistBinning(PS);
  EXPECT_THAT(binning.size(), Eq(N/2+1));
  EXPECT_THAT(binning.back(), DoubleEq(fs/2.));
}

TEST_F(aDFT, PowerSpectrumHasPeakAtCorrectPosition) {
  DFT dft(hSample);
  auto PS = dft.spectrum(SpectrumType::PS);

  ASSERT_THAT( PS.GetBinLowEdge(PS.GetMaximumBin()), DoubleEq(20) );
}

TEST_F(aDFT, ApplyingDifferentWindowFunctionsChangesPowerSpectrum) {
  DFT dft(hSample);
  auto ps_default = dft.spectrum(SpectrumType::PS, "ps_default");

  dft.setWindowFctType(WindowFct::BLAHAR);
  auto ps_other = dft.spectrum(SpectrumType::PS, "ps_other");

  ASSERT_THAT( getHistContent(ps_default),
      Pointwise(Ne(),getHistContent(ps_other)));
}

TEST_F(aDFT, PowerSpectralDensityHasPeakAtCorrectPosition) {
  DFT dft(hSample);
  auto PSD = dft.spectrum(SpectrumType::PSD);

  ASSERT_THAT( PSD.GetBinLowEdge(PSD.GetMaximumBin()), DoubleEq(20) );
}

TEST_F(aDFT, SplittingYieldsCorrectValues) {
  double fs           = 10000;
  double fres_desired = 3.0;

  double fres_effective;
  auto Nsplit = DFT::calcSplitting(fs, fres_desired, fres_effective);

  EXPECT_THAT( Nsplit,         Eq(3328) );
  EXPECT_THAT( fres_effective, DoubleNear(3.00481, 1e-5) );
}

TEST_F(aDFT, SettingTargetFreqResolutionYieldsCorrectSplitting) {
  auto   fres_target = 1.0;
  double fres_effective;
  auto   Nsplit = DFT::calcSplitting(fs, 1.0, fres_effective);

  DFT dft(hSample);
  dft.setFreqResolution(fres_target);

  EXPECT_THAT( dft.getFreqResolution(), DoubleEq(fres_effective) );
  EXPECT_THAT( dft.getN(), Eq(Nsplit) );
  EXPECT_THAT( dft.getTimeSpan(), DoubleEq(Nsplit / fs) );
  EXPECT_THAT( dft.getWindowFct()->size(), Eq(Nsplit) );
}

TEST_F(aDFT, SettingTargetFreqResolutionYieldsCorrectSpectrumScale) {
  auto   fres_target = 1.0;
  double fres_effective;

  DFT::calcSplitting(fs, 1.0, fres_effective);
  DFT dft(hSample);
  dft.setFreqResolution(fres_target);

  auto ps = dft.spectrum(SpectrumType::PS);
  ASSERT_THAT( ps.GetBinWidth(1), DoubleEq(fres_effective) );
}

TEST_F(aDFT, SettingTargetFreqResolutionThrowsExceptionIfTooLow) {
  auto fres_target = 0.00001;

  DFT dft(hSample);
  ASSERT_THROW( dft.setFreqResolution(fres_target), std::runtime_error );
}

TEST_F(aDFT, AutomaticDCRemovalZerosFirstFrequencyBin) {
  // Add offset to hSample
  for (size_t i = 0; i < N; ++i)
    hSample->AddBinContent(i+1, 200);

  DFT dft(hSample);
  auto PS_corrected = dft.spectrum(SpectrumType::PS);
  EXPECT_THAT(PS_corrected.GetBinContent(1), DoubleNear(0.0, 1e-9));

  dft.setDCremoval(false);
  auto PS_uncorrected = dft.spectrum(SpectrumType::PS);
  EXPECT_THAT(PS_uncorrected.GetBinContent(1), Gt(1.0));
}



class aIDFT : public Test {
 public:
  // Create sine with f = 20Hz of 3s sampled at 100Hz
  double fs      = 100.0; // [Hz] sampling frequency
  double fres    = 1.0;   // [Hz] frequency resolution
  double T       = 1.0;   // [s]  resulting timeseries length
  unsigned int N = fs*T;

  TH1D* hSample = nullptr;

  void SetUp() {
    TRandom3 rnd;
    hSample = new TH1D("hsample", "hsample", N, 0, T);
    for (unsigned int i = 0; i < N ; i++)
      hSample->SetBinContent(i+1, rnd.Gaus(0.0, 1.0));
  }

  void TearDown() {
    delete hSample;
  }
};

TEST_F(aIDFT, ConstructorGetsNyquistFrequencyRight) {
  DFT dft(hSample);
  dft.setFreqResolution(fres);

  IDFT idft( dft.spectrum(SpectrumType::PS), SpectrumType::PS);
  ASSERT_THAT(idft.getNyquistFreq(), DoubleEq(fs/2.));
}

TEST_F(aIDFT, CalculatesSamplingFrequencyRight) {
  DFT dft(hSample);
  dft.setFreqResolution(fres);

  IDFT idft( dft.spectrum(SpectrumType::PS), SpectrumType::PS);
  ASSERT_THAT( idft.getSamplingFreq(), DoubleEq(fs) );
}

TEST_F(aIDFT, RandomSamplingYieldsTimeSeriesOfCorrectLength) {
  DFT dft(hSample);
  dft.setFreqResolution(fres);

  IDFT idft( dft.spectrum(SpectrumType::PS), SpectrumType::PS);
  auto sample = idft.drawSample();

  auto binning = getHistBinning(sample, true);   // get overflow bin too
  EXPECT_THAT( binning.size()-1, Eq(N) );
  EXPECT_THAT( binning.back(),   DoubleEq(T) );
}

TEST_F(aIDFT, DrawingARandomPhaseForGivenMagnitudeWorks) {
  DFT dft(hSample);
  dft.setFreqResolution(fres);

  IDFT idft( dft.spectrum(SpectrumType::PS), SpectrumType::PS);

  auto mag = 2.0;
  double re, im;

  idft.drawRndPhase(mag, re, im);
  ASSERT_THAT( re*re+im*im, DoubleEq(mag*mag) );
}

TEST_F(aIDFT, DFTofRandomSampleResultsInSamePowerSpectrum) {
  DFT dft(hSample);
  dft.setWindowFctType(WindowFct::HANNING);
  auto origPS = dft.spectrum(SpectrumType::PS);

  IDFT idft( origPS, SpectrumType::PS);
  auto rndSample = idft.drawSample();

  DFT newDft(rndSample);
  newDft.setWindowFctType(WindowFct::RECT);
  auto newPS = newDft.spectrum(SpectrumType::PS);

  // ignore DC bin and nyq bin since they were set to 0
  for (int i = 2; i < newPS.GetNbinsX()-1; ++i)
    EXPECT_THAT(newPS.GetBinContent(i),
                DoubleNear(origPS.GetBinContent(i), 1e-9));
}

TEST_F(aIDFT, TwoSubsequentDrawnSamplesAreNotNumericalEqual) {
  DFT dft(hSample);
  dft.setFreqResolution(fres);

  IDFT idft( dft.spectrum(SpectrumType::PS), SpectrumType::PS);
  auto sample1 = idft.drawSample();
  //sample1.SetName("sample1");
  auto sample2 = idft.drawSample();
  //sample2.SetName("sample2");

  ASSERT_THAT( getHistContent(sample1),
               Pointwise(Ne(), getHistContent(sample2)) );
}

TEST_F(aIDFT, DFTofRandomSampleResultsInSameLinearSpectralDensity) {
  DFT dft(hSample);
  dft.setWindowFctType(WindowFct::HANNING);
  auto origLSD = dft.spectrum(SpectrumType::LSD);

  IDFT idft( origLSD, SpectrumType::LSD);
  auto rndSample = idft.drawSample();

  DFT newDft(rndSample);
  newDft.setWindowFctType(WindowFct::RECT);
  auto newLSD = newDft.spectrum(SpectrumType::LSD);

  // ignore DC bin and nyq bin since they were set to 0
  for (int i = 2; i < newLSD.GetNbinsX()-1; ++i)
    EXPECT_THAT(newLSD.GetBinContent(i),
                DoubleNear(origLSD.GetBinContent(i), 1e-9));
}

TEST_F(aIDFT, SettingNoRandomSeedGeneratesDifferentSamples) {
  DFT dft(hSample);
  auto lsd = dft.spectrum(SpectrumType::LSD);

  IDFT idft_1( lsd, SpectrumType::LSD );
  IDFT idft_2( lsd, SpectrumType::LSD );

  ASSERT_THAT( getHistContent(idft_1.drawSample()),
               Pointwise(Ne(), getHistContent(idft_2.drawSample()))
             );
}

TEST_F(aIDFT, SettingARandomSeedGeneratesEqualSamples) {
  DFT dft(hSample);
  auto lsd = dft.spectrum(SpectrumType::LSD);

  IDFT idft_1( lsd, SpectrumType::LSD );
  idft_1.setRandomSeed(42);
  IDFT idft_2( lsd, SpectrumType::LSD );
  idft_2.setRandomSeed(42);

  ASSERT_THAT( getHistContent(idft_1.drawSample()),
               Pointwise(Eq(), getHistContent(idft_2.drawSample()))
             );
}
