#include "gmock/gmock.h"
#include "test_common.h"
#include "cuts.h"


using namespace testing;
using namespace fast;


class AnEventCut : public Test {
 public:
  struct Record {
    double value;
  } record;

  void SetUp() {
    record.value = 42.;
  }
};

TEST_F(AnEventCut, TakesTemplatedArgument) {
  EventCut<Record> test_cut("testCut",
      [](const Record& r){ return r.value == 42.; });

  EXPECT_THAT( test_cut(record), Eq(true) );
  record.value = 21.;
  EXPECT_THAT( test_cut(record), Eq(false) );
}

TEST_F(AnEventCut, CountersAreZeroAfterInitialization) {
  EventCut<Record> test_cut("testCut",
      [](const Record& r){ return r.value == 42.; });
  EXPECT_THAT( test_cut.getRejected() , Eq(0) );
  EXPECT_THAT( test_cut.getPassed()   , Eq(0) );
}

TEST_F(AnEventCut, RejectedEventsAreCountedCorrectly) {
  EventCut<Record> test_cut("testCut",
      [](const Record& r){ return r.value == 42.; });

  test_cut(record);
  EXPECT_THAT( test_cut.getRejected() , Eq(1) );
  EXPECT_THAT( test_cut.getPassed()   , Eq(0) );
}

TEST_F(AnEventCut, PassedEventsAreCountedCorrectly) {
  EventCut<Record> test_cut("testCut",
      [](const Record& r){ return r.value == 42.; });

  record.value = 21.;
  test_cut(record);
  EXPECT_THAT( test_cut.getRejected() , Eq(0) );
  EXPECT_THAT( test_cut.getPassed()   , Eq(1) );
}

TEST_F(AnEventCut, SumsUpCorrectly) {
  EventCut<Record> test_cut("testCut",
      [](const Record& r){ return r.value == 42.; });

  test_cut(record);
  record.value = 21.;
  test_cut(record);

  ASSERT_THAT( test_cut.getTotal(), Eq(2) );
}
