#include "processor.h"
#include "utils.h"
#include "pulsefct.h"
#include <algorithm>
#include <numeric>


using namespace fast;


ProcessingUnit::ProcessingUnit(const std::string& name,
                               const Channel& selection)
    : ch_(selection), name_(name) { }

Channel ProcessingUnit::getChannel() const {
  return ch_;
}

std::string ProcessingUnit::getName() const {
  return name_;
}

std::vector<double>*
ProcessingUnit::getSelectedData(const AlazarRecord& record) const {
  switch (getChannel()) {
    case Channel::ChA:
      return record.dataChA;
    case Channel::ChB:
      return record.dataChB;
    default:
      return nullptr;
  }
}

std::string ProcessingUnit::getBranchName(const std::string& original) const {
  return utils::add_prefix_suffix(name_, original, ch_);
}


PeakFindingUnit::PeakFindingUnit(const std::string& name,
                                 const Channel& selection,
                                 const MethodID methodID)
    : ProcessingUnit(name, selection), kMethodID(methodID) {}

PeakFindingUnit::MethodID PeakFindingUnit::getMethodID() const {
  return kMethodID;
}


ToTProcessor::ToTProcessor(const std::string& name, const Channel selection,
                           const Sign sign, const double threshold)
    : ProcessingUnit(name, selection), sign_(sign), threshold_(threshold) { }

const ToTProcessor::Record&
ToTProcessor::operator() (const AlazarRecord& record) {
  auto nPointsOverThreshold = 0;
  auto data = getSelectedData(record);

  for (const auto &v : *data)
    if (sgn(v-threshold_) == sign_) ++nPointsOverThreshold;

  result_.tot = (double) nPointsOverThreshold / record.samplingFrequency;

  return result_;
}

void ToTProcessor::setTree(const std::shared_ptr<TTree>& tree) {
  tree->Branch( getBranchName("tot").c_str(), &result_.tot);
}

int ToTProcessor::sgn(const double val) const {
  return (0.0 < val) - (val < 0.0);
}


PulseFitProcessor::PulseFitProcessor(const std::string& name,
                                     const Channel& selection,
                                     const double uncert,
                                     const std::shared_ptr<PeakFindingUnit>& pf)
    : ProcessingUnit(name, selection),
      uncert_(uncert), fitter_(new PulseFitter), pf_(pf) { }

const PulseFitProcessor::Record&
PulseFitProcessor::operator() (const AlazarRecord& record) {
  auto data    = getSelectedData(record);
  auto peakPos = pf_ ? pf_->getPeakPos(record) : std::vector<double>();
  auto res     = fitter_->fit(record.samplingFrequency, *data, uncert_, peakPos);
  auto nPhotons = std::max<size_t>(1, peakPos.size());

  result_.amplitude.clear();
  result_.tRise    .clear();
  result_.tFall    .clear();
  result_.tOffset  .clear();
  for (size_t n = 0; n < nPhotons; ++n) {
    result_.amplitude.push_back(res->Parameter(n*4+0));
    result_.tRise    .push_back(res->Parameter(n*4+1));
    result_.tFall    .push_back(res->Parameter(n*4+2));
    result_.tOffset  .push_back(res->Parameter(n*4+3));
  }
  result_.DCoffset  = res->Parameter(nPhotons*4);
  std::cout << res->ParName(nPhotons*4) << std::endl;

  result_.chi2      = res->Chi2();
  result_.ndf       = res->Ndf();
  result_.graph     = fitter_->getGraph();
  result_.fitResult = res;

  return result_;
}

void PulseFitProcessor::setTree(const std::shared_ptr<TTree>& tree) {
  tree->Branch(getBranchName("amplitude").c_str(), &result_.amplitude);
  tree->Branch(getBranchName("tRise").c_str(),     &result_.tRise);
  tree->Branch(getBranchName("tFall").c_str(),     &result_.tFall);
  tree->Branch(getBranchName("tOffset").c_str(),   &result_.tOffset);
  tree->Branch(getBranchName("DCoffset").c_str(),  &result_.DCoffset);
  tree->Branch(getBranchName("chi2").c_str(),      &result_.chi2);
  tree->Branch(getBranchName("ndf").c_str(),       &result_.ndf);
}

const std::unique_ptr<PulseFitter>& PulseFitProcessor::pulseFitter() const {
  return fitter_;
}

const std::shared_ptr<PeakFindingUnit> PulseFitProcessor::getPeakFinder() const {
  return pf_;
}


CloneProcessor::CloneProcessor(const std::string& name,
                               const Channel& selection)
    : ProcessingUnit(name, selection) { }

const CloneProcessor::Record&
CloneProcessor::operator() (const AlazarRecord& record) {
  result_.samplingFrequency = record.samplingFrequency;
  switch (getChannel()) {
    case Channel::ChA:
      result_.trigger = record.triggerChA;
      result_.data = record.dataChA;
      break;
    case Channel::ChB:
      result_.trigger = record.triggerChB;
      result_.data = record.dataChB;
      break;
  }
  return result_;
}

void CloneProcessor::setTree(const std::shared_ptr<TTree>& tree) {
  tree->Branch(getBranchName("samplingFrequency").c_str(),
               &result_.samplingFrequency);
  tree->Branch(getBranchName("trigger").c_str(),
               &result_.trigger);
  tree->Branch(getBranchName("data").c_str(),
               &result_.data);
}


PHPIProcessor::PHPIProcessor(const std::string& name, const Channel& selection,
                             bool fullLength)
    : ProcessingUnit(name, selection), fullLength_(fullLength) { }

const PHPIProcessor::Record&
PHPIProcessor::operator() (const AlazarRecord& record) {
  auto data = getSelectedData(record);

  using RVecIter = std::reverse_iterator<std::vector<double>::iterator>;
  auto ymin  = std::min_element(data->begin(), data->end());
  auto rymin = RVecIter(ymin+1);

  std::vector<double>::iterator pulseBeg, pulseEnd;
  if (fullLength_) {
    pulseBeg = data->begin();
    pulseEnd = data->end();
  } else { // area under the actual pulse only
    auto greaterZero = [](const double v){return v >= 0.0;};
    pulseBeg = std::find_if(rymin, data->rend(), greaterZero).base();
    pulseEnd = std::find_if(ymin, data->end(), greaterZero);
  }

  result_.ph = *ymin;
  result_.pi = std::accumulate(pulseBeg, pulseEnd, 0.0)
               / record.samplingFrequency;
  result_.ph_pos_idx = ymin - data->begin();
  result_.pi_beg_idx = pulseBeg - data->begin();
  result_.pi_end_idx = pulseEnd - data->begin() - 1;

  return result_;
}

void PHPIProcessor::setTree(const std::shared_ptr<TTree>& tree) {
  tree->Branch(getBranchName("ph").c_str(),         &result_.ph);
  tree->Branch(getBranchName("pi").c_str(),         &result_.pi);
  tree->Branch(getBranchName("ph_pos_idx").c_str(), &result_.ph_pos_idx);
  tree->Branch(getBranchName("pi_beg_idx").c_str(), &result_.pi_beg_idx);
  tree->Branch(getBranchName("pi_end_idx").c_str(), &result_.pi_end_idx);
}


XCorrProcessor::XCorrProcessor(const std::string& name,
                               const Channel& selection,
                               const PulseShape& shape,
                               const double tMinFactor,
                               const double threshold)
    : PeakFindingUnit(name, selection, PeakFindingUnit::XCORR),
      kShape(shape), kTminFactor(tMinFactor),
      kTmin(pulsefct::tmin_analytic(shape)),
      threshold_(threshold) {
  result_.methodID = getMethodID();
  result_.params = {kTminFactor, kShape.amplitude, kShape.tRise, kShape.tFall};
}

const XCorrProcessor::Record&
XCorrProcessor::operator() (const AlazarRecord& record) {
  if (!kernel_) initFilterKernel(record.samplingFrequency);

  auto delayCorrection = kernel_->getKernelHalfSize();
  auto data = kernel_->correlate_offsetCorrected(*getSelectedData(record));
  result_.data = std::vector<double>(data.begin()+delayCorrection, data.end());

  result_.time.clear();
  result_.value.clear();
  result_.threshold = threshold_;

  std::vector<size_t> maxima;
  if (threshold_ != 0)
    maxima = local_maxima(result_.data, threshold_);
  else
    maxima = local_maxima(result_.data);

  for (const auto i : maxima) {
    result_.time.push_back( i / record.samplingFrequency);
    result_.value.push_back( result_.data.at(i) );
  }

  auto sortPerm = fast::sort_permuation(result_.value,
      [](const double& a, const double& b){ return a > b; });
  fast::apply_permutation(result_.value, sortPerm);
  fast::apply_permutation(result_.time, sortPerm);

  return result_;
}

void XCorrProcessor::setTree(const std::shared_ptr<TTree>& tree) {
  tree->Branch(getBranchName("time").c_str(), &result_.time);
  tree->Branch(getBranchName("value").c_str(), &result_.value);
  tree->Branch(getBranchName("data").c_str(), &result_.data);
  tree->Branch(getBranchName("methodID").c_str(), &result_.methodID);
  tree->Branch(getBranchName("params").c_str(), &result_.params);
  tree->Branch(getBranchName("threshold").c_str(), &result_.threshold);
}

std::vector<double> XCorrProcessor::getPeakPos(const AlazarRecord& record) {
  return (*this)(record).time;
}

void XCorrProcessor::initFilterKernel(const double fs) {
  auto photon = pulsefct::tf1Photon(kShape);

  unsigned int N = kTmin * kTminFactor * fs;
  if (N % 2 == 0) N += 1;
  std::vector<double> kern(N);

  for (size_t i = 0; i < kern.size(); ++i)
    kern.at(i) = photon.Eval( (double) i / fs );

  kernel_ = std::unique_ptr<FilterKernel>(new FilterKernel(kern));
}


DerivProcessor::DerivProcessor(const std::string& name,
                               const Channel& selection,
                               const double cutOff,
                               const double threshold)
    : PeakFindingUnit(name, selection, PeakFindingUnit::DERIV),
      kCutOff(cutOff),
      threshold_(threshold) {
  result_.methodID = getMethodID();
  result_.params = {cutOff};
}

const DerivProcessor::Record&
DerivProcessor::operator() (const AlazarRecord& record) {
  auto fs       = record.samplingFrequency;
  auto pRawData = getSelectedData(record);

  result_.time.clear();
  result_.value.clear();
  result_.data.clear();

  if (kCutOff > 0) {
    if (!filter_) initDSPFilter(fs);
    std::vector<double> tmpData(pRawData->size());
    std::reverse_copy(pRawData->cbegin(), pRawData->cend(), tmpData.begin());
    filter_->processInPlace(tmpData);
    std::reverse(tmpData.begin(), tmpData.end());
    filter_->processInPlace(tmpData);
    result_.data = derivative( tmpData, fs);
  } else {
    result_.data = derivative( getSelectedData(record), fs);
  }
  std::transform(result_.data.begin(), result_.data.end(), result_.data.begin(),
                 [](double v) { return (-1.0) * v; });

  std::vector<size_t> maxima;
  result_.threshold = threshold_;
  if (threshold_ != 0)
    maxima = local_maxima(result_.data, threshold_);
  else
    maxima = local_maxima(result_.data);

  for (const auto i : maxima) {
    result_.time.push_back( i/record.samplingFrequency );
    result_.value.push_back( result_.data.at(i) );
  }

  auto sortPerm = fast::sort_permuation(result_.value,
      [](const double& a, const double& b){ return a > b; });
  fast::apply_permutation(result_.value, sortPerm);
  fast::apply_permutation(result_.time, sortPerm);

  return result_;
}

void DerivProcessor::setTree(const std::shared_ptr<TTree>& tree) {
  tree->Branch(getBranchName("time").c_str(), &result_.time);
  tree->Branch(getBranchName("value").c_str(), &result_.value);
  tree->Branch(getBranchName("data").c_str(), &result_.data);
  tree->Branch(getBranchName("methodID").c_str(), &result_.methodID);
  tree->Branch(getBranchName("params").c_str(), &result_.params);
  tree->Branch(getBranchName("threshold").c_str(), &result_.threshold);
}

std::vector<double> DerivProcessor::getPeakPos(const AlazarRecord& record) {
  return (*this)(record).time;
}

void DerivProcessor::initDSPFilter(const double fs) {
  filter_.reset(new DspFilter<Dsp::Butterworth::LowPass<5>>);
  filter_->setup(5, fs, kCutOff);
  utils::LogDebug("DerivProcessor")
      << "Initialized DSP LowPass with cutOff " + std::to_string(kCutOff);
}
