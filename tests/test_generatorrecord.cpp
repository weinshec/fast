#include "gmock/gmock.h"
#include "test_common.h"

#include "generatorrecord.h"
#include "utils.h"


using namespace testing;


class AGeneratorRecord: public Test {
 public:
  std::size_t size = 5;
  double      fs   = 1.0;
  fast::GeneratorRecord flat{size, fs};

  void SetUp() {
   fast::utils::Logger::instance()->setLevel(fast::utils::LogLevel::QUIET);
  }
};

TEST_F(AGeneratorRecord, CreatesDatasampleOfCorrectSize) {
  ASSERT_THAT( flat.size(), Eq(size) );
}

TEST_F(AGeneratorRecord, DatasampleIsFlatAndOfCorrectSize) {
  auto sample = flat.getData();
  EXPECT_THAT( sample, SizeIs(5) );
  EXPECT_THAT( sample, Each(0.0) );
}

TEST_F(AGeneratorRecord, AddOperatorAddsPointwiseForScalars) {
  double addend = 2.;

  flat += addend;
  ASSERT_THAT( flat.getData(), Each(addend) );
}

TEST_F(AGeneratorRecord, AddOperatorAddsPointwiseForVectors) {
  std::vector<double> addend{1, 2, 3, 4, 5};

  flat += addend;
  ASSERT_THAT( flat.getData(), ElementsAreArray(addend) );
}

TEST_F(AGeneratorRecord, AddOperatorDoesNothingIfSizesDontMatch) {
  std::vector<double> addend{42};
  flat += addend;
  ASSERT_THAT( flat.getData(), Each(0.0) );
}

TEST_F(AGeneratorRecord, MultiplyOperatorActsPointwise) {
  double factor = 2.;
  flat += 1;
  flat *= factor;
  ASSERT_THAT( flat.getData(), Each(factor) );
}

TEST_F(AGeneratorRecord, ShiftOperatorAllowsLeftShift) {
  flat += std::vector<double>{0, 1, 2, 3, 4};

  flat << 2;

  ASSERT_THAT(flat.getData(), ElementsAre(2, 3, 4, 0, 0));
}

TEST_F(AGeneratorRecord, ShiftOperatorAllowsRightShift) {
  flat += std::vector<double>{0, 1, 2, 3, 4};

  flat >> 2;

  ASSERT_THAT(flat.getData(), ElementsAre(0, 0, 0, 1, 2));
}

TEST_F(AGeneratorRecord, AppendResultsInCorrectSize) {
  fast::GeneratorRecord duplicate(flat);
  flat.append(duplicate);
  ASSERT_THAT( flat.size(), Eq(size*2) );
}

TEST_F(AGeneratorRecord, AppendWorksForStdVectors) {
  std::vector<double> appendend{1,2,3};
  flat.append(appendend);
  ASSERT_THAT( flat.size(), Eq(size+appendend.size()) );
}

TEST_F(AGeneratorRecord, MakeHistogramHasCorrectScale) {
  auto hist    = flat.makeHistogram();
  auto binning = getHistBinning(hist);

  EXPECT_THAT(binning.size(),  Eq(size));
  EXPECT_THAT(binning.front(), DoubleEq(0.0));
  EXPECT_THAT(binning.back(),  DoubleEq((size-1)/fs));
}

TEST_F(AGeneratorRecord, MakeHistogramHasCorrectContent) {
  flat += std::vector<double>{0, 1, 2, 3, 4};
  auto hist = flat.makeHistogram();

  ASSERT_THAT( getHistContent(hist),
               Pointwise(ArrDoubleEq(), flat.getData()) );
}

TEST_F(AGeneratorRecord, TruthMapIsEmptyAfterInitialization) {
  ASSERT_THAT( flat.getTruthMap(), IsEmpty() );
}

TEST_F(AGeneratorRecord, TruthMapAllowsInsertion) {
  flat.addTruth("foobar", 42);
  ASSERT_THAT( flat.getTruthMap()["foobar"], Eq(42) );
}

TEST_F(AGeneratorRecord, TruthMapStaysConstantOnDuplicateKey) {
  flat.addTruth("foobar", 42);
  flat.addTruth("foobar", 21);
  ASSERT_THAT( flat.getTruthMap()["foobar"], Eq(42) );
}
