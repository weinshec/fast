#include "pulsefitter.h"
#include "utils.h"

#include "TMath.h"
#include "Math/MinimizerOptions.h"

using namespace fast;


PulseFitter::PulseFitter(const PulseShape& initShape)
    : kShape(initShape), fixedParameter_(), multiPhoton_(nullptr) {}

TFitResultPtr PulseFitter::fit(const double fs, const std::vector<double> &data,
                               const double uncert,
                               const std::vector<double>& peaks) {
  auto time = sampleTime(data.size(), fs);
  auto yerr = std::vector<double>(data.size(), uncert);
  graph_ = std::make_shared<TGraphErrors>(
      data.size(), &time[0], &data[0], nullptr, &yerr[0]);
  setApproxPeakPositions(peaks);
  return fitGraph();
}

TFitResultPtr PulseFitter::fit(const double fs, const std::vector<double> &data,
                               const std::vector<double>& uncert,
                               const std::vector<double>& peaks) {
  auto time = sampleTime(data.size(), fs);
  graph_ = std::make_shared<TGraphErrors>(
      data.size(), &time[0], &data[0], nullptr, &uncert[0]);
  setApproxPeakPositions(peaks);
  return fitGraph();
}

TFitResultPtr PulseFitter::fit(const TGraphErrors &graph,
                               const std::vector<double>& peaks) {
  graph_ = std::shared_ptr<TGraphErrors>( (TGraphErrors*) graph.Clone() );
  setApproxPeakPositions(peaks);
  return fitGraph();
}

std::shared_ptr<TGraphErrors> PulseFitter::getGraph() const {
  return graph_;
}

void PulseFitter::fixParameter(size_t idx, const double value) {
  multiPhoton_->tf1()->FixParameter(idx, value);
  fixedParameter_.at(idx) = true;
  utils::LogInfo("PulseFitter") << "Fixing parameter "
      + std::string(multiPhoton_->tf1()->GetParName(idx)) + " to "
      + std::to_string(value);
}

void PulseFitter::resetParameter(size_t idx) {
  fixedParameter_.at(idx) = false;
  utils::LogInfo("PulseFitter") << "Set parameter "
      + std::string(multiPhoton_->tf1()->GetParName(idx)) + " to floating";
}

void PulseFitter::setInitFitValues() {
  multiPhoton_.reset(new pulsefct::MultiPhoton(peaks_.size()));
  auto N     = graph_->GetN();
  auto x     = graph_->GetX();

  for (size_t i = 0; i < peaks_.size(); ++i) {
    setParameterIfNotFixed(i*4+0, kShape.amplitude,
        kShape.amplitude*10, kShape.amplitude/10.0);
    setParameterIfNotFixed(i*4+1, kShape.tRise,
        10e-9, kShape.tRise*10.);
    setParameterIfNotFixed(i*4+2, kShape.tFall,
        kShape.tRise*10., kShape.tFall*10.);
    setParameterIfNotFixed(i*4+3, peaks_.at(i),
        x[0], x[N-1]);
  }
  setParameterIfNotFixed(peaks_.size()*4, kInitDCoffset,
      -TMath::Abs(kShape.amplitude), TMath::Abs(kShape.amplitude));

  multiPhoton_->tf1()->SetRange(x[0], x[N-1]);
}

void PulseFitter::lowerTOffsetForIteration(unsigned int iteration) {
  auto shiftRange = pulsefct::tmin_analytic(kShape) * 5.0;
  auto additionalShift = shiftRange * iteration / kMaxIterations;

  for (size_t i = 0; i < peaks_.size(); ++i) {
    if (fixedParameter_.at(i*4+3)) continue;
    auto currentValue = multiPhoton_->tf1()->GetParameter(i*4+3);
    multiPhoton_->tf1()->SetParameter(i*4+3,
        currentValue - shiftRange + additionalShift);
    multiPhoton_->tf1()->SetParLimits(i*4+3,
        currentValue - shiftRange, currentValue + shiftRange);
  }
}

TFitResultPtr PulseFitter::fitGraph(unsigned int iteration) {
  setInitFitValues();
  lowerTOffsetForIteration(iteration);

  ROOT::Math::MinimizerOptions::SetDefaultMinimizer(kMinimizer.c_str());

  std::string fitOptions = "SR";
  if (iteration != kMaxIterations-1) fitOptions += "Q";
  else fitOptions += "V";

  auto resPtr = graph_->Fit(multiPhoton_->tf1().get(), fitOptions.c_str());

  if (!resPtr->IsValid() && iteration < kMaxIterations-1)
    return fitGraph(iteration+1);

  utils::LogDebug("PulseFitter")
      << "Returned after iteration " + std::to_string(iteration);

  return resPtr;
}

std::vector<double> PulseFitter::sampleTime(const std::size_t N,
                                            const double fs) const {
  auto time = std::vector<double>(N);
  for (std::size_t i = 0; i < N; i++)
    time.at(i) = (double) i / fs;
  return time;
}

void PulseFitter::setParameterIfNotFixed(size_t idx, const double value,
                                         const double lowerBound,
                                         const double upperBound) {
  if (!fixedParameter_.at(idx)) {
    multiPhoton_->tf1()->SetParameter(idx, value);
    multiPhoton_->tf1()->SetParLimits(idx, lowerBound, upperBound);
  }
}

void PulseFitter::setApproxPeakPositions(const std::vector<double>& peaks) {
  peaks_.clear();
  if (peaks.empty()) {
    auto y = graph_->GetY();
    auto t_min = graph_->GetX()[
        std::distance(y, std::min_element(y, y+graph_->GetN()))];
    peaks_.push_back(t_min);
  } else {
    peaks_ = peaks;
  }
  fixedParameter_ = std::vector<bool>(4*peaks_.size()+1, false);

  utils::LogDebug("PulseFitter") << "Fitting peaks at the following positions...";
  for (const auto p : peaks_)
    utils::LogDebug("PulseFitter") << p;
}
